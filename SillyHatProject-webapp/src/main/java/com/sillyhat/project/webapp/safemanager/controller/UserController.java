package com.sillyhat.project.webapp.safemanager.controller;

import com.sillyhat.project.common.dto.DataTables;
import com.sillyhat.project.common.dto.SillyHatAJAX;
import com.sillyhat.project.org.user.dto.UserDTO;
import com.sillyhat.project.org.user.service.UserService;
import com.sillyhat.project.webapp.common.base.BaseController;
import com.wordnik.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * SafeManagerController
 *
 * @author 徐士宽
 * @date 2017/5/12 10:05
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;


    @ResponseBody
    @ApiOperation(value = "后台管理-系统管理-用户列表", response = String.class, notes = "后台管理-系统管理-分页查询用户列表")
    @RequestMapping(value = "/queryUserPage", method = {RequestMethod.POST},consumes = MediaType.APPLICATION_JSON_VALUE)
    public DataTables queryUserPage(@RequestBody DataTables dataTables) {
        return userService.queryUserByPage(dataTables);
    }

    @ResponseBody
    @ApiOperation(value = "后台管理-系统管理-保存用户", response = String.class, notes = "后台管理-系统管理-保存用户")
    @RequestMapping(value = "/saveUser", method = {RequestMethod.POST},consumes = MediaType.APPLICATION_JSON_VALUE)
    public SillyHatAJAX saveUser(@RequestBody UserDTO user) {
        if(user.getId() != null && user.getId() != 0l){
            userService.updateUser(user);
        }else{
            userService.addUser(user);
        }
        return new SillyHatAJAX(true);
    }

//    @ResponseBody
//    @ApiOperation(value = "后台管理-系统管理-查询用户", response = String.class, notes = "后台管理-系统管理-查询用户ById")
//    @RequestMapping(value = "/getUserById", method = {RequestMethod.GET},consumes = MediaType.APPLICATION_JSON_VALUE)
//    public SillyHatAJAX getUserById(@RequestParam("userId")String id) {
//        UserDTO dto = userService.getUserById(id);
//        return new SillyHatAJAX(dto);
//    }
    @ResponseBody
    @ApiOperation(value = "后台管理-系统管理-查询用户", response = String.class, notes = "后台管理-系统管理-查询用户ById")
    @RequestMapping(value = "/getUserById", method = {RequestMethod.GET})
    public SillyHatAJAX getUserById(@RequestParam("userId")String id) {
        UserDTO dto = userService.getUserById(id);
        return new SillyHatAJAX(dto);
    }

    @ResponseBody
    @ApiOperation(value = "后台管理-系统管理-删除用户", response = String.class, notes = "后台管理-系统管理-删除用户")
    @RequestMapping(value = "/deleteById", method = {RequestMethod.POST},consumes = MediaType.APPLICATION_JSON_VALUE)
    public SillyHatAJAX deleteById(@RequestParam("userId")String id) {
        UserDTO dto = new UserDTO();
        dto.setId(Long.parseLong(id));
        dto.setUpdatedUser(getCurrentUser().getId());
        userService.deleteUserById(dto);
        return new SillyHatAJAX(true);
    }

}
