package com.sillyhat.project.webapp.learningword.controller;

import com.sillyhat.project.common.utils.SillyHatDateUtils;
import com.sillyhat.project.learning.usertodayplan.service.UserTodayPlanService;
import com.sillyhat.project.learning.wordrepository.dto.WordRepositoryDTO;
import com.sillyhat.project.learning.wordrepository.service.WordRepositoryService;
import com.sillyhat.project.webapp.common.base.BaseController;
import com.wordnik.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * LearningWordController
 *
 * @author 徐士宽
 * @date 2017/4/1 13:59
 */
@Controller
public class LearningWordController extends BaseController {

    @Autowired
    private WordRepositoryService wordRepositoryService;

    @Autowired
    private UserTodayPlanService userTodayPlanService;

    @ApiOperation(value = "进入单词主界面", response = String.class, notes = "进入主界面")
    @RequestMapping(value = "/learing-word/toLearningWord", method = {RequestMethod.GET})
    public String toLearningWord() {
        loadMenuHeader111();
        setAttribute("currentPage","learningWrod");
        setAttribute("currentSubDiv","wordLearning");
        return "/learningWord/wordLearnging";
    }

    @RequestMapping(value = "/learing-word/toWordBook", method = {RequestMethod.GET})
    public String toWordBook() {
        loadMenuHeader111();
        List<WordRepositoryDTO> resultList = wordRepositoryService.queryWordRepositoryAll();
        setAttribute("resultList",resultList);
        setAttribute("currentPage","learningWrod");
        setAttribute("currentSubDiv","wordBook");
        return "/learningWord/wordBook";
    }

    @RequestMapping(value = "/learing-word/toMyWordRepository", method = {RequestMethod.GET})
    public String toMyWordRepository() {
        loadMenuHeader111();
        setAttribute("currentPage","learningWrod");
        setAttribute("currentSubDiv","myWordRepository");
        return toTodayWord();
    }

    /**
     * 今日单词
     * @return
     */
    @RequestMapping(value = "/learing-word/toTodayWord", method = {RequestMethod.GET})
    public String toTodayWord(){
        loadMenuHeader111();
//        userTodayPlanService.
        int learningDateNum = SillyHatDateUtils.getTodayNum();
        setAttribute("currentPage","learningWrod");
        setAttribute("myWordRepositoryType","myWordRepositoryType_today");
        return "/learningWord/myWordRepository";
    }

    /**
     * 正在学习
     * @return
     */
    @RequestMapping(value = "/learing-word/toFamiliarWord", method = {RequestMethod.GET})
    public String toFamiliarWord(){
        loadMenuHeader111();

        setAttribute("currentPage","learningWrod");
        setAttribute("myWordRepositoryType","'myWordRepositoryType_familiar'");
        return "/learningWord/myWordRepository";
    }

    /**
     * 掌握单词
     * @return
     */
    @RequestMapping(value = "/learing-word/toMasterWord", method = {RequestMethod.GET})
    public String toMasterWord(){
        loadMenuHeader111();

        setAttribute("currentPage","learningWrod");
        setAttribute("myWordRepositoryType","myWordRepositoryType_today");

        return "/learningWord/myWordRepository";
    }

    @RequestMapping(value = "/learing-word/toWordSet", method = {RequestMethod.GET})
    public String toWordSet() {
        loadMenuHeader111();

        setAttribute("currentPage","learningWrod");
        setAttribute("currentSubDiv","wordSet");
        return "/learningWord/wordSet";
    }
}
