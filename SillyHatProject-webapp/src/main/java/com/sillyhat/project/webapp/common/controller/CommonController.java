package com.sillyhat.project.webapp.common.controller;

import com.sillyhat.project.common.constants.Constants;
import com.sillyhat.project.common.dto.SillyHatAJAX;
import com.sillyhat.project.common.utils.MD5Util;
import com.sillyhat.project.org.menu.dto.MenuDTO;
import com.sillyhat.project.org.menu.service.MenuService;
import com.sillyhat.project.org.user.dto.UserDTO;
import com.sillyhat.project.org.user.dto.UserDetailDTO;
import com.sillyhat.project.org.user.service.UserService;
import com.sillyhat.project.sys.business.service.SystemBusinessService;
import com.sillyhat.project.webapp.common.base.BaseController;
import com.wordnik.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CommonController
 *
 * @author 徐士宽
 * @date 2017/3/30 17:27
 */
@Controller("commonController")
public class CommonController extends BaseController {


    @Value("${images.path}")
    private String imagesPath;

    @Autowired
    private UserService userService;

    @Autowired
    private SystemBusinessService systemBusinessService;

    @Autowired
    private MenuService menuService;

    /**
     * 主页(进入项目)
     * @return
     */
    @RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.POST})
    public String index() {
//        loadMenuHeader();
        String carouselImage1 = imagesPath + "homepage/79974c3d559ce57934e351adc1e3718a.jpg";
        String carouselImage2 = imagesPath + "homepage/fc3566cced02453738b83ab2afa62c18.jpg";
        String carouselImage3 = imagesPath + "homepage/b808f689be8192143d20323a1bdd0218.jpg";
        String smallImage1 = imagesPath + "homepage/IMG_1352.JPG";
        String smallImage2 = imagesPath + "homepage/IMG_1353.JPG";
        String smallImage3 = imagesPath + "homepage/IMG_1354.JPG";
        String image1 = imagesPath + "homepage/IMG_1355.JPG";
        String image2 = imagesPath + "homepage/IMG_1356.JPG";
        String image3 = imagesPath + "homepage/IMG_1358.JPG";
        setAttribute("carouselImage1",carouselImage1);
        setAttribute("carouselImage2",carouselImage2);
        setAttribute("carouselImage3",carouselImage3);
        setAttribute("smallImage1",smallImage1);
        setAttribute("smallImage2",smallImage2);
        setAttribute("smallImage3",smallImage3);
        setAttribute("image1",image1);
        setAttribute("image2",image2);
        setAttribute("image3",image3);
        setAttribute("currentPage","homePage");
        return "/homepage/index";
    }

    @ApiOperation(value = "加载首页菜单", response = String.class, notes = "加载首页菜单")
    @RequestMapping(value = "/loadMenuHeader", method = {RequestMethod.GET})
    public String loadMenuHeader() {
        UserDTO currentUser = getCurrentUser();
        long permissionId = Constants.PERMISSION_PUBLIC_MENU;
        if(currentUser != null){
            permissionId = currentUser.getId();
        }
        if(systemBusinessService.initSystemData()){
            initSystem();//初始化系统
        }
        List<MenuDTO> permissionMenuList = menuService.queryHeaderMenuByPermission(permissionId);
        setAttribute("permissionMenuList",permissionMenuList);
        return "/commonPage/menu/headerMenuList";
    }


    @RequestMapping(value = "/toHomepage", method = {RequestMethod.GET})
    public String toHomepage() {
        String carouselImage1 = imagesPath + "homepage/79974c3d559ce57934e351adc1e3718a.jpg";
        String carouselImage2 = imagesPath + "homepage/fc3566cced02453738b83ab2afa62c18.jpg";
        String carouselImage3 = imagesPath + "homepage/b808f689be8192143d20323a1bdd0218.jpg";
        String smallImage1 = imagesPath + "homepage/IMG_1352.JPG";
        String smallImage2 = imagesPath + "homepage/IMG_1353.JPG";
        String smallImage3 = imagesPath + "homepage/IMG_1354.JPG";
        String image1 = imagesPath + "homepage/IMG_1355.JPG";
        String image2 = imagesPath + "homepage/IMG_1356.JPG";
        String image3 = imagesPath + "homepage/IMG_1358.JPG";
        setAttribute("carouselImage1",carouselImage1);
        setAttribute("carouselImage2",carouselImage2);
        setAttribute("carouselImage3",carouselImage3);
        setAttribute("smallImage1",smallImage1);
        setAttribute("smallImage2",smallImage2);
        setAttribute("smallImage3",smallImage3);
        setAttribute("image1",image1);
        setAttribute("image2",image2);
        setAttribute("image3",image3);
        setAttribute("currentPage","homePage");
        return "/homepage/homepage";
    }

    @RequestMapping(value = "/toLogin", method = {RequestMethod.GET, RequestMethod.POST})
    public String toLogin() {
//        loadMenuHeader();
        setAttribute("currentPage","login");
        return "/homepage/login";
    }

    @ResponseBody
    @RequestMapping(value = "/login", method = {RequestMethod.GET, RequestMethod.POST})
    public SillyHatAJAX login(@RequestParam("loginName") String loginName, @RequestParam("password")String password) {
        Map<String,Object> resultMap = new HashMap<String,Object>();
        UserDTO user = userService.getUserByLogin(loginName);
        boolean flag = false;
        if(user != null){
            if(user.getPassword().equals(MD5Util.toMD5Upper(password))){
                UserDetailDTO detailDTO = userService.getUserDetailByUserId(user.getId());
                user.setUserDetail(detailDTO);
                setCurrentUser(user);
                flag = true;
            }
        }
        resultMap.put("flag",flag);
        return new SillyHatAJAX(resultMap);
    }

    @RequestMapping(value = "/common/error", method = {RequestMethod.GET, RequestMethod.POST})
    public String err() {
        return "/page/err";
    }

    @RequestMapping(value = "/common/resource_not_found", method = {RequestMethod.GET, RequestMethod.POST})
    public String not_found() {
        return "/page/page_no_fount";
    }
}
