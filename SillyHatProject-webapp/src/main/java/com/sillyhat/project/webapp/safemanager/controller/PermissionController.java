package com.sillyhat.project.webapp.safemanager.controller;

import com.sillyhat.project.common.dto.DataTables;
import com.sillyhat.project.common.dto.SillyHatAJAX;
import com.sillyhat.project.org.menu.dto.MenuDTO;
import com.sillyhat.project.org.menu.service.MenuService;
import com.sillyhat.project.org.role.dto.RoleDTO;
import com.sillyhat.project.org.role.service.RoleService;
import com.sillyhat.project.org.user.dto.UserDTO;
import com.sillyhat.project.org.user.service.UserService;
import com.sillyhat.project.webapp.common.base.BaseController;
import com.wordnik.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * SafeManagerController
 *
 * @author 徐士宽
 * @date 2017/5/12 10:05
 */
@Controller
@RequestMapping("/permission")
public class PermissionController extends BaseController {

    private final Logger logger = LoggerFactory.getLogger(PermissionController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private RoleService roleService;


    @ResponseBody
    @ApiOperation(value = "后台管理-系统管理-菜单列表", response = String.class, notes = "后台管理-系统管理-分页查询菜单列表")
    @RequestMapping(value = "/queryMenuByPage", method = {RequestMethod.POST},consumes = MediaType.APPLICATION_JSON_VALUE)
    public DataTables queryMenuByPage(@RequestBody DataTables dataTables) {
        return menuService.queryMenuByPage(dataTables);
    }

    @ResponseBody
    @ApiOperation(value = "后台管理-系统管理-得到全部可用菜单", response = String.class, notes = "后台管理-系统管理-得到全部可用菜单")
    @RequestMapping(value = "/queryAllMenu", method = {RequestMethod.GET})
    public SillyHatAJAX queryAllMenu() {
        return new SillyHatAJAX(menuService.queryMenuAll());
    }

    @ResponseBody
    @ApiOperation(value = "后台管理-系统管理-保存用户", response = String.class, notes = "后台管理-系统管理-保存用户")
    @RequestMapping(value = "/saveMenu", method = {RequestMethod.POST},consumes = MediaType.APPLICATION_JSON_VALUE)
    public SillyHatAJAX saveMenu(@RequestBody MenuDTO obj) {
        obj.setCreatedUser(getCurrentUser().getId());
        obj.setUpdatedUser(getCurrentUser().getId());
        if(obj.getId() != null && obj.getId() != 0l){
            menuService.updateMenu(obj);
        }else{
            menuService.addMenu(obj);
        }
        return new SillyHatAJAX(true);
    }

    @ResponseBody
    @ApiOperation(value = "后台管理-系统管理-查询菜单详情", response = String.class, notes = "后台管理-系统管理-查询菜单详情ById")
    @RequestMapping(value = "/getMenuById", method = {RequestMethod.GET})
    public SillyHatAJAX getMenuById(@RequestParam("id")String id) {
        MenuDTO dto = menuService.selectMenuByPrimaryKey(Long.parseLong(id));
        return new SillyHatAJAX(dto);
    }

    @ResponseBody
    @ApiOperation(value = "后台管理-系统管理-删除用户", response = String.class, notes = "后台管理-系统管理-删除用户")
    @RequestMapping(value = "/deleteById", method = {RequestMethod.POST},consumes = MediaType.APPLICATION_JSON_VALUE)
    public SillyHatAJAX deleteById(@RequestParam("userId")String id) {
        UserDTO dto = new UserDTO();
        dto.setId(Long.parseLong(id));
        dto.setUpdatedUser(getCurrentUser().getId());
        userService.deleteUserById(dto);
        return new SillyHatAJAX(true);
    }


    @ResponseBody
    @ApiOperation(value = "后台管理-系统管理-角色列表", response = String.class, notes = "后台管理-系统管理-分页查询角色列表")
    @RequestMapping(value = "/queryRoleByPage", method = {RequestMethod.POST},consumes = MediaType.APPLICATION_JSON_VALUE)
    public DataTables queryRoleByPage(@RequestBody DataTables dataTables) {
        return roleService.queryMenuByPage(dataTables);
    }


    @ResponseBody
    @ApiOperation(value = "后台管理-系统管理-保存角色", response = String.class, notes = "后台管理-系统管理-保存角色")
    @RequestMapping(value = "/saveRole", method = {RequestMethod.POST},consumes = MediaType.APPLICATION_JSON_VALUE)
    public SillyHatAJAX saveRole(@RequestBody RoleDTO obj) {
        obj.setCreatedUser(getCurrentUser().getId());
        obj.setUpdatedUser(getCurrentUser().getId());
        if(obj.getId() != null && obj.getId() != 0l){
            roleService.updateRole(obj);
        }else{
            roleService.addRole(obj);
        }
        return new SillyHatAJAX(true);
    }


    @ResponseBody
    @ApiOperation(value = "后台管理-系统管理-查询角色详情", response = String.class, notes = "后台管理-系统管理-查询角色详情ById")
    @RequestMapping(value = "/getRoleById", method = {RequestMethod.GET})
    public SillyHatAJAX getRoleById(@RequestParam("id")String id) {
        RoleDTO dto = roleService.selectRoleByPrimaryKey(Long.parseLong(id));
        return new SillyHatAJAX(dto);
    }
}
