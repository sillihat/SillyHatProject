package com.sillyhat.project.webapp.safemanager.controller;

import com.sillyhat.project.org.menu.dto.MenuDTO;
import com.sillyhat.project.org.menu.service.MenuService;
import com.sillyhat.project.webapp.common.base.BaseController;
import com.wordnik.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * SafeManagerController
 *
 * @author 徐士宽
 * @date 2017/5/12 10:05
 */
@Controller
@RequestMapping("/safe-manager")
public class SafeManagerController extends BaseController {

    private final Logger logger = LoggerFactory.getLogger(SafeManagerController.class);

    @Autowired
    private MenuService menuService;

    @ApiOperation(value = "进入后台管理页面", response = String.class, notes = "进入后台管理页面")
    @RequestMapping(value = "/toSafeManager", method = {RequestMethod.GET})
    public String toSafeManager() {
        return "/safeManager/mainPage/SafeManagerHome";
    }


    @ApiOperation(value = "加载后台管理端左侧菜单栏", response = String.class, notes = "加载后台管理端左侧菜单栏")
    @RequestMapping(value = "/loadSafeManagerMenu", method = {RequestMethod.GET})
    public String loadSafeManagerMenu() {
        List<MenuDTO> safeManagerHeaderMenuList = menuService.querySafeManagerHeaderMenu();
        setAttribute("safeManagerHeaderMenuList",safeManagerHeaderMenuList);
        return "/safeManager/mainPage/SafeManagerHeaderMenu";
    }



    @ApiOperation(value = "进入系统管理-用户管理", response = String.class, notes = "进入系统管理-用户管理")
    @RequestMapping(value = "/toSystemManagerUserPanel", method = {RequestMethod.GET})
    public String toSystemManagerUserPanel() {
        return "/safeManager/systemManager/userPanel";
    }

    @ApiOperation(value = "进入系统管理-菜单管理", response = String.class, notes = "进入系统管理-菜单管理")
    @RequestMapping(value = "/toSystemManagerMenuPanel", method = {RequestMethod.GET})
    public String toSystemManagerMenuPanel() {
        return "/safeManager/systemManager/menuPanel";
    }

    @ApiOperation(value = "进入系统管理-角色管理", response = String.class, notes = "进入系统管理-角色管理")
    @RequestMapping(value = "/toSystemManagerRolePanel", method = {RequestMethod.GET})
    public String toSystemManagerRolePanel() {
        return "/safeManager/systemManager/rolePanel";
    }


    @ApiOperation(value = "进入系统管理-权限管理", response = String.class, notes = "进入系统管理-权限管理")
    @RequestMapping(value = "/toSystemManagerPermissionPanel", method = {RequestMethod.GET})
    public String toSystemManagerPermissionPanel() {
        return "/safeManager/systemManager/permissionPanel";
    }


    @ApiOperation(value = "进入后台管理页面1", response = String.class, notes = "进入后台管理页面1")
    @RequestMapping(value = "/toTestSafeManager1", method = {RequestMethod.GET})
    public String toTestSafeManager1() {
        return "/safeManager/SafeManagerMain1";
    }
    @ApiOperation(value = "进入后台管理页面2", response = String.class, notes = "进入后台管理页面2")
    @RequestMapping(value = "/toTestSafeManager2", method = {RequestMethod.GET})
    public String toTestSafeManager2() {
        return "/safeManager/SafeManagerMain2";
    }
    @ApiOperation(value = "进入表格页面", response = String.class, notes = "进入表格页面")
    @RequestMapping(value = "/toTestSafeManagerTable1", method = {RequestMethod.GET})
    public String toTestSafeManagerTable1() {
        return "/safeManager/SafeManagerTable1";
    }

}
