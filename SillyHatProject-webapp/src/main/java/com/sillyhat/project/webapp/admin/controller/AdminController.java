package com.sillyhat.project.webapp.admin.controller;

import com.sillyhat.project.org.user.dto.UserDTO;
import com.sillyhat.project.org.user.service.UserService;
import com.sillyhat.project.webapp.common.base.BaseController;
import com.wordnik.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

import javax.annotation.Resource;

@RequestMapping("admin")
@Controller("adminController")
public class AdminController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(AdminController.class);

    private UserService userService;


    @RequestMapping(value = "/check_username/{id}", method = RequestMethod.GET)
    @ResponseBody
    public UserDTO checkUsername(@PathVariable("id") Long id) {
        List<UserDTO> list = userService.queryAllUser();
        logger.info("params is {}",id);
        return list.get(0);
    }

    @RequestMapping(value = "/list", method = {RequestMethod.GET, RequestMethod.POST})
    public String add(ModelMap model) {
        model.addAttribute("roles", "admin");
        return "/admin/member/list";
    }

    @ApiOperation(value = "登录系统", response = String.class, notes = "登录系统")
    @RequestMapping(value = "/login", method = {RequestMethod.GET, RequestMethod.POST})
    public String login() {
        return "/login";
    }
    @ApiOperation(value = "登录成功进入主界面", response = String.class, notes = "登录成功进入主界面")
    @RequestMapping(value = "/main", method = {RequestMethod.GET, RequestMethod.POST})
    public String main(){
        return "/admin/main";
    }
}
