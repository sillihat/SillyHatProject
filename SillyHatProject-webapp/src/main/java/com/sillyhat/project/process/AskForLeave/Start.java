package com.sillyhat.project.process.AskForLeave;

import com.sillyhat.project.activiti.config.exception.BPMException;
import com.sillyhat.project.activiti.process.headler.HeadlerAdapter;

import org.springframework.context.ApplicationContext;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * Start
 *
 * @author 徐士宽
 * @date 2017/5/11 10:09
 */
public class Start extends HeadlerAdapter {

    @Override
    public void executeApplyBefore(ApplicationContext context, HttpServletRequest request, Map<String, Object> params) throws BPMException {
        super.executeApplyBefore(context, request, params);
        request.setAttribute("AskForLeave","Start executeApplyBefore");
    }

    @Override
    public void executeApplyAfter(ApplicationContext context, HttpServletRequest request, Map<String, Object> params) throws BPMException {
        super.executeApplyAfter(context, request, params);
    }

    @Override
    public void executeSubmitBefore(ApplicationContext context, HttpServletRequest request, Map<String, Object> params) throws BPMException {
        super.executeSubmitBefore(context, request, params);
    }

    @Override
    public void executeSubmitAfter(ApplicationContext context, HttpServletRequest request, Map<String, Object> params) throws BPMException {
        super.executeSubmitAfter(context, request, params);
    }
}
