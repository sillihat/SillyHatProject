$(function () {
    var _aoData = {
        draw:1,
        start:0,
        length:20
    };
    $('#tablePanel').DataTable({
        deferRender:true,//当处理大数据时，延迟渲染数据，有效提高Datatables处理能力
        processing:true, //隐藏加载提示,自行处理
        // bProcessing:false, //隐藏加载提示,自行处理
        serverSide:true, //启用服务器端分页
        pagingType:"full_numbers",//simple:上一页，下一页；simple_numbers：上下+页数；full：上下首末；full_numbers上下首末+页数
        oLanguage: {
        sLengthMenu: "每页显示 _MENU_ 条记录",
            sZeroRecords: "没有检索到数据",
            sInfoEmpty: "没有检索到数据",
            sInfo: "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
            sInfoFiltered: "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
            sProcessing:"-----加载中-----",
            // "sProcessing": "<img src='/images/datatable_loading.gif'>  努力加载数据中.",
            oPaginate: {
            sFirst: "首页",
                sPrevious: "前一页",
                sNext: "后一页",
                sLast: "尾页",
                sJump:"跳转"
            },
            sProcessing:"加载中......",//加载时
        },
        rowCallback:function(row, data) { //data是后端返回的数据
           if(data.isDelete == 0) {
               // $('td:eq(3)', row).html('<a target="_blank" href=http://' + data.isDelete + '>' + data.isDelete + '</a>');
               $('td:eq(3)', row).html("未删除")
           }else{
               $('td:eq(3)', row).html("已删除")
           }
           if(data.isAdministrators == 0) {
               $('td:eq(4)', row).html("用户")
           }else{
               $('td:eq(4)', row).html("超管")
           }
        },
        sLoadingRecords:"载入中......",//加载时
        lengthMenu:[20,30,50,100], //更改显示记录数选项
        iDisplayLength:20, //默认显示的记录数
        fnServerData:retrieveData,
        columns:[
            {
                "data":null,
                "createdCell": function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).html((iRow + 1));
                }//索引列
            },
            {"data":"login"},
            {"data":"userName"},
            {"data":"isDelete"},
            {"data":"isAdministrators"},
            {"data":"createdUser"},
            {"data":"createdDate"}
        ]
    });

    function retrieveData(sSource,aoData,fnCallback){
        $.ajax({
            url:sillyhatBase+"/user/queryUserPage",
            type:"post",
            dataType:"json",
            contentType:"application/json;charset=UTF-8",
            data:JSON.stringify(_aoData),
            // data:function(result){
            //     // data:{"student.studentId":aaa,"student.studentName":bbb,"user.userId":bbb ,"user.userName":ccc}
            //     return JSON.stringify(result);
            //     return result;
            //     return JSON.stringify({dataTables.draw:1,"dataTables.start":1,"dataTables.length":20});
            // },//后台参数
            error:function (resp){
                alert(JSON.stringify(resp));
            },
            // dataSrc:function(result){
            //     //因为返回标准格式为如下方式，但大部分为非标准格式，所以这里定义，返回json对象中，标准格式的list
            //     // "data": [
            //     //     {"name": "AAA", "position": "BBB"},
            //     //     ……
            //     // ]
            //     //封装返回数据
            //     var returnData = {};
            //     returnData.draw = result.data.draw;//这里直接自行返回了draw计数器,应该由后台返回
            //     returnData.recordsTotal = result.data.recordsTotal;//返回数据全部记录
            //     returnData.recordsFiltered = result.data.recordsTotal;//后台不实现过滤功能，每次查询均视作全部结果
            //     returnData.data = result.data.resultList;//返回的数据列表
            //     return result.data.resultList;
            // },
            success:function(result){
                fnCallback(result);
            }
        })
    }
});