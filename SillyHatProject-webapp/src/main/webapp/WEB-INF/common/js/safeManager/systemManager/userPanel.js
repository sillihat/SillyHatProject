$(function () {
    // $('#userBirthday').datepicker({
    //     autoclose: true
    // });
    // $('#entryDate').datepicker({
    //     autoclose: true
    // });
    // $('#leaveDate').datepicker({
    //     autoclose: true
    // });
	sillyhatwin.ui.datatable.getInstans("shTablePanel",{
        // sillyhatSortColumns:false,//全部可排序
        // sillyhatSortColumns:[0,1,2],//1,2,3列不可排序
        columns:[
            {
                "data":null,
                "createdCell": function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).html((iRow + 1));
                }//索引列
            },
            {"data":"login","name":"LOGIN","searchable":true},
            {"data":"userDetail.userName","name":"USER_NAME","searchable":true},
            {"data":"userDetail.userGender","name":"IS_DELETE","searchable":true},
            {"data":"userDetail.userPhone","name":"IS_ADMINISTRATORS"},
            {"data":"userDetail.isJob","name":"IS_ADMINISTRATORS"},
	        {"data":"updatedUserName"},
	        {"data":"updatedDate"},
	        {
		        "data":null,
		        "createdCell": function (nTd, sData, oData, iRow, iCol) {
		        	var _operatorHtml = "<a href='javascript:void(0);' data-toggle='modal' data-target='#shTableModal' data-id='"+oData.id+"'>编辑</a>&nbsp;&nbsp;" +
				        "<a href='javascript:void(0);' onclick='fnDelete(\""+oData.id+"\")'>删除</a>";
			        $(nTd).html(_operatorHtml);
		        }//操作列
	        },
        ],
	    fnInitComplete:initComplete,
        fnRowCallbackFormat:rowCallback,
	    searchDataTables:[
            {
                eleId:"btnSearch",//绑定点击查询组件
                fnSearchDataTables:searchDataTables//自定义回调函数，可不定义
            }
        ],
        fnQueryServerData:function (paramsData,fnCallback){
	        paramsData.searchParams.login = $("#searchIntLogin").val();
	        paramsData.searchParams.userName = $("#searchIntUserName").val();
	        paramsData.searchParams.userGender = $("#searchIntUserGender").val();
	        paramsData.searchParams.isJob = $("#searchIntIsJob").val();
            $.ajax({
               url:sillyhatBase+"/user/queryUserPage",
               type:"post",
               dataType:"json",
               contentType:"application/json;charset=UTF-8",
               data:JSON.stringify(paramsData),
               error:function (resp){
                   alert(JSON.stringify(resp));
               },
               success:function(result){
                   fnCallback(result);
               }
           })
        }
    });
	
	sillyhatwin.ui.modal.getInstans("shTableModal",{
		// isScroll:false,//是否包含滚动条
		initDatepicker:true,//初始化日期选择器
		showModalBefore:{
			paramsKey:[
				"id"
			],//返回值params对象，传值方式data-id=""，返回params.id
			fnShowModalBefore:function (event,params) {
				if(params!=null){
					if(null != params.id && ""!= params.id){
						$("#addLabel").hide();
						$("#updateLabel").show();
						$.ajax({
						  url:sillyhatBase+"/user/getUserById",
						  type:"GET",
						  dataType:"json",//返回json自动转化为object
						  // contentType:"application/json;charset=UTF-8",
						  data:{userId:params.id},
						  error:function (resp){
						      alert(JSON.stringify(resp));
						  },
						  success:function(result){
							  $("#userId").val(result.data.id);
							  $("#login").val(result.data.login);
							  $("#password").val(result.data.password);
							  $("#userName").val(result.data.userDetail.userName);
							  $("#userGender").val(result.data.userDetail.userGender);
							  $("#userAge").val(result.data.userDetail.userAge);
							  $("#identityCard").val(result.data.userDetail.identityCard);
							  $("#userBirthday").val(result.data.userDetail.userBirthday);
							  $("#userPhone").val(result.data.userDetail.userPhone);
							  $("#userEmail").val(result.data.userDetail.userEmail);
							  $("#userIcon").val(result.data.userDetail.userIcon);
							  // $("#companyId").val(result.data.userDetail.companyId);
							  // $("#departmentId").val(result.data.userDetail.departmentId);
							  $("#isJob").val(result.data.userDetail.isJob);
							  $("#entryDate").val(result.data.userDetail.entryDate);
							  $("#leaveDate").val(result.data.userDetail.leaveDate);
							  $("#isAdministrators").val(result.data.isAdministrators);
						  }
						})
					}else{
						$("#addLabel").show();
						$("#updateLabel").hide();
					}
				}
			}
		}
	});
});

/**
 * 初始化top组件
 * settings  table设置方案
 * result    查询返回object
 */
function initComplete(settings, dataTable) {
	// $("#sillyhatDataTablesTools").append('<button id="datainit" type="button" class="btn btn-primary btn-sm">增加基础数据</button>&nbsp');
	// $("#sillyhatDataTablesTools").append('<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal">添加</button>');
}

/**
 * 绑定查询事件，回调函数
 * @param settings
 * @param dataTable
 */
function searchDataTables(settings,dataTable) {
	// alert("searchDataTables");
	return true;//true:执行后续查询；false:不执行后续查询
}

function rowCallback(row, data) { //data是后端返回的数据
	if(data.userDetail.userGender == 0) {
		// $('td:eq(3)', row).html('<a target="_blank" href=http://' + data.isDelete + '>' + data.isDelete + '</a>');
		$('td:eq(3)', row).html("未知")
	}else if(data.userDetail.userGender == 1) {
		$('td:eq(3)', row).html("男")
	}else if(data.userDetail.userGender == 2) {
		$('td:eq(3)', row).html("女")
	}
	if(data.userDetail.isJob == 1) {
		$('td:eq(5)', row).html("在职")
	}else if(data.userDetail.isJob == 0) {
		$('td:eq(5)', row).html("离职")
	}
}

function fnSave() {
	var attributesArray = document.getElementById("leaveDate").attributes;
	for(var i = 0; i < attributesArray.length; i++){
		console.log(attributesArray[i].nodeName + ' : ' + attributesArray[i].nodeValue);
	}
	// if(sillyhatwin.ui.modal.dataVerify("shTableModal")){
	// $.ajax({
     //   url:sillyhatBase+"/user/addUser",
     //   type:"post",
     //   dataType:"json",
     //   contentType:"application/json;charset=UTF-8",
     //   data:JSON.stringify(paramsData),
     //   error:function (resp){
	//        alert(JSON.stringify(resp));
     //   },
     //   success:function(result){
	//        fnCallback(result);
     //   }
	// })
	// sillyhatwin.ui.modal.close("shTableModal");
	// refresh();
	// }
}

function fnDelete(id) {
	
}

function refresh() {
	sillyhatwin.ui.datatable.refresh("shTablePanel");
}