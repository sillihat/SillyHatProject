function changeSafeManagerContentURL(url) {
    $.ajax({
       type:"GET",
       url: url,
       dataType:"html",
       async:false,
       success: function(data){
           $("#safeManagerContent").empty();
           $("#safeManagerContent").html(data);
       }
    });
}