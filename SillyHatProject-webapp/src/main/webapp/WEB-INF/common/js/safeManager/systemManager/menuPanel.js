$(function () {
	$("#menuType").select2({
       language: "zh-CN",
       width:"100%",
       height:"100%",
       placeholder: "----------",
       allowClear:true,
       dropdownParent:$("#shTableModal")
	});
    // $("#menuType").prop("disabled", false);//可用
    // $("#menuType").prop("disabled", true);//不可用
    
	$("#menuParentId").select2({
       language: "zh-CN",
       width:"100%",
       height:"100%",
       placeholder: "----------",
       allowClear:true,
       dropdownParent:$("#shTableModal"),
       selectOnClose:true,
       ajax:{
			url:sillyhatBase+"/permission/queryAllMenu",
			type:"get",
			dataType:"json",
			cache:false,
	       // delay: 250,
	       // data: function (params) {
		    //    return {
			 //       q: params.term, // search term
			 //       page: params.page
		      //  };
	       // },
	       // processResults: function (data, params) {
			processResults: function (data){
				var resultArray = [];
				for(var i = 0;i < data.data.length;i++){
                    resultArray[i]={id:data.data[i].id,text:data.data[i].menuCode}
				}
				return {results:resultArray};
			}
       }
	});
	sillyhatwin.ui.datatable.getInstans("shTablePanel",{
        columns:[
            {
                "data":null,
                "createdCell": function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).html((iRow + 1));
                }//索引列
            },
            {"data":"menuCode"},
            {"data":"menuName"},
            {"data":"menuParentCode"},
            {"data":"menuType"},
            {"data":"isDisabled"},
            {"data":"isDelete"},
	        {"data":"updatedUserName"},
	        {"data":"updatedDate"},
	        {
		        "data":null,
		        "createdCell": function (nTd, sData, oData, iRow, iCol) {
		        	var _operatorHtml = "<a href='javascript:void(0);' data-toggle='modal' data-target='#shTableModal' data-id='"+oData.id+"'>编辑</a>&nbsp;&nbsp;" +
				        "<a href='javascript:void(0);' onclick='fnDelete(\""+oData.id+"\")'>删除</a>";
			        $(nTd).html(_operatorHtml);
		        }//操作列
	        },
        ],
	    fnInitComplete:initComplete,
        fnRowCallbackFormat:rowCallback,
	    searchDataTables:[
            {
                eleId:"btnSearch",//绑定点击查询组件
                fnSearchDataTables:searchDataTables//自定义回调函数，可不定义
            }
        ],
        fnQueryServerData:function (paramsData,fnCallback){
	        paramsData.searchParams.login = $("#searchIntLogin").val();
	        paramsData.searchParams.userName = $("#searchIntUserName").val();
	        paramsData.searchParams.userGender = $("#searchIntUserGender").val();
	        paramsData.searchParams.isJob = $("#searchIntIsJob").val();
            $.ajax({
               url:sillyhatBase+"/permission/queryMenuByPage",
               type:"post",
               dataType:"json",
               contentType:"application/json;charset=UTF-8",
               data:JSON.stringify(paramsData),
               error:function (resp){
                   alert(JSON.stringify(resp));
               },
               success:function(result){
                   fnCallback(result);
               }
           })
        }
    });
	
	sillyhatwin.ui.modal.getInstans("shTableModal",{
		// isScroll:false,//是否包含滚动条
		initDatepicker:true,//初始化日期选择器
		showModalBefore:{
			paramsKey:[
				"id"
			],//返回值params对象，传值方式data-id=""，返回params.id
			fnShowModalBefore:function (event,params) {
				if(params!=null){
					if(null != params.id && ""!= params.id){
						$("#addLabel").hide();
						$("#updateLabel").show();
						$.ajax({
						  url:sillyhatBase+"/permission/getMenuById",
						  type:"GET",
						  dataType:"json",//返回json自动转化为object
						  data:{id:params.id},
						  error:function (resp){
						      alert(JSON.stringify(resp));
						  },
						  success:function(result){
							  $("#id").val(result.data.id);
							  $("#menuCode").val(result.data.menuCode);
							  $("#menuName").val(result.data.menuName);
							  $("#menuLink").val(result.data.menuLink);
							  $("#menuType").val(result.data.menuType);
							  $("#menuIcon").val(result.data.menuIcon);
                              // $example.val("CA").trigger("change");
							  $("#menuParentId").val(result.data.menuParentCode).trigger("change");;
							  $("#menuSort").val(result.data.menuSort);
							  $("#isDisabled").val(result.data.isDisabled);
							  $("#isDelete").val(result.data.isDelete);
						  }
						})
					}else{
						$("#addLabel").show();
						$("#updateLabel").hide();
					}
				}
			}
		}
	});
});

/**
 * 初始化top组件
 * settings  table设置方案
 * result    查询返回object
 */
function initComplete(settings, dataTable) {
	// $("#sillyhatDataTablesTools").append('<button id="datainit" type="button" class="btn btn-primary btn-sm">增加基础数据</button>&nbsp');
	// $("#sillyhatDataTablesTools").append('<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal">添加</button>');
}

/**
 * 绑定查询事件，回调函数
 * @param settings
 * @param dataTable
 */
function searchDataTables(settings,dataTable) {
	// alert("searchDataTables");
	return true;//true:执行后续查询；false:不执行后续查询
}

function rowCallback(row, data) { //data是后端返回的数据
	if(data.menuType == 1) {
		$('td:eq(4)', row).html("一级菜单")
	}else if(data.menuType == 2) {
		$('td:eq(4)', row).html("二级菜单")
	}else if(data.menuType == 3) {
		$('td:eq(4)', row).html("三级菜单")
	}else if(data.isDelete == 4) {
		$('td:eq(4)', row).html("四级菜单")
	}
	if(data.isDelete == 0) {
		$('td:eq(5)', row).html("未禁用")
	}else if(data.isDelete == 1) {
		$('td:eq(5)', row).html("已禁用")
	}
	if(data.isDelete == 0) {
		$('td:eq(6)', row).html("未删除")
	}else if(data.isDelete == 1) {
		$('td:eq(6)', row).html("已删除")
	}
	if(data.isDelete == 0) {
		$('td:eq(6)', row).html("未删除")
	}else if(data.isDelete == 1) {
		$('td:eq(6)', row).html("已删除")
	}
}

function fnSave() {
    var paramsData = {
        id:$("#menuId").val(),
        menuCode:$("#menuCode").val(),
        menuName:$("#menuName").val(),
        menuLink:$("#menuLink").val(),
        menuType:$("#menuType").val(),
        menuIcon:$("#menuIcon").val(),
        menuParentId:$("#menuParentId").val(),
        menuSort:$("#menuSort").val(),
        isDisabled:$("#isDisabled").val(),
        isDelete:$("#isDelete").val()
	}
	$.ajax({
		url:sillyhatBase+"/permission/saveMenu",
		type:"post",
		dataType:"json",
		contentType:"application/json;charset=UTF-8",
		data:JSON.stringify(paramsData),
		error:function (resp){
			alert(JSON.stringify(resp));
		},
		success:function(result){
			if(result.data){
                alert("保存成功");
                sillyhatwin.ui.modal.close("shTableModal");
                refresh();
			}else{
				alert("保存失败");
			}
		}
	})
}

function fnDelete(id) {
	
}

function refresh() {
	sillyhatwin.ui.datatable.refresh("shTablePanel");
}