sillyhatwin.ui.common = {
	/**
	 * 判断是否为数组
	 * @param obj   判断对象
	 * @returns {boolean}   true:是数组
	 */
	isArray : function(obj) {
		return Object.prototype.toString.call(obj) === '[object Array]';
	}
}