/**
 * DataTable封装处理
 */
sillyhatwin.ui.datatable = {
    getInstans : function(elementId,args) {
        var sillyhatDataTable;
        var options = {};
        $(document).ready(function() {
            if (typeof(args) == "undefined") {
                args = {};
            }
            if (typeof(args.deferRender) == "undefined") {
	            options.deferRender =
		            args.deferRender = true;//当处理大数据时，延迟渲染数据，有效提高Datatables处理能力
            }
            if (typeof(args.serverSide) == "undefined") {
                args.serverSide = true;//开启服务器模式，默认是关闭
            }
            if (typeof(args.processing) == "undefined") {
                args.processing = true;//是否开启本地分页，默认是开启
            }
            if (typeof(args.columns) == "undefined") {
                args.columns = true;//报表列
            }
            if (typeof(args.sillyhatSortColumns) == "undefined") {
                args.bSort = false;//默认不可排序
                args.aoColumnDefs = [{"bSortable": true,"aTargets":[0]}];//不启用排序过滤
            }else if(typeof(args.sillyhatSortColumns) == "boolean") {
                args.bSort = args.sillyhatSortColumns;//全部可排序
                args.aoColumnDefs = [{"bSortable": true,"aTargets":[0]}];//不启用排序过滤
            }else if(typeof(args.sillyhatSortColumns) == "object") {
                args.bSort = true;//可排序
                args.aoColumnDefs = [{"bSortable": false,"aTargets":args.sillyhatSortColumns}];//启用排序过滤//[{"bSortable": false,"aTargets":[0,1,2]}]
            }
            if (typeof(args.searching) == "undefined") {
                args.searching = true;//搜索框，默认是开启
            }
            if (typeof(args.lengthChange) == "undefined") {
                args.lengthChange = true;//是否允许用户改变表格每页显示的记录数，默认是开启
            }
            if (typeof(args.paging) == "undefined") {
                args.paging = true;//是否开启本地分页，默认是开启
            }
            if (typeof(args.scrollY) == "undefined") {
                args.scrollY = "100%";//设置高
            }
            if (typeof(args.scrollX) == "undefined") {
                args.scrollX = "100%";//设置宽度
            }
            if (typeof(args.scrollXInner) == "undefined") {
                args.scrollXInner = "100%";//设置内宽度
            }
            if (typeof(args.scrollCollapse) == "undefined") {
                args.scrollCollapse = true;//是否开启DataTables的高度自适应，当数据条数不够分页数据条数的时候，插件高度是否随数据条数而改变
            }
            if (typeof(args.jQueryUI) == "undefined") {
                args.jQueryUI = false;//jquery 风格
            }
            if (typeof(args.autoWidth) == "undefined") {
                args.autoWidth = true;//是否自适应宽度
            }
            if (typeof(args.pagingType) == "undefined") {
                args.pagingType = "full_numbers";//simple:上一页，下一页；simple_numbers：上下+页数；full：上下首末；full_numbers上下首末+页数
            }
            if (typeof(args.lengthMenu) == "undefined") {
                args.lengthMenu = [20,30,50,100];//更改显示记录数选项
            }
            if (typeof(args.iDisplayLength) == "undefined") {
                args.iDisplayLength = 20;//默认显示的记录数
            }
            if (typeof(args.rowCallbackFormat) == "undefined") {
                args.rowCallbackFormat = function(row, data) {
                    //data是后端返回的数据
                }
            }
            sillyhatDataTable = $('#'+elementId).DataTable({
               deferRender:args.deferRender,//当处理大数据时，延迟渲染数据，有效提高Datatables处理能力
               // bProcessing:false,//隐藏加载提示,自行处理
               serverSide:args.serverSide,//开启服务器模式，默认是关闭
               processing:args.processing,//是否显示中文提示
               searching:args.searching,//搜索框，默认是开启
               lengthChange:args.lengthChange,//是否允许用户改变表格每页显示的记录数，默认是开启
               paging:args.paging,//是否开启本地分页，默认是开启
               scrollY:args.scrollY,//设置高
               scrollX:args.scrollX,//设置宽度
               scrollXInner:args.scrollXInner,//设置内宽度
               scrollCollapse:args.scrollCollapse,//是否开启DataTables的高度自适应，当数据条数不够分页数据条数的时候，插件高度是否随数据条数而改变
               jQueryUI:args.jQueryUI,//jquery 风格
               autoWidth:args.autoWidth,//是否自适应宽度
               // columnDefs:elo.status,//列表状态
               pagingType:args.pagingType,//simple:上一页，下一页；simple_numbers：上下+页数；full：上下首末；full_numbers上下首末+页数
               language:SillyHatDataTableCommon.language.zh_cn,
               lengthMenu:args.lengthMenu,//更改显示记录数选项
               iDisplayLength:args.iDisplayLength,//默认显示的记录数
               // bSort:true,
               bSort:args.bSort,//false:不可排序，true:全部可排序
               aoColumnDefs:args.aoColumnDefs,//是否启用排序过滤 eg:[{"bSortable": false,"aTargets":[0,1,2]}]启用过滤，并0,1,2,列不可排序
               // aoColumnDefs:[{"bSortable": false,"aTargets":[0,1,2]}],//是否启用排序过滤 eg:[{"bSortable": false,"aTargets":[0,1,2]}]启用过滤，并0,1,2,列不可排序
               // columnDefs:[{
               //     orderable:false,//禁用排序
               //     targets:0   //指定的列
               // }],
               // aoColumnDefs:[{"bSortable": false,"aTargets":[0,1,2]}],
               columns:args.columns,//字段
               fnInitComplete: function (oSettings, json) {
                   $('<a href="#myModal" id="addFun" class="btn btn-primary" data-toggle="modal">新增</a>' + '&nbsp;' +
                     '<a href="#" class="btn btn-primary" id="editFun">修改</a> ' + '&nbsp;' +
                     '<a href="#" class="btn btn-danger" id="deleteFun">删除</a>' + '&nbsp;').appendTo($('.myBtnBox'));
               },
               rowCallback:args.rowCallbackFormat,
               // fnServerData:args.queryServerData
               fnServerData:function (sSource,aoData,fnCallback){
                   if (typeof(args.queryServerData) == "function") {
                       var paramsData = {};
                       for(var i = 0;i < aoData.length;i++){
                           var aoObject = aoData[i];
                           if(aoObject.name == 'draw'){
                               paramsData.draw = aoObject.value;
                           }else if(aoObject.name == 'start'){
                               paramsData.start = aoObject.value;
                           }else if(aoObject.name == 'length'){
                               paramsData.length = aoObject.value;
                           }else if(aoObject.name == 'columns'){
                               paramsData.columns = aoObject.value;
                           }else if(aoObject.name == 'order'){
                               paramsData.order = aoObject.value;
                           }else if(aoObject.name == 'search'){
                               paramsData.search = aoObject.value;
                           }
                       }
                       args.queryServerData(paramsData,fnCallback);//执行回调函数
                   }
               }
            });
        });
        return {
            sillyhatDataTableId : elementId,
            sillyhatDataTable:sillyhatDataTable
        };
    }
}

SillyHatDataTableCommon = {
    language : {
        zh_cn : {
            paginate:{
                first:'首页',
                last:'末页',
                previous:'上一页',
                next:'下一页',
                jump:"跳转"
            },
            lengthMenu:'显示 _MENU_ 条记录',
            zeroRecords:'没有检索到数据',
            info:'从 _START_ 到 _END_ /共 _TOTAL_ 条数据',
            infoEmpty:'没有检索到数据',
            search:'查找：',
            processing:'正在加载数据，请稍候...',
            // "sProcessing": "<img src='/images/datatable_loading.gif'>  努力加载数据中.",
            infoFiltered:'共计 _TOTAL_ 条记录'
        }
    }
}


var SillyHatDataTable = function(){
    //汉化分页条
    var language = {
        paginate:{
            first:'首页',
            last:'末页',
            previous:'上一页',
            next:'下一页',
            jump:"跳转"
        },
        lengthMenu:'显示 _MENU_ 条记录',
        zeroRecords:'没有检索到数据',
        info:'从 _START_ 到 _END_ /共 _TOTAL_ 条数据',
        infoEmpty:'没有检索到数据',
        search:'快速过滤：',
        processing:'正在加载数据，请稍候...',
        // "sProcessing": "<img src='/images/datatable_loading.gif'>  努力加载数据中.",
        infoFiltered:'共计 _TOTAL_ 条记录'
    }
}