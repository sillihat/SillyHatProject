<!DOCTYPE html><!--HTML5写法 -->
<head xmlns="http://www.w3.org/1999/html">
[#include "commonPage/jscss.ftl" /]
</head>
<body>
[#include "commonPage/header.ftl" /]
<div class="sillyhat-bloc-blog-masthead blog-masthead" style="padding-top: 4rem">
    <div class="sillyhat-bloc-container container">
        <nav class="sillyhat-bloc-blog-nav blog-nav">
            <a class="sillyhat-bloc-blog-nav-item active" href="#">${message("menu.learning.word.learning")}</a>
            <a class="sillyhat-bloc-blog-nav-item" href="#">${message("menu.learning.word.wordBook")}</a>
            <a class="sillyhat-bloc-blog-nav-item" href="#">${message("menu.learning.word.myWordRepository")}</a>
            <a class="sillyhat-bloc-blog-nav-item" href="#">${message("menu.learning.word.wordSet")}</a>
        </nav>
    </div>
</div>
[#include "commonPage/footer.ftl" /]
</body>
</html>