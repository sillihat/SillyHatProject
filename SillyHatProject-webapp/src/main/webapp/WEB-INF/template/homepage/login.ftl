<!DOCTYPE html><!--HTML5写法 -->
<head xmlns="http://www.w3.org/1999/html">
[#include "/commonPage/jscss.ftl" /]
<script src="${base}/common/js/homepage/login.js" type="text/javascript"></script>
</head>
<body>
[#include "/commonPage/header.ftl" /]
<div id="sillyhatLogin" class="container">
    <div class="login-container-content">
        <div class="login-container-content-carousel">
            <div id="sillyhatCarousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#sillyhatCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#sillyhatCarousel" data-slide-to="1"></li>
                    <li data-target="#sillyhatCarousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="first-slide" src="http://192.168.233.133:18081/images/homepage/IMG_1352.JPG" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="second-slide" src="http://192.168.233.133:18081/images/homepage/IMG_1353.JPG" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="third-slide" src="http://192.168.233.133:18081/images/homepage/IMG_1354.JPG" alt="Third slide">
                    </div>
                </div>
                [#--<a class="carousel-control-prev" href="#sillyhatCarousel" role="button" data-slide="prev">--]
                    [#--<span class="carousel-control-prev-icon" aria-hidden="true"></span>--]
                    [#--<span class="sr-only">Previous</span>--]
                [#--</a>--]
                [#--<a class="carousel-control-next" href="#sillyhatCarousel" role="button" data-slide="next">--]
                    [#--<span class="carousel-control-next-icon" aria-hidden="true"></span>--]
                    [#--<span class="sr-only">Next</span>--]
                [#--</a>--]
            </div>
        </div>
        <div class="login-container-content-form">
            <form class="form-signin" method="post" action="${base}/login">
                <h5 class="form-signin-heading">${message("lbl.welcome")}</h5>
                [#--<label for="inputEmail" class="sr-only">${message("lbl.login")}</label>--]
                <input type="text" id="loginName" name="loginName" class="form-control" placeholder="${message("lbl.login")}" required autofocus>
                [#--<label for="inputPassword" class="sr-only">${message("lbl.password")}</label>--]
                <input type="password" id="password" name="password" class="form-control" placeholder="${message("lbl.password")}" required>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> ${message("lbl.remember")}
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="button" onclick="login()">${message("btn.login")}</button>
            </form>
        </div>
    </div>
</div>
[#include "/commonPage/footer.ftl" /]
</body>
</html>