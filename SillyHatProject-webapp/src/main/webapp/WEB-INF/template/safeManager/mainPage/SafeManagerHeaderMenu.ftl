<!-- 左侧菜单导航 -->
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="${sillyhatImagePath}/manager/user/icon/0E3B073CB6B84539B594643B2C79E48C/C73D6CC4984245A1ADAEA934AC591F5B.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>Alexander Pierce</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                <i class="fa fa-search"></i>
            </button>
          </span>
        </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="header">${message("menu.manager.main.navigation")}</li>
        [#list safeManagerHeaderMenuList as safeManagerMenuBean]
            <li class="active treeview">
                <a class="nav-link [#if (1 == menuBean.isDisabled)]disabled[/#if] [#if (menuBean.menuCode == currentPage)]active[/#if]" href="${base}${menuBean.menuLink}">${message(menuBean.menuName)}</a>
                <a href="#">
                    <i class="fa fa-table"></i>
                    <span>${message(menuBean.menuName)}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                [#list safeManagerMenuBean.childMenus as childMenuBean]
                    <ul class="treeview-menu">
                        <li class="active">
                            <a href="${base}${childMenuBean.menuLink}"><i class="fa fa-circle-o"></i>
                            ${message(childMenuBean.menuName)}
                            </a>
                        </li>
                    </ul>
                [/#list]
            </li>
        [/#list]
        <li class="active treeview">
            <a href="#">
                <i class="fa fa-table"></i>
                <span>${message("menu.manager.system.manager")}</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="active">
                [#--<a href="javascript:void(0)" onclick="changeSafeManagerContentURL('${base}/safe-manager/toSystemManagerUserPanel')"><i class="fa fa-circle-o"></i>--]
                    <a href="${base}/safe-manager/toSystemManagerUserPanel"><i class="fa fa-circle-o"></i>
                    ${message("menu.manager.system.manager.user")}
                    </a>
                </li>
                <li>
                    <a href="${base}/safe-manager/toSystemManagerMenuPanel"><i class="fa fa-circle-o"></i>
                    ${message("menu.manager.system.manager.menu")}
                    </a>
                </li>
                <li>
                    <a href="${base}/safe-manager/toSystemManagerRolePanel"><i class="fa fa-circle-o"></i>
                    ${message("menu.manager.system.manager.role")}
                    </a>
                </li>
                <li>
                    <a href="${base}/safe-manager/toSystemManagerPermissionPanel"><i class="fa fa-circle-o"></i>
                    ${message("menu.manager.system.manager.permission")}
                    </a>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-dashboard"></i>
                <span>Dashboard</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="active">
                    <a href="index.html">
                        <i class="fa fa-circle-o"></i>
                        Dashboard v1
                    </a>
                </li>
                <li>
                    <a href="index2.html">
                        <i class="fa fa-circle-o"></i>
                        Dashboard v2
                    </a>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Layout Options</span>
                <span class="pull-right-container">
                        <span class="label label-primary pull-right">4</span>
                    </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="pages/layout/top-nav.html"><i class="fa fa-circle-o">
                    </i>
                        Top Navigation
                    </a>
                </li>
                <li>
                    <a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i>
                        Boxed
                    </a>
                </li>
                <li>
                    <a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i>
                        Fixed
                    </a>
                </li>
                <li>
                    <a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i>
                        Collapsed Sidebar
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</section>
<!-- /.sidebar -->