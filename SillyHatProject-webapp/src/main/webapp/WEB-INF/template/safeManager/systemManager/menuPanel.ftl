<!DOCTYPE html><!--HTML5写法 -->
<head xmlns="http://www.w3.org/1999/html">
[#include "commonPage/jscssManager.ftl" /]
[#include "commonPage/jscssDataTables.ftl" /]
    <script src="${base}/common/js/safeManager/systemManager/menuPanel.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
[#include "safeManager/mainPage/SafeManagerHeader.ftl" /]
<div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            ${message("menu.manager.system.manager")}
                <small>${message("menu.manager.system.manager.menu")}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>${message("menu.manager.system.manager")}</a></li>
                <li class="active">${message("menu.manager.system.manager.menu")}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section id="sillyhatDataTables" class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">${message("menu.manager.system.manager.menu")}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row row-margin-top">
                                <div class="col-xs-6">
                                    <label class="col-xs-2">${message("lbl.manager.menu.menuCode")}</label>
                                    <div class="col-xs-10">
                                        <input id="searchIntLogin" type="text" class="form-control" placeholder="${message("lbl.manager.menu.menuCode")}">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label class="col-xs-2">${message("lbl.manager.menu.menuName")}</label>
                                    <div class="col-xs-10">
                                        <input id="searchIntUserName" type="text" class="form-control" placeholder="${message("lbl.manager.menu.menuName")}">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-margin-top">
                                <div class="col-xs-6">
                                    <label class="col-xs-2" for="testInput1">${message("lbl.manager.admin.isDisabled")}</label>
                                    <div class="col-xs-10">
                                        <select id="searchIntUserGender" class="form-control">
                                            <option selected="selected" value="">----------</option>
                                            <option value="0">${message("lbl.manager.user.userGender.unknown")}</option>
                                            <option value="1">${message("lbl.manager.user.userGender.male")}</option>
                                            <option value="2">${message("lbl.manager.user.userGender.female")}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label class="col-xs-2">${message("lbl.manager.admin.isDelete")}</label>
                                    <div class="col-xs-10">
                                        <select id="searchIntIsJob" class="form-control">
                                            <option selected="selected" value="">----------</option>
                                            <option value="1">${message("lbl.manager.user.isJob.yes")}</option>
                                            <option value="0">${message("lbl.manager.user.isJob.no")}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-margin-top">
                                <div class="col-xs-5">
                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#shTableModal" data-id="">
                                    ${message("btn.add")}
                                    </button>
                                </div>
                                <div class="col-xs-4 sillyhat-align-right">
                                    <button id="btnSearch" type="button" class="btn btn-primary">
                                    ${message("btn.search")}
                                    </button>
                                </div>
                                <div class="col-xs-3"></div>
                            </div>
                        [#--<table id="tablePanel" class="table table-bordered table-striped">--]
                            <table id="shTablePanel" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    [#--lbl.manager.menu.menuCode = 唯一性标识--]
                                    [#--lbl.manager.menu.menuName = 名称(国际化code)--]
                                    [#--lbl.manager.menu.menuLink = 链接--]
                                    [#--lbl.manager.menu.menuType = 菜单类型--]
                                    [#--lbl.manager.menu.menuIcon = 菜单图标--]
                                    [#--lbl.manager.menu.menuParentId = 父节点ID--]
                                    [#--lbl.manager.menu.menuParentName = 父节点名称--]
                                    [#--lbl.manager.menu.menuSort = 排序--]
                                    [#--lbl.manager.admin.isDisabled = 是否禁用--]
                                    [#--lbl.manager.admin.isDisabled.yes = 已禁用--]
                                    [#--lbl.manager.admin.isDisabled.no = 未禁用--]
                                    [#--lbl.manager.admin.isDelete = 是否删除--]
                                    <th>${message("lbl.manager.admin.index")}</th>
                                    <th>${message("lbl.manager.menu.menuCode")}</th>
                                    <th>${message("lbl.manager.menu.menuName")}</th>
                                    <th>${message("lbl.manager.menu.menuParentName")}</th>
                                    <th>${message("lbl.manager.menu.menuType")}</th>
                                    <th>${message("lbl.manager.admin.isDisabled")}</th>
                                    <th>${message("lbl.manager.admin.isDelete")}</th>
                                    <th>${message("lbl.manager.admin.updatedUser")}</th>
                                    <th>${message("lbl.manager.admin.updatedDate")}</th>
                                    <th>${message("lbl.operation")}</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>${message("lbl.manager.admin.index")}</th>
                                    <th>${message("lbl.manager.menu.menuCode")}</th>
                                    <th>${message("lbl.manager.menu.menuName")}</th>
                                    <th>${message("lbl.manager.menu.menuParentName")}</th>
                                    <th>${message("lbl.manager.menu.menuType")}</th>
                                    <th>${message("lbl.manager.admin.isDisabled")}</th>
                                    <th>${message("lbl.manager.admin.isDelete")}</th>
                                    <th>${message("lbl.manager.admin.updatedUser")}</th>
                                    <th>${message("lbl.manager.admin.updatedDate")}</th>
                                    <th>${message("lbl.operation")}</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <!-- Modal -->
    <!--tabindex = "-1";//默认tab键只能切换模态框内的元素的焦点-->
    <div id="shTableModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    [#--<div class="modal fade" id="shTableModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="false">--]
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">
                        <label id="addLabel">${message("lbl.modal.title.add")}</label>
                        <label id="updateLabel">${message("lbl.modal.title.update")}</label>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <form name="sillyhat-dataTables-modal" class="form-horizontal">
                            <input type="hidden" id="menuId" value="">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.menu.menuCode")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="menuCode" type="text" class="form-control"
                                           shdt-validations="true" shdt-msg="校验失败" shdt-required="true" shdt-minLength="6" shdt-maxLength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.menu.menuName")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="menuName" type="text" class="form-control"
                                           shdt-validations="true" shdt-msg="校验失败" shdt-required="true" shdt-minLength="6" shdt-maxLength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.menu.menuLink")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="menuLink" type="text" class="form-control"
                                           shdt-validations="true" shdt-msg="校验失败" shdt-required="true" shdt-minLength="6" shdt-maxLength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.menu.menuType")}</label>
                                <div class="col-sm-10 input-group">
                                    <select id="menuType" class="form-control"
                                            shdt-validations="true" shdt-msg="校验失败" shdt-required="true">
                                        <option selected="selected" value="">----------</option>
                                        <option value="1">${message("lbl.manager.menu.menuType.one")}</option>
                                        <option value="2">${message("lbl.manager.menu.menuType.two")}</option>
                                        <option value="3">${message("lbl.manager.menu.menuType.three")}</option>
                                        <option value="4">${message("lbl.manager.menu.menuType.four")}</option>
                                        <option value="5">${message("lbl.manager.menu.menuType.five")}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.menu.menuIcon")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="menuIcon" type="text" class="form-control"
                                           shdt-validations="true" shdt-msg="校验失败" shdt-required="true" shdt-minLength="6" shdt-maxLength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                [#--menuParentName--]
                                <label class="col-sm-2 control-label">${message("lbl.manager.menu.menuParentId")}</label>
                                <div class="col-sm-10 input-group">
                                    <select id="menuParentId" class="form-control"
                                            shdt-validations="true" shdt-msg="校验失败" shdt-required="true">
                                        <option selected="selected" value="">----------</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.menu.menuSort")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="menuSort" type="text" class="form-control"
                                           shdt-validations="true" shdt-msg="校验失败" shdt-required="true" shdt-minLength="6" shdt-maxLength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.admin.isDisabled")}</label>
                                <div class="col-sm-10 input-group">
                                    <select id="isDisabled" class="form-control" shdt-validations="true" shdt-msg="校验失败" shdt-required="true">
                                        <option selected="selected" value="">----------</option>
                                        <option value="1">${message("lbl.manager.admin.isDisabled.yes")}</option>
                                        <option value="0">${message("lbl.manager.admin.isDisabled.no")}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.admin.isDelete")}</label>
                                <div class="col-sm-10 input-group">
                                    <select id="isDelete" class="form-control" shdt-validations="true" shdt-msg="校验失败" shdt-required="true">
                                        <option selected="selected" value="">----------</option>
                                        <option value="1">${message("lbl.manager.admin.isDelete.yes")}</option>
                                        <option value="0">${message("lbl.manager.admin.isDelete.no")}</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="fnSave()">${message("btn.save")}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">${message("btn.cancel")}</button>
                </div>
            </div>
        </div>
    </div>
[#include "safeManager/mainPage/SafeManagerFooter.ftl" /]
</div>
</body>
</html>