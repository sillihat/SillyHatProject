<!DOCTYPE html><!--HTML5写法 -->
<head xmlns="http://www.w3.org/1999/html">
[#include "commonPage/jscssManager.ftl" /]
[#include "commonPage/jscssDataTables.ftl" /]
<script src="${base}/common/js/safeManager/systemManager/userPanel.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
[#include "safeManager/mainPage/SafeManagerHeader.ftl" /]
<div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            ${message("menu.manager.system.manager")}
                <small>${message("menu.manager.system.manager.user")}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>${message("menu.manager.system.manager")}</a></li>
                <li class="active">${message("menu.manager.system.manager.user")}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section id="sillyhatDataTables" class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">${message("menu.manager.system.manager.user")}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row row-margin-top">
                                <div class="col-xs-6">
                                    <label class="col-xs-2">${message("lbl.manager.user.login")}</label>
                                    <div class="col-xs-10">
                                        <input id="searchIntLogin" type="text" class="form-control" placeholder="${message("lbl.manager.user.login")}">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label class="col-xs-2">${message("lbl.manager.user.userName")}</label>
                                    <div class="col-xs-10">
                                        <input id="searchIntUserName" type="text" class="form-control" placeholder="${message("lbl.manager.user.userName")}">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-margin-top">
                                <div class="col-xs-6">
                                    <label class="col-xs-2" for="testInput1">${message("lbl.manager.user.userGender")}</label>
                                    <div class="col-xs-10">
                                        <select id="searchIntUserGender" class="form-control">
                                            <option selected="selected" value="">----------</option>
                                            <option value="0">${message("lbl.manager.user.userGender.unknown")}</option>
                                            <option value="1">${message("lbl.manager.user.userGender.male")}</option>
                                            <option value="2">${message("lbl.manager.user.userGender.female")}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label class="col-xs-2">${message("lbl.manager.user.isJob")}</label>
                                    <div class="col-xs-10">
                                        <select id="searchIntIsJob" class="form-control">
                                            <option selected="selected" value="">----------</option>
                                            <option value="1">${message("lbl.manager.user.isJob.yes")}</option>
                                            <option value="0">${message("lbl.manager.user.isJob.no")}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-margin-top">
                                <div class="col-xs-5">
                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#shTableModal" data-id="">
                                        ${message("btn.add")}
                                    </button>
                                </div>
                                <div class="col-xs-4 sillyhat-align-right">
                                    <button id="btnSearch" type="button" class="btn btn-primary">
                                        ${message("btn.search")}
                                    </button>
                                </div>
                                <div class="col-xs-3"></div>
                            </div>
                            [#--<table id="tablePanel" class="table table-bordered table-striped">--]
                            <table id="shTablePanel" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>${message("lbl.manager.admin.index")}</th>
                                    <th>${message("lbl.manager.user.login")}</th>
                                    <th>${message("lbl.manager.user.userName")}</th>
                                    <th>${message("lbl.manager.user.userGender")}</th>
                                    <th>${message("lbl.manager.user.userPhone")}</th>
                                    <th>${message("lbl.manager.user.isJob")}</th>
                                    <th>${message("lbl.manager.admin.updatedUser")}</th>
                                    <th>${message("lbl.manager.admin.updatedDate")}</th>
                                    <th>${message("lbl.operation")}</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>${message("lbl.manager.admin.index")}</th>
                                    <th>${message("lbl.manager.user.login")}</th>
                                    <th>${message("lbl.manager.user.userName")}</th>
                                    <th>${message("lbl.manager.user.userGender")}</th>
                                    <th>${message("lbl.manager.user.userPhone")}</th>
                                    <th>${message("lbl.manager.user.isJob")}</th>
                                    <th>${message("lbl.manager.admin.updatedUser")}</th>
                                    <th>${message("lbl.manager.admin.updatedDate")}</th>
                                    <th>${message("lbl.operation")}</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <!-- Modal -->
    <!--tabindex = "-1";//默认tab键只能切换模态框内的元素的焦点-->
    <div id="shTableModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    [#--<div class="modal fade" id="shTableModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="false">--]
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">
                        <label id="addLabel">${message("lbl.modal.title.add")}</label>
                        <label id="updateLabel">${message("lbl.modal.title.update")}</label>
                        [#--[#if ('wordLearning' == currentSubDiv)]--]
                        [#--[#else]--]
                        [#--[/#if]--]
                    </h4>
                </div>
                <div class="modal-body">
                    [#--private String login;账号--]
                    [#--private String password;密码--]
                    [#--private String userName;//用户名--]
                    [#--private int userGender;//性别；0：未知；1：男；2：女--]
                    [#--private int userAge;//年龄--]
                    [#--private String identityCard;//身份证号--]
                    [#--private Date userBirthday;//出生日期--]
                    [#--private String userPhone;//手机号--]
                    [#--private String userEmail;//邮箱--]
                    [#--private String userIcon;//用户头像--]
                    [#--private Long companyId;//公司ID--]
                    [#--private Long departmentId;//部门ID--]
                    [#--private int isJob;//是否在职；0：离职；1：在职--]
                    [#--private Date entryDate;//入职时间--]
                    [#--private Date leaveDate;//离职时间--]
                    [#--private int isAdministrators;是否超管；0：不是；1：是--]
                    <div class="box-body">
                        <form name="sillyhat-dataTables-modal" class="form-horizontal">
                            <input type="hidden" id="userId" value="">
                        [#--<div class="row">--]
                            [#--<div class="col-sm-6">--]
                                [#--<div class="form-group">--]
                                    [#--<label class="col-sm-2 control-label">${message("lbl.manager.user.login")}</label>--]
                                    [#--<div class="col-sm-10">--]
                                    [#--<input id="login" type="text" class="form-control" placeholder="${message("xxxxx")}">--]
                                        [#--<input id="login" type="text" class="form-control"">--]
                                    [#--</div>--]
                                [#--</div>--]
                            [#--</div>--]
                            [#--<div class="col-sm-6">--]
                                [#--<div class="form-group">--]
                                    [#--<label class="col-sm-2 control-label">${message("lbl.manager.user.password")}</label>--]
                                    [#--<div class="col-sm-10">--]
                                        [#--<input id="password" type="password" class="form-control"">--]
                                    [#--</div>--]
                                [#--</div>--]
                            [#--</div>--]
                        [#--</div>--]
                        [#--<div class="form-group has-success">--]
                            [#--<label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> Input with success</label>--]
                            [#--<input type="text" class="form-control" id="inputSuccess" placeholder="Enter ...">--]
                            [#--<span class="help-block">Help block with success</span>--]
                        [#--</div>--]
                        [#--<div class="form-group has-warning">--]
                            [#--<label class="control-label" for="inputWarning"><i class="fa fa-bell-o"></i> Input with--]
                                [#--warning</label>--]
                            [#--<input type="text" class="form-control" id="inputWarning" placeholder="Enter ...">--]
                            [#--<span class="help-block">Help block with warning</span>--]
                        [#--</div>--]
                        [#--<div class="form-group has-error">--]
                            [#--<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> Input with--]
                                [#--error</label>--]
                            [#--<input type="text" class="form-control" id="inputError" placeholder="Enter ...">--]
                            [#--<span class="help-block">Help block with error</span>--]
                        [#--</div>--]
                        [#--<input id="login" type="text" class="form-control" placeholder="${message("xxxxx")}">--]
                            [#--jquery.rating.js 星级评价插件--]
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.login")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="login" type="text" class="form-control"
                                           shdt-validations="true" shdt-msg="校验失败" shdt-required="true" shdt-minLength="6" shdt-maxLength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.password")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="password" type="password" class="form-control"
                                           shdt-validations="true" shdt-msg="校验失败" shdt-required="true" shdt-minLength="6" shdt-maxLength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.affirmPassword")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="affirmPassword" type="password" class="form-control"
                                           shdt-validations="true" shdt-msg="校验失败" shdt-required="true" shdt-minLength="6" shdt-maxLength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.userName")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="userName" type="text" class="form-control"
                                           shdt-validations="true" shdt-msg="校验失败" shdt-required="true" shdt-minLength="6" shdt-maxLength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.userGender")}</label>
                                <div class="col-sm-10 input-group">
                                    <select id="userGender" class="form-control" shdt-validations="true" shdt-msg="校验失败" shdt-required="true">
                                        <option selected="selected" value="">----------</option>
                                        <option value="0">${message("lbl.manager.user.userGender.unknown")}</option>
                                        <option value="1">${message("lbl.manager.user.userGender.male")}</option>
                                        <option value="2">${message("lbl.manager.user.userGender.female")}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.userAge")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="userAge" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.identityCard")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="identityCard" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.userBirthday")}</label>
                                <div class="col-sm-10 input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input id="userBirthday" type="text" class="form-control pull-right" sillyhat-data-type="date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.userPhone")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="userPhone" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.userEmail")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="userEmail" type="email" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.userIcon")}</label>
                                <div class="col-sm-10 input-group">
                                    <input id="userIcon" type="text" class="form-control">
                                </div>
                            </div>
                            [#--<div class="form-group">--]
                                [#--<label class="col-sm-2 control-label">${message("xxx")}</label>--]
                                [#--<div class="col-sm-10">--]
                                    [#--<input id="companyId" type="text" class="form-control">--]
                                [#--</div>--]
                            [#--</div>--]
                            [#--<div class="form-group">--]
                                [#--<label class="col-sm-2 control-label">${message("xxxxx")}</label>--]
                                [#--<div class="col-sm-10">--]
                                    [#--<input id="departmentId" type="text" class="form-control">--]
                                [#--</div>--]
                            [#--</div>--]
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.isJob")}</label>
                                <div class="col-sm-10 input-group">
                                    <select id="isJob" class="form-control" shdt-validations="true" shdt-msg="校验失败" shdt-required="true">
                                        <option selected="selected" value="">----------</option>
                                        <option value="1">${message("lbl.manager.user.isJob.yes")}</option>
                                        <option value="0">${message("lbl.manager.user.isJob.no")}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.entryDate")}</label>
                                <div class="col-sm-10 input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input id="entryDate" type="text" class="form-control pull-right" sillyhat-data-type="date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.leaveDate")}</label>
                                <div class="col-sm-10 input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input id="leaveDate" type="text" class="form-control pull-right" sillyhat-data-type="date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">${message("lbl.manager.user.isAdministrators")}</label>
                                <div class="col-sm-10 input-group">
                                    <select id="isAdministrators" class="form-control" shdt-validations="true" shdt-msg="校验失败" shdt-required="true">
                                        <option selected="selected" value="">----------</option>
                                        <option value="1">${message("lbl.selected.yes")}</option>
                                        <option value="0">${message("lbl.selected.no")}</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                        <!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="fnSave()">${message("btn.save")}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">${message("btn.cancel")}</button>
                </div>
            </div>
        </div>
    </div>
[#include "safeManager/mainPage/SafeManagerFooter.ftl" /]
</div>
</body>
</html>