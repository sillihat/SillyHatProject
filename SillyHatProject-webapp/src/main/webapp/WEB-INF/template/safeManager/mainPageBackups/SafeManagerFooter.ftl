<!DOCTYPE html><!--HTML5写法 -->
<head xmlns="http://www.w3.org/1999/html">
[#include "commonPage/jscssManager.ftl" /]
</head>
<body>
<!-- footer -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    [#--<strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong>--]
    <strong>${message("lbl.footer","<a href='${base}'>","</a>")}</strong>
</footer>
</body>
</html>