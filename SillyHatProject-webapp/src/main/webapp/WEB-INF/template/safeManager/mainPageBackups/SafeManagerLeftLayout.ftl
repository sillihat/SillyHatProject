<!DOCTYPE html><!--HTML5写法 -->
<head xmlns="http://www.w3.org/1999/html">
[#include "commonPage/jscssManager.ftl" /]
</head>
<body>
<!-- 左侧菜单导航 -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="${base}/common/manager/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                    <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">${message("menu.manager.main.navigation")}</li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-table"></i>
                    <span>${message("menu.manager.system.manager")}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active">
                        <a href="javascript:void(0)" onclick="changeSafeManagerContentURL('${base}/safe-manager/toSystemManagerUserPanel')"><i class="fa fa-circle-o"></i>
                        ${message("menu.manager.system.manager.user")}
                        </a>
                    </li>
                    <li>
                        <a href="index2.html"><i class="fa fa-circle-o"></i>
                        ${message("menu.manager.system.manager.menu")}
                        </a>
                    </li>
                    <li>
                        <a href="index2.html"><i class="fa fa-circle-o"></i>
                        ${message("menu.manager.system.manager.role")}
                        </a>
                    </li>
                    <li>
                        <a href="index2.html"><i class="fa fa-circle-o"></i>
                        ${message("menu.manager.system.manager.permission")}
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active">
                        <a href="index.html">
                            <i class="fa fa-circle-o"></i>
                            Dashboard v1
                        </a>
                    </li>
                    <li>
                        <a href="index2.html">
                            <i class="fa fa-circle-o"></i>
                            Dashboard v2
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Layout Options</span>
                    <span class="pull-right-container">
                            <span class="label label-primary pull-right">4</span>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="pages/layout/top-nav.html"><i class="fa fa-circle-o">
                        </i>
                            Top Navigation
                        </a>
                    </li>
                    <li>
                        <a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i>
                            Boxed
                        </a>
                    </li>
                    <li>
                        <a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i>
                            Fixed
                        </a>
                    </li>
                    <li>
                        <a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i>
                            Collapsed Sidebar
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
</body>
</html>