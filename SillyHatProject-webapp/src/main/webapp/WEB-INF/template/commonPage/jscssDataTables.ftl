<!-- DataTables -->
<link rel="stylesheet" href="${base}/common/manager/plugins/datatables/dataTables.bootstrap.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="${base}/common/manager/plugins/datepicker/datepicker3.css">
<!-- Select2 -->
<link rel="stylesheet" href="${base}/common/manager/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="${base}/common/sillyhat/css/datatables/dataTables.sillyhat.css">

<!-- DataTables -->
[#--<script src="${base}/common/manager/plugins/datatables/jquery.dataTables.min.js"></script>--]
<script src="${base}/common/manager/plugins/datatables/jquery.dataTables.js"></script>
[#--<script src="${base}/common/manager/plugins/datatables/dataTables.bootstrap.min.js"></script>--]
<script src="${base}/common/manager/plugins/datatables/dataTables.bootstrap.js"></script>
<!-- bootstrap datepicker -->
<script src="${base}/common/manager/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Select2 -->
<script src="${base}/common/manager/plugins/select2/js/select2.min.js"></script>
<script src="${base}/common/sillyhat/js/datatables/dataTables.sillyhat.js"></script>
