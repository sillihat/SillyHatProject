[#list permissionMenuList as menuBean]
    <a class="nav-link [#if (1 == menuBean.isDisabled)]disabled[/#if] [#if (menuBean.menuCode == currentPage)]active[/#if]" href="${base}${menuBean.menuLink}">${message(menuBean.menuName)}</a>
[/#list]