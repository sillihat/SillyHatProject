<footer class="blog-footer">
    <p>${message("lbl.footer","<a href='${base}'>","</a>")}</p>
    <p>
        <a href="#">Back to top</a>
    </p>
</footer>
<!--
<footer class="footer">
    <div class="container">
        <span class="text-muted">${message("lbl.footer","<a href='${base}'>","</a>")}</span>
    </div>
</footer>
-->