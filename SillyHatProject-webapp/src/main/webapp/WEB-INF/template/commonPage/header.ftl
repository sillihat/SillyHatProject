<!DOCTYPE html><!--HTML5写法 -->
<head xmlns="http://www.w3.org/1999/html">
[#include "commonPage/jscss.ftl" /]
<script src="${base}/common/js/homepage/header.js" type="text/javascript"></script>
</head>
<body>
    <div class="blog-masthead">
        <div class="container">
            <nav id="sillyhat-header-nav" class="nav">
            [#--
            [#list permissionMenuList as menuBean]
                <a class="nav-link [#if (1 == menuBean.isDisabled)]disabled[/#if] [#if (menuBean.menuCode == currentPage)]active[/#if]" href="${base}${menuBean.menuLink}">${message(menuBean.menuName)}</a>
            [/#list]
            --]
                [#--<a class="nav-link [#if ('homePage' == currentPage)]active[/#if]" href="${base}">${message("menu.home.page")}</a>--]
                [#--<a class="nav-link [#if ('homePage' == currentPage)]active[/#if]" href="${base}">${message("menu.home.page")}</a>--]
                [#--<a class="nav-link disabled [#if 'project' == currentPage]active[/#if]" href="${base}">${message("menu.project")}</a>--]
                [#--<a class="nav-link disabled [#if 'blog' == currentPage]active[/#if]" href="#">${message("menu.blog")}</a>--]
                [#--<a class="nav-link [#if 'learningWrod' == currentPage]active[/#if]" href="${base}/learing-word/toLearningWord">${message("menu.learning.word")}</a>--]
                [#--<a class="nav-link [#if 'about' == currentPage]active[/#if]" href="#">${message("menu.about")}</a>--]
                [#--<a class="nav-link sillyhat-menu-right [#if 'login' == currentPage]active[/#if]" href="#">${message("menu.login")}</a>--]
            </nav>
        </div>
    </div>
</body>
</html>