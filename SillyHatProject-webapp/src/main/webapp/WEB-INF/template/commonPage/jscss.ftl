<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<!--
<meta name="author" content="mgface.com" />
<meta name="copyright" content="mgface.com" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="MGFACE group">
-->

<!--加载小图标-->
<link rel="icon" href="${base}/common/images/system/headLogo.ico" type="image/x-png" sizes="32x32"/>
<link href="${base}/common/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="${base}/common/tether/1.3.3/css/tether.min.css" rel="stylesheet" type="text/css" />
<link href="${base}/common/bootstrap/css/blog.css" rel="stylesheet" type="text/css"/>
<link href="${base}/common/sillyhat/css/sillyhat.css" rel="stylesheet" type="text/css"/>
<script src="${base}/common/jquery/3.2.0/jquery-3.2.0.min.js" type="text/javascript"></script>
<!--<script src="${base}/common/jquery/cookie1.4.1/jquery.cookie.js" type="text/javascript"></script>-->
<script src="${base}/common/tether/1.3.3/js/tether.min.js" type="text/javascript"></script>
<script src="${base}/common/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<script src="${base}/common/bootstrap/js/ie10-viewport-bug-workaround.js" type="text/javascript"></script>
<script src="${base}/common/bootstrap/js/ie-emulation-modes-warning.js" type="text/javascript"></script>
<script type="text/javascript">
    var sillyhatwin = sillyhatwin || {};
    sillyhatwin.ui = sillyhatwin.ui || {};
    sillyhatwin.web = sillyhatwin.web || {};
    sillyhatwin.web.ctx = "${base}";
    sillyhatBase = sillyhatwin.web.ctx;
</script>
<script src="${base}/common/sillyhat/js/utils/common.js"></script>
