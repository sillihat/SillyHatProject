<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<!--加载小图标-->
<link rel="icon" href="${base}/common/images/system/headLogo.ico" type="image/x-png" sizes="32x32"/>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<!--<link href="${base}/common/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />-->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="${base}/common/manager/bootstrap/3.3.6/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="${base}/common/manager/fontawesome/4.5.0/css/font-awesome.min.css">
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">-->
<!-- Ionicons -->
<link rel="stylesheet" href="${base}/common/manager/ionicons/2.0.1/css/ionicons.min.css">
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">-->
<!-- Theme style -->
<link rel="stylesheet" href="${base}/common/manager/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="${base}/common/manager/dist/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="${base}/common/manager/plugins/iCheck/flat/blue.css">
<!-- Morris chart -->
<link rel="stylesheet" href="${base}/common/manager/plugins/morris/morris.css">
<!-- jvectormap -->
<link rel="stylesheet" href="${base}/common/manager/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Date Picker -->
<link rel="stylesheet" href="${base}/common/manager/plugins/datepicker/datepicker3.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="${base}/common/manager/plugins/daterangepicker/daterangepicker.css">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="${base}/common/manager/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!--<script src="${base}/common/jquery/3.2.0/jquery-3.2.0.min.js" type="text/javascript"></script>-->
<!-- jQuery 2.2.3 -->
<script src="${base}/common/manager/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="${base}/common/manager/jqueryui/1.11.4/jquery-ui.min.js"></script>
<!--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>-->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>$.widget.bridge('uibutton', $.ui.button);</script>
<!--<script src="${base}/common/tether/1.3.3/js/tether.min.js" type="text/javascript"></script>-->
<!--<script src="${base}/common/bootstrap/js/bootstrap.js" type="text/javascript"></script>-->
<!-- Bootstrap 3.3.6 -->
<script src="${base}/common/manager/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="${base}/common/manager/Morris/2.1.0/raphael-min.js"></script>
<script src="${base}/common/manager/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="${base}/common/manager/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="${base}/common/manager/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="${base}/common/manager/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="${base}/common/manager/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="${base}/common/manager/daterangepicker/2.11.2/moment.min.js"></script>
<script src="${base}/common/manager/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="${base}/common/manager/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="${base}/common/manager/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="${base}/common/manager/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${base}/common/manager/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${base}/common/manager/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes)绘图js -->
<!--<script src="${base}/common/manager/dist/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="${base}/common/manager/dist/js/demo.js"></script>
<script type="text/javascript">
    var sillyhatwin = sillyhatwin || {};
    sillyhatwin.ui = sillyhatwin.ui || {};
    sillyhatwin.web = sillyhatwin.web || {};
    sillyhatwin.web.ctx = "${base}";
    sillyhatwin.web.imgPath = "http://192.168.233.133:18081";
    sillyhatBase = sillyhatwin.web.ctx;
    sillyhatImgPath= sillyhatwin.web.imgPath;
</script>
<script src="${base}/common/sillyhat/js/utils/common.js"></script>
<script src="${base}/common/js/safeManager/mainPage/safeManagerHeader.js" type="text/javascript"></script>

