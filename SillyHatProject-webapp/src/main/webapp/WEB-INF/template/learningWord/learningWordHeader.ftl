<!DOCTYPE html><!--HTML5写法 -->
<head xmlns="http://www.w3.org/1999/html">
</head>
<body>
<div class="blog-masthead-sub">
    <div class="container">
        <nav class="nav">
            <a class="nav-link-sub [#if ('wordLearning' == currentSubDiv)]active[/#if][#if ('' == currentSubDiv)]active[/#if]" href="${base}/learing-word/toLearningWord">
                ${message("menu.learning.wordLearning")}
            </a>
            <a class="nav-link-sub disabled [#if 'wordBook' == currentSubDiv]active[/#if]" href="${base}/learing-word/toWordBook">${message("menu.learning.word.wordBook")}</a>
            <a class="nav-link-sub disabled [#if 'myWordRepository' == currentSubDiv]active[/#if]" href="${base}/learing-word/toMyWordRepository">${message("menu.learning.word.myWordRepository")}</a>
            <a class="nav-link-sub [#if 'wordSet' == currentSubDiv]active[/#if]" href="${base}/learing-word/toWordSet">${message("menu.learning.word.wordSet")}</a>
        </nav>
    </div>
</div>
</body>
</html>