<!DOCTYPE html><!--HTML5写法 -->
<head xmlns="http://www.w3.org/1999/html">
[#include "commonPage/jscss.ftl" /]
</head>
<body>
[#include "commonPage/header.ftl" /]
[#include "learningWord/learningWordHeader.ftl" /]
<div class="container">
    [#--<tr class="active">...</tr>--]
    [#--<tr class="success">...</tr>--]
    [#--<tr class="warning">...</tr>--]
    [#--<tr class="danger">...</tr>--]
    [#--<tr class="info">...</tr>--]
    <table class="table table-striped table-hover .table-condensed">
        <thead>
            <tr>
                <th>单词</th>
                <th>音标</th>
                <th>类型</th>
                <th>翻译</th>
                <th>网络翻译</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            [#list resultList as wordRepositoryDTO]
            <tr>
                <td>${wordRepositoryDTO.word}</td>
                <td>美:[${wordRepositoryDTO.usPhonetic}]&nbsp;英:[${wordRepositoryDTO.ukPhonetic}]</td>
                <td>${wordRepositoryDTO.wordType}</td>
                <td>${wordRepositoryDTO.wordTranslate}</td>
                <td>${wordRepositoryDTO.webTranslate}</td>
                <td>按钮</td>
            </tr>
            [/#list]
        </tbody>
    </table>
</div>
[#include "commonPage/footer.ftl" /]
</body>
</html>