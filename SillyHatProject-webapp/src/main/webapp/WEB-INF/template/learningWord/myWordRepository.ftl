<!DOCTYPE html><!--HTML5写法 -->
<head xmlns="http://www.w3.org/1999/html">
[#include "commonPage/jscss.ftl" /]
</head>
<body>
[#include "commonPage/header.ftl" /]
[#include "learningWord/learningWordHeader.ftl" /]
<div id="sillyhatWordLearning" class="container">
    <div class="row">
        <div class="col-md-8">
            <div>
                <ul class="nav nav-horizon nav-tabs">
                    <li [#if 'myWordRepositoryType_today' == myWordRepositoryType]class="active"[/#if]><a href="/bdc/learnings/library/#today_tab">今日单词</a></li>
                    <li [#if 'myWordRepositoryType_new' == myWordRepositoryType]class="active"[/#if]><a href="/bdc/learnings/library/#fresh_tab">新的单词X</a></li>
                    <li [#if 'myWordRepositoryType_familiar' == myWordRepositoryType]class="active"[/#if]><a href="/bdc/learnings/library/#fresh_tab">正在学习</a></li>
                    <li [#if 'myWordRepositoryType_master' == myWordRepositoryType]class="active"[/#if]><a href="/bdc/learnings/library/#fresh_tab">掌握单词</a></li>
                    <li [#if 'myWordRepositoryType_resolved' == myWordRepositoryType]class="active"[/#if]><a href="/bdc/learnings/library/#fresh_tab">简单词X</a></li>
                    <li [#if 'myWordRepositoryType_hard' == myWordRepositoryType]class="active"[/#if]><a href="/bdc/learnings/library/#fresh_tab">易错词X</a></li>
                </ul>
            </div>
            <div>
                <table class="table table-striped table-hover .table-condensed">
                    <thead>
                    <tr>
                        <th>单词</th>
                        <th>音标</th>
                        <th>类型</th>
                        <th>翻译</th>
                        <th>网络翻译</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    [#list resultList as wordRepositoryDTO]
                    <tr>
                        <td>${wordRepositoryDTO.word}</td>
                        <td>美:[${wordRepositoryDTO.usPhonetic}]&nbsp;英:[${wordRepositoryDTO.ukPhonetic}]</td>
                        <td>${wordRepositoryDTO.wordType}</td>
                        <td>${wordRepositoryDTO.wordTranslate}</td>
                        <td>${wordRepositoryDTO.webTranslate}</td>
                        <td>按钮</td>
                    </tr>
                    [/#list]
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-4">
            按钮组
        </div>
    </div>
</div>
[#include "commonPage/footer.ftl" /]
</body>
</html>