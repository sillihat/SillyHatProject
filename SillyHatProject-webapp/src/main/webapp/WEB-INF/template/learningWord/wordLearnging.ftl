<!DOCTYPE html><!--HTML5写法 -->
<head xmlns="http://www.w3.org/1999/html">
[#include "commonPage/jscss.ftl" /]
</head>
<body>
[#include "commonPage/header.ftl" /]
[#include "learningWord/learningWordHeader.ftl" /]
<div class="container sillyhat" id="sillyhatWordLearning">
    <div class="row">
        <div class="col-md-12 text-center">
            <h1>${message("menu.learning.wordLearnging.container.title")}</h1>
        </div>
    </div>
    <div class="row sillyhat-terrace">
        <div class="col-md-1"></div>
        <div class="col-md-2">
            <h1 class="span2 today">
                70
                <small>${message("menu.learning.wordLearnging.today.word")}</small>
            </h1>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-2">
            <h1 class="span2">
                27
                <small>${message("menu.learning.wordLearnging.new.word")}</small>
            </h1>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4 text-center">
            <button class="btn btn-lg btn-success">
                开始学习
            </button>
        </div>
    </div>
</div>
[#include "commonPage/footer.ftl" /]
</body>
</html>