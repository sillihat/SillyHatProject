significant,significance,extreme,extremely,tremendous,comment,outstanding,excellent,splendid,formal
glance,grace,graceful,elegant,elegance,grateful,magnificent,official,officer,sacrifice
appealing,ball,race,racial,racial discrimination,global,globe,glory,glorious,glare
flag,flame,fly,flexible,flexibility,reflect,reflection,conflict,appeal,alcohol
sympathetic,pathetic,miserable,symbol,meter,system,systematic,flood,float,fluent
criticize,criticism,critic,classic,classical,symptom,sympathetically,flu,influence,influential
professional,removal,vital,fatal,essence,essential,critical,a critial moment,critical thinking,a critical patient
civil,citizen,civilize,civilization,industrialized,speciality,specialist,utilize,utility
external,individual,continual,qualify,quality,qualified,modernize,modernized,globalize,globalized
private,privacy,modify,model,mode,modern,moderate,modest,alter,alternative
enthusiasm,enthusiastic,entitle,entertain,entertainment,entertainment industry,hesitate,agent,agency,candidate
engine,engineer,enlarge,enclose,ensure,endure,tolerant,tolerate,bear,unbearable
discount,count,count on,account,counter,encounter,enter,enforce,encourage,enable
remove,removal,remote,motive,motivate,organ,organic,organize,organization,bargain
automatic,automation,move,movement,motion,motionless,emotional,emotion,promote,industry
frame,framework,fragment,fragile,friction,mobile,mobile network,hostile,mission,automobile
local,locate,location,site,website,situation,estimate,valid,frustrate,A frustrating result
press,pressure,compress,oppress,depress,depressed,depression,impress,impressive,impression
express,expression,dozen,dozens of,decade,regular,regulation,operate,operation,debate
battle,battlefield,bat,batman,isolate,calculate,participate,decorate,core,scorn
vigor,vigorous,inevitable,embrace,celebrate,brake,economic,economical,economist,economize
humble,humiliate,insult,bloom,gloom,gloomy,bore,boring,refuge,feature
accuse,accuse of,gene,genius,genuine,genuinely,generate,generous,generation,human
cheap,vocation,continual,continuous,amid,blame,blame for,charge,charge with,participant
society,social,socialism,socialist,appreciate,precious,anxious,curious,brave,march
scenery,accommodate,accommodation,ancient,ancestor,principle,discipline,associate,rent,concern
accelerate,excessive,access,factory,laboratory,library,memory,terrible,gallery,nursery
potential,tense,tension,intense,tennis,succeed,success,succession,successful,successive
attend,attention,attendant,attendance,extend,extensive,extent,to a large extent,necessary,pleasant
suitable,unfortunately,fortune,unlikely,usual,unwilling,uncover,intend,intention,intentional
pretend,tend,tendency,content,unfair,fair,fairly,fairness,visual,visual effect
purpose,on purpose,impose,invisible,vision,advise,advisable,devise,device,revise
cease,ceaseless,courage,divorce,contribute,attribute,nutrition,suspect,superior,superior or
inspect,inspector,inspection,speculate,spectacular,aspect,respect,respective,circumstance,forum
vehicle,perspective,objective,decisive,since,guilty,protest,protester,preface,surface
superficial,artificial,proceed,perfect,better,person,personal,personnel,persuade,persuasive
percent,percentage,permit,inferior,rely,refresh,restart,reset,renew,recover
discover,discovery,recovery,resort,retire,retirement,request,remind,reply,reluctant
reproach,import,internal,interior,immigrate,injure,inherit,involve,insure,insurance
policy,index,infant,ingredient,spend,expense,at the expense of,international,interfere,interfere with,interfere in
abroad,aboard,board,absolute,adapt,adopt,adjust,stick to,admire,admit
admission,omit,acquire,inquire,inquiry,accomplish,attack,attach,attach importance to,attachment
attempt,attract,attractive,attraction,abstract,conquer,contest,combine,combination
compare,compare to,compare with,comparison,comparable,comparative,put up,give up,eat up,make up
promise,sympathetic,disapprove,profession,respect,community,earn,barely,provide,consequence
exercise,healthful,decent,circle,major,social,compared,record,race
explain,affluent,analysis,correlation,equal,reflect,interaction,relationship,reinforce,access to
foresight,identify,product,service,market,increase,minority,population,neighborhood,prompt
prepare,crises,capable,corporate,alarming,price,affect,availability,value,priority