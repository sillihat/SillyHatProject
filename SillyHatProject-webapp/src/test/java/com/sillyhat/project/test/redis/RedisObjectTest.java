package com.sillyhat.project.test.redis;

import com.sillyhat.project.common.constants.Constants;
import com.sillyhat.project.common.utils.SessionIdGenerator;
import com.sillyhat.project.org.user.dto.UserDTO;
import com.sillyhat.project.org.user.service.UserService;
import com.sillyhat.project.redis.utils.SillyHatSerializeUtils;
import com.sillyhat.project.utils.JunitTestSupport;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * RedisTest
 *
 * @author 徐士宽
 * @date 2017/4/13 14:35
 */
public class RedisObjectTest extends JunitTestSupport {

    private final Logger logger = LoggerFactory.getLogger(RedisObjectTest.class);

    @Autowired
    protected RedisTemplate<Serializable, Serializable> redisTemplate;

    @Autowired
    private UserService userService;

    @Test
    public void testRedisObject() {
        List<UserDTO> list = userService.queryAllUser();
        UserDTO dto1 = list.get(0);
        UserDTO dto2 = list.get(1);
        ValueOperations valueOper = redisTemplate.opsForValue();
        logger.info("database 1 is {}",dto1);
        logger.info("database 2 is {}",dto2);
//        valueOper.set(redisTemplate.getStringSerializer().serialize("user.admin"), SillyHatSerializeUtils.serialize(dto1));
//        valueOper.set(redisTemplate.getStringSerializer().serialize("user.sillyhat"), SillyHatSerializeUtils.serialize(dto2));
//        valueOper.set(SillyHatSerializeUtils.serialize("user.admin"), SillyHatSerializeUtils.serialize(dto1));
//        valueOper.set(SillyHatSerializeUtils.serialize("user.sillyhat"), SillyHatSerializeUtils.serialize(dto2));
        valueOper.set("user.admin", dto1,2, TimeUnit.SECONDS);
        valueOper.set("user.sillyhat", dto2);
        UserDTO redisDTO1 = (UserDTO) valueOper.get("user.admin");
        UserDTO redisDTO2 = (UserDTO) valueOper.get("user.sillyhat");
        logger.info("redisDTO 1 is {}",redisDTO1);
        logger.info("redisDTO 2 is {}",redisDTO2);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        redisDTO1 = (UserDTO) valueOper.get("user.admin");
        redisDTO2 = (UserDTO) valueOper.get("user.sillyhat");
        logger.info("redisDTO sleep 1 is {}",redisDTO1);
        logger.info("redisDTO sleep 2 is {}",redisDTO2);
    }

    @Test
    public void testRedisLogin() {
        List<UserDTO> list = userService.queryAllUser();
        UserDTO dto1 = list.get(0);
        UserDTO dto2 = list.get(1);
        ValueOperations valueOper = redisTemplate.opsForValue();
        logger.info("database 1 is {}",dto1);
        logger.info("database 2 is {}",dto2);
        String dto1Key = Constants.CURRENT_USER + SessionIdGenerator.getJSessionId();
        String dto2Key = Constants.CURRENT_USER + SessionIdGenerator.getJSessionId();
        String src3Key = Constants.CURRENT_USER + SessionIdGenerator.getJSessionId();
        logger.info(dto1Key);
        logger.info(dto2Key);
        logger.info(src3Key);
        valueOper.set(dto1Key, dto1,30, TimeUnit.SECONDS);
        valueOper.set(dto2Key, dto2);
        valueOper.set(src3Key, "hahahahahahahahahahahaha");
        UserDTO redisDTO1 = (UserDTO) valueOper.get(dto1Key);
        UserDTO redisDTO2 = (UserDTO) valueOper.get(dto2Key);
        logger.info("redisDTO 1 is {}",redisDTO1);
        logger.info("redisDTO 2 is {}",redisDTO2);
        logger.info("src 3 is {}",valueOper.get(src3Key));
    }
}
