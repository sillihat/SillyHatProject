package com.sillyhat.project.test.learning;

/**
 * WordInit
 *
 * @author 徐士宽
 * @date 2017/4/6 12:00
 */
public class WordInit {

    public static void main(String[] args) {
        String test1 = "sdfa sa6fd4sf";
        String test2 = "sdfasa6fd4sf";
        if(test1.indexOf(" ") == -1){
            System.out.println("无空格");
        }else{
            System.out.println("有空格");
        }
        if(test2.indexOf(" ") == -1){
            System.out.println("无空格");
        }else{
            System.out.println("有空格");
        }
//        String test = "private,privacy,modify,model,mode,modern,moderate,modest,alert,alternative,significant,significance,extreme,extremely,tremendous,comment,outstanding,excellent,splendid,formal,glance ,grace,graceful,elegant,elegance,grateful,magnificent,official,officer,sacrifice,appealing,ball,race,racial,discrimination,global,globe,glory,glorious,glare,flag,flame,fly,flexible,flexibility,reflect,reflection,conflict,appeal,alcohol,sympathetic,pathetic,miserable,symbol,meter,system,systematic,flood,float,fluent,criticize,criticism,critic,classical,symptom,sympathetically,flu,influence,influential,classic";
//        String [] array = test.split(",");
//        System.out.println(array.length);
//        String s = "";
//        int j = 0;
//        for (int i = 0; i < array.length; i++) {
//            if(j == 10){
//                System.out.println(s);
//                j = 0;
//                s = "";
//            }
//            s += array[i] + ",";
//            j++;
//        }
//        System.out.println(s);
    }
}
