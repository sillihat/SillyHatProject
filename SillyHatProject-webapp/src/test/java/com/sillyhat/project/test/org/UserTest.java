package com.sillyhat.project.test.org;

import com.sillyhat.project.common.constants.Constants;
import com.sillyhat.project.common.dto.DataTables;
import com.sillyhat.project.common.utils.MD5Util;
import com.sillyhat.project.org.user.dto.UserDTO;
import com.sillyhat.project.org.user.dto.UserDetailDTO;
import com.sillyhat.project.org.user.service.UserService;
import com.sillyhat.project.utils.JunitTestSupport;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * UserTest
 *
 * @author 徐士宽
 * @date 2017/3/29 13:52
 */
public class UserTest extends JunitTestSupport{

    private final Logger logger = LoggerFactory.getLogger(UserTest.class);

    @Autowired
    private UserService userService;

    @Test
    public void testQueryAllUser() {
        List<UserDTO> list = userService.queryAllUser();
        for (int i = 0; i < list.size(); i++) {
            UserDTO dto = list.get(i);
            logger.info("得到第{}个DTO结果为：{}",(i+1),dto.toString());
        }
    }

    @Test
    public void testQueryDataTablesUser() {
        DataTables dataTables = new DataTables();
        dataTables.setStart(0);
        dataTables.setLength(50);
        dataTables = userService.queryUserByPage(dataTables);
        logger.info("dataTables is {}",dataTables);
        if(dataTables != null){
            List<UserDTO> list = (List<UserDTO>) dataTables.getData();
            for (int i = 0; i < list.size(); i++) {
                UserDTO dto = list.get(i);
                logger.info("得到第{}个DTO结果为：{}",(i+1),dto.toString());
            }
        }

    }

    @Test
    public void testAddUser() {
        for (int i = 1; i < 100; i++) {
            if(i != 1){
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            UserDTO dto = new UserDTO();
            int index = i+1;
            dto.setLogin("tempdk" + index);
            dto.setPassword(MD5Util.toMD5Upper("1"));
            dto.setIsAdministrators(0);
            dto.setIsDelete(0);
            dto.setCreatedUser(1l);
            dto.setUpdatedUser(1l);
            UserDetailDTO userDetail = new UserDetailDTO();
            userDetail.setUserName("tempdkUserName" + index);//用户名
            int max=40;
            int min=20;
            Random random = new Random();
            int age = random.nextInt(max)%(max-min+1) + min;
            userDetail.setUserAge(age);//年龄
            userDetail.setUserGender(i%3);//性别；0：未知；1：男；2：女
            userDetail.setIdentityCard("12332454984");//身份证号
            userDetail.setUserBirthday(new Date());//出生日期
            userDetail.setUserPhone("1234567" + index);//手机号
            userDetail.setUserEmail("1234567" + index + "@qq.com");//邮箱
            userDetail.setUserIcon(null);//用户头像
            userDetail.setCompanyId(null);//公司ID
            userDetail.setDepartmentId(null);//部门ID
            userDetail.setIsJob(1);//是否在职；0：离职；1：在职
            dto.setUserDetail(userDetail);
            userService.addUser(dto);
        }
    }
}
