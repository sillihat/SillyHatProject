package com.sillyhat.project.test.redis;

import com.sillyhat.project.org.user.dto.UserDTO;
import com.sillyhat.project.org.user.service.UserService;
import com.sillyhat.project.redis.service.RedisService;
import com.sillyhat.project.utils.JunitTestSupport;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;

import java.util.List;

import javax.annotation.Resource;

import redis.clients.jedis.Jedis;

/**
 * RedisTest
 *
 * @author 徐士宽
 * @date 2017/4/13 14:35
 */
public class RedisTest extends JunitTestSupport {

    private final Logger logger = LoggerFactory.getLogger(RedisTest.class);

    @Autowired
    private RedisService redisService;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate<String,UserDTO> redisTemplate;

    @Test
    public void testRedisConnection() {
        //连接redis服务器，192.168.0.100:6379
        Jedis jedis = new Jedis("192.168.233.133", 6379);
        jedis.auth("sillyhat");
        jedis.set("testA","AAAAAAAAAAAAA");
        jedis.set("testB","BBBBBBBBBBBBB");
        logger.info("testA is {}",jedis.get("testA"));
        logger.info("testB is {}",jedis.get("testB"));
    }

    @Test
    public void testRedisOperation() {
        logger.info("sillyhat_cluster is {}",redisService.get("sillyhat_cluster"));
        List<UserDTO> list = userService.queryAllUser();
        UserDTO dto1 = list.get(0);
        UserDTO dto2 = list.get(1);
        redisService.set("testA",dto1.toString());
        redisService.set("testB",dto2.toString());
        logger.info("testA is {}",redisService.get("testA"));
        logger.info("testB is {}",redisService.get("testB"));
    }

    @Test
    public void testRedisObject() {
        List<UserDTO> list = userService.queryAllUser();
        UserDTO dto1 = list.get(0);
        UserDTO dto2 = list.get(1);
        ValueOperations<String, UserDTO> valueOper = redisTemplate.opsForValue();
//        redisTemplate.getValueSerializer().serialize(new Jackson2JsonRedisSerializer(dto1));
        valueOper.set("testA", dto1);
        valueOper.set("testB", dto2);
        logger.info("testA is {}",dto1);
        logger.info("testB is {}",dto2);
    }
}
