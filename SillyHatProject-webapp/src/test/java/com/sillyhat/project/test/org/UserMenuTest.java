package com.sillyhat.project.test.org;

import com.sillyhat.project.common.constants.Constants;
import com.sillyhat.project.org.usermenu.service.UserMenuService;
import com.sillyhat.project.org.usermenu.dto.UserMenuDTO;
import com.sillyhat.project.utils.JunitTestSupport;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 *
 * @author XUSHIKUAN
 * @date 2017-04-14
 */
public class UserMenuTest extends JunitTestSupport {

    private final Logger logger = LoggerFactory.getLogger(UserMenuTest.class);

    @Autowired
    private UserMenuService userMenuService;

    @Test
    public void testAdd() {
//        long [] menus = {1l,2l,3l,5l,6l};
        long [] menus = {1l,2l,3l,4l,5l,6l};
        for (int i = 0; i < menus.length; i++) {
            UserMenuDTO dto = new UserMenuDTO();
//            dto.setUserId(ActivitiConstants.PERMISSION_PUBLIC_MENU);//用户ID
            dto.setUserId(2l);//用户ID
            dto.setMenuId(menus[i]);//菜单ID
            userMenuService.addUserMenu(dto);
        }
    }

    @Test
    public void testUpdate() {
        UserMenuDTO dto = new UserMenuDTO();
		dto.setId(0l);//主键
        dto.setUserId(Constants.PERMISSION_PUBLIC_MENU);//用户ID
        dto.setMenuId(1l);//菜单ID
        userMenuService.updateUserMenu(dto);
    }

    @Test
    public void testDelete() {
        Long id = 0l;
        userMenuService.deleteUserMenuByPrimaryKey(id);
    }

    @Test
    public void testQueryAll() {
        List<UserMenuDTO> list = userMenuService.queryUserMenuAll();
        for (int i = 0; i < list.size(); i++) {
            UserMenuDTO dto = list.get(i);
            logger.info(dto.toString());
        }
    }

    @Test
    public void testGetCount() {
        logger.info("count-----------{}",userMenuService.getUserMenuCountByParams(null));
    }

}
