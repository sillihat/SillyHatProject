package com.sillyhat.project.test.org;

import com.sillyhat.project.common.constants.Constants;
import com.sillyhat.project.org.menu.dto.MenuDTO;
import com.sillyhat.project.org.menu.service.MenuService;
import com.sillyhat.project.utils.JunitTestSupport;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Date;
import java.util.List;

/**
 *
 * @author XUSHIKUAN
 * @date 2017-04-12
 */
public class MenuTest extends JunitTestSupport {

    private final Logger logger = LoggerFactory.getLogger(MenuTest.class);

    @Autowired
    private MenuService menuService;

    @Test
    public void testAdd() {
        String [] menuCodeArray = {"homePage","project","blog","learningWrod","about","login","safeManager"};
        String [] menuNameArray = {"menu.home.page","menu.project","menu.blog","menu.learning.word","menu.about","menu.login","menu.safe.manager"};
        String [] menuLinkArray = {"#","#","#","/learing-word/toLearningWord","#","/toLogin","/safe-manager/toSafeManager"};
        String [] menuIconArray = {"","","","","","",""};
        for (int i = 0; i < menuNameArray.length; i++) {
            MenuDTO dto = new MenuDTO();
            dto.setMenuCode(menuCodeArray[i]);//唯一性标识
            dto.setMenuName(menuNameArray[i]);//名称(国际化code)
            dto.setMenuLink(menuLinkArray[i]);//链接
            dto.setMenuIcon(menuIconArray[i]);
            dto.setMenuType(Constants.MENU_TYPE_HEADER);//菜单类型
            dto.setMenuParentId(0l);//父节点ID
            dto.setMenuSort((i+1) * 10);//排序
            dto.setCreatedUser(1l);//创建人
            dto.setCreatedDate(new Date());//创建时间
            dto.setUpdatedUser(1l);//修改人
            dto.setUpdatedDate(new Date());//修改时间
            menuService.addMenu(dto);
        }
    }

    @Test
    public void testUpdate() {
        MenuDTO dto = new MenuDTO();
        dto.setId(0l);//主键
        dto.setMenuCode("");//唯一性标识
        dto.setMenuName("");//名称(国际化code)
        dto.setMenuLink("");//链接
        dto.setMenuType(0);//菜单类型
        dto.setMenuParentId(0l);//父节点ID
        dto.setMenuSort(0);//排序
        dto.setCreatedUser(1l);//创建人
        dto.setCreatedDate(new Date());//创建时间
        dto.setUpdatedUser(1l);//修改人
        dto.setUpdatedDate(new Date());//修改时间
        menuService.updateMenu(dto);
    }

    @Test
    public void testDelete() {
        Long id = 2l;
        menuService.deleteMenuByPrimaryKey(id);
    }

    @Test
    public void testQueryAll() {
        List<MenuDTO> list = menuService.queryMenuAll();
        for (int i = 0; i < list.size(); i++) {
            MenuDTO dto = list.get(i);
            logger.info(dto.toString());
        }
    }

    @Test
    public void testGetCount() {
//        logger.info("count-----------{}",menuService.getMenuCountByParams(null));
    }

}
