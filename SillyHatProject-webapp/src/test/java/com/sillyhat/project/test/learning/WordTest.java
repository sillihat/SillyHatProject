package com.sillyhat.project.test.learning;

import com.sillyhat.project.common.constants.Constants;
import com.sillyhat.project.common.utils.YouDaoHttpUtils;
import com.sillyhat.project.learning.userlearningword.dto.UserLearningWordDTO;
import com.sillyhat.project.learning.userlearningword.service.UserLearningWordService;
import com.sillyhat.project.learning.wordrepository.dto.WordRepositoryDTO;
import com.sillyhat.project.learning.wordrepository.service.WordRepositoryService;
import com.sillyhat.project.learning.youdao.dto.YouDaoDTO;
import com.sillyhat.project.learning.youdao.dto.YouDaoWebDTO;
import com.sillyhat.project.utils.JunitTestSupport;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * WordTest
 *
 * @author 徐士宽
 * @date 2017/4/6 12:00
 */
public class WordTest extends JunitTestSupport{

    private final Logger logger = LoggerFactory.getLogger(WordTest.class);

    @Autowired
    private WordRepositoryService wordRepositoryService;

    @Autowired
    private UserLearningWordService userLearningWordService;

    /**
     * 将字典表copy到用户学习词汇表中
     */
    @Test
    public void testCopyWordRepositoryToUserLearningWord(){
        long userId = 2l;
        UserLearningWordDTO dto = null;
        List<WordRepositoryDTO> wordRepositoryDTOList = wordRepositoryService.queryWordRepositoryAll();
        List<UserLearningWordDTO> userLearningWordDTOList = userLearningWordService.queryUserLearningWordByUserId(userId);
        for (int i = 0; i < wordRepositoryDTOList.size(); i++) {
            WordRepositoryDTO wordRepositoryDTO = wordRepositoryDTOList.get(i);
            boolean add = true;
            for (int j = 0; j < userLearningWordDTOList.size(); j++) {
                UserLearningWordDTO userLearningWordDTO = userLearningWordDTOList.get(j);
                if(userLearningWordDTO.getWordId() == wordRepositoryDTO.getId()){
                    add = false;
                    break;
                }
            }
            if(add){
                dto = new UserLearningWordDTO();
                dto.setUserId(userId);
                dto.setWordId(wordRepositoryDTO.getId());
                dto.setCreatedUser(1l);
                dto.setUpdatedUser(1l);
                userLearningWordService.addUserLearningWord(dto);
            }
        }
    }

    @Test
    public void testQueryAllWordRepository(){
        List<WordRepositoryDTO> list = wordRepositoryService.queryWordRepositoryAll();
        for (int i = 0; i < list.size(); i++) {
            logger.info("单词{}详细信息：{}",(i + 1),list.get(i).toString());
        }
    }
    /**
     * 初始化字典表
     */
    @Test
    public void testInitWordRepository() {
        String youdaoInterface = "http://fanyi.youdao.com/openapi.do?keyfrom=SillyHatYouDao&key=987724779&type=data&doctype=json&version=1.1&q=";
        String filePathName = "D:\\word.txt";
        Map<String,List<String>> resultMap = getWordList(filePathName);
        List<String> wordList = resultMap.get("wordList");//单词列表
        List<String> phraseList = resultMap.get("phraseList");//短语列表
        addWordList(youdaoInterface,wordList);
        addPhraseList(youdaoInterface,phraseList);
    }

    private void addPhraseList(String youdaoInterface,List<String> wordList){
        for (int i = 0; i < wordList.size(); i++) {
            String word = wordList.get(i);
            WordRepositoryDTO wordDTO = wordRepositoryService.getWordRepositoryByWord(word);
            if(wordDTO != null){
                continue;//数据库中已经存在不需要连接有道接口查询
            }else {
                try {
                    Thread.sleep(5000);
                    YouDaoDTO youdaoDTO = YouDaoHttpUtils.requestHttpGetToYouDao(youdaoInterface,word);
                    if(youdaoDTO != null){
                        WordRepositoryDTO dto = new WordRepositoryDTO();
                        dto.setWord(word);
                        String wordTranslate = "";
                        List<String> wordTranslateList = youdaoDTO.getTranslation();
                        for (int j = 0; j < wordTranslateList.size(); j++) {
                            wordTranslate += wordTranslateList.get(j) + "\r\n";
                        }
                        dto.setWordTranslate(wordTranslate);
                        dto.setWordType(Constants.WORD_TYPE_PHRASE);
                        dto.setCreatedUser(1l);
                        dto.setUpdatedUser(1l);
                        wordRepositoryService.addWordRepository(dto);
                    }else{
                        logger.error("调用有道接口查询{}获得结果为null",word);
                    }
                } catch (Exception e) {
                    logger.error("新增单词{}出现异常",word,e);
                }
            }
        }
    }
    private void addWordList(String youdaoInterface,List<String> wordList){
        for (int i = 0; i < wordList.size(); i++) {
            String word = wordList.get(i);
            WordRepositoryDTO wordDTO = wordRepositoryService.getWordRepositoryByWord(word);
            if(wordDTO != null){
                continue;//数据库中已经存在不需要连接有道接口查询
            }else {
                try {
                    Thread.sleep(5000);
                    YouDaoDTO youdaoDTO = YouDaoHttpUtils.requestHttpGetToYouDao(youdaoInterface,word);
                    if(youdaoDTO != null){
                        WordRepositoryDTO dto = new WordRepositoryDTO();
                        dto.setWord(word);
                        dto.setUsPhonetic("[" + youdaoDTO.getBasic().getUsPhonetic() + "]");
                        dto.setUkPhonetic("[" + youdaoDTO.getBasic().getUkPhonetic() + "]");
                        List<String> wordTranslateList = youdaoDTO.getBasic().getExplains();
                        String wordTranslate = "";
                        for (int j = 0; j < wordTranslateList.size(); j++) {
                            wordTranslate += wordTranslateList.get(j) + "\r\n";
                        }
                        dto.setWordTranslate(wordTranslate);
                        String webTranslate = "";
                        List<YouDaoWebDTO> youdaoWebList = youdaoDTO.getWeb();
                        for (int j = 0; j < youdaoWebList.size(); j++) {
                            YouDaoWebDTO youDaoWebDTO = youdaoWebList.get(j);
                            webTranslate += youDaoWebDTO.getKey() + "  ";
                            List<String> values = youDaoWebDTO.getValue();
                            for (int k = 0; k < values.size(); k++) {
                                webTranslate += values.get(k) + ";  ";
                            }
                            webTranslate += "\r\n";
                        }
                        dto.setWebTranslate(webTranslate);
                        dto.setWordType(Constants.WORD_TYPE_WORD);
                        dto.setCreatedUser(1l);
                        dto.setUpdatedUser(1l);
                        wordRepositoryService.addWordRepository(dto);
                    }else{
                        logger.error("调用有道接口查询{}获得结果为null",word);
                    }
                } catch (Exception e) {
                    logger.error("新增单词{}出现异常",word,e);
                }
            }
        }
    }


    /**
     * 得到词汇列表
     * @param filePathName
     * @return
     */
    private Map<String,List<String>> getWordList(String filePathName){
        Map<String,List<String>> resultMap = new HashMap<String,List<String>>();
        List<String> wordList = new ArrayList<String>();//单词列表
        List<String> phraseList = new ArrayList<String>();//短语列表
        List<String> fileContentList = getFileContent(filePathName);
        for (int i = 0; i < fileContentList.size(); i++) {
            String [] array = fileContentList.get(i).split(",",-1);
            for (int j = 0; j < array.length; j++) {
                String word = array[j];
                if (word.equals("")){
                    continue;
                }
                if(word.indexOf(" ") == -1){
                    wordList.add(word);
                }else{
                    //有空格，为短语类型
                    phraseList.add(word);
                }
            }
        }
        resultMap.put("wordList",wordList);
        resultMap.put("phraseList",phraseList);
        return resultMap;
//        logger.info("===================================");
//        logger.info("===================================");
//        logger.info("===================================");
//        logger.info("===================================");
//        for (int i = 0; i < wordList.size(); i++) {
//            logger.info("单词：" + wordList.get(i));
//        }
//        logger.info("===================================");
//        logger.info("===================================");
//        logger.info("===================================");
//        logger.info("===================================");
//        for (int i = 0; i < phraseList.size(); i++) {
//            logger.info("短语：" + phraseList.get(i));
//        }
    }

    /**
     * 从文件中提取词汇
     * @param filePathName
     * @return
     */
    private List<String> getFileContent(String filePathName){
        List<String> resuletList = new ArrayList<String>();
        try {
            String encoding="UTF-8";
            File file = new File(filePathName);
            if(file.isFile() && file.exists()){//判断文件是否存在
                InputStreamReader read = new InputStreamReader(new FileInputStream(file),encoding);
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = null;
                while((lineTxt = bufferedReader.readLine()) != null){
                    logger.info(lineTxt);
                    resuletList.add(lineTxt);
                }
                read.close();
            }else{
                logger.info("找不到指定的文件");
            }
        } catch (Exception e) {
            logger.error("读取文件内容出错",e);
        }
        return resuletList;
    }
}
