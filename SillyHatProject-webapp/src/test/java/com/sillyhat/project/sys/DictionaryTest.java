package com.sillyhat.project.sys;

import com.sillyhat.project.sys.dictionary.service.DictionaryService;
import com.sillyhat.project.sys.dictionary.dto.DictionaryDTO;
import com.sillyhat.project.utils.JunitTestSupport;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 *
 * @author XUSHIKUAN
 * @date 2017-07-05
 */
public class DictionaryTest extends JunitTestSupport {

    private final Logger logger = LoggerFactory.getLogger(DictionaryTest.class);

    @Autowired
    private DictionaryService dictionaryService;

    @Test
    public void testAdd() {
        DictionaryDTO dto = new DictionaryDTO();
		dto.setId(0l);//主键ID
		dto.setDictCode("");//code
		dto.setDictName("");//名称
		dto.setDictValue("");//值
		dto.setIsDisabled(0);//是否禁用;0:未禁用;1:已禁用
		dto.setDictSort(0);//排序
		dto.setRemark("");//备注
		dto.setCreatedUser(0l);//创建人
		dto.setCreatedDate(null);//创建时间
		dto.setUpdatedUser(0l);//修改人
		dto.setUpdatedDate(null);//修改时间

        dictionaryService.addDictionary(dto);
    }

    @Test
    public void testUpdate() {
        DictionaryDTO dto = new DictionaryDTO();
		dto.setId(0l);//主键ID
		dto.setDictCode("");//code
		dto.setDictName("");//名称
		dto.setDictValue("");//值
		dto.setIsDisabled(0);//是否禁用;0:未禁用;1:已禁用
		dto.setDictSort(0);//排序
		dto.setRemark("");//备注
		dto.setCreatedUser(0l);//创建人
		dto.setCreatedDate(null);//创建时间
		dto.setUpdatedUser(0l);//修改人
		dto.setUpdatedDate(null);//修改时间

        dictionaryService.updateDictionary(dto);
    }

    @Test
    public void testDelete() {
        Long id = 0l;
        dictionaryService.deleteDictionaryByPrimaryKey(id);
    }

    @Test
    public void testQueryAll() {
        List<DictionaryDTO> list = dictionaryService.queryDictionaryAll();
        for (int i = 0; i < list.size(); i++) {
            DictionaryDTO dto = list.get(i);
            logger.info(dto.toString());
        }
    }

    @Test
    public void testGetCount() {
        logger.info("count-----------{}",dictionaryService.getDictionaryCountByParams(null));
    }

}
