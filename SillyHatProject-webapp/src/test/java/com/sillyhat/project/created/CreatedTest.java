package com.sillyhat.project.created;

import com.sillyhat.generator.dto.SillyHatGeneratorCreatedDTO;
import com.sillyhat.generator.main.SillyHatGeneratorCreatedMain;
import com.sillyhat.generator.utils.SillyHatGeneratorConstants;

/**
 * Created by ${XUSHIKUAN} on 2017-04-11.
 */
public class CreatedTest {

    public static void main(String[] args) {
        String databaseDriverClassName = "com.mysql.jdbc.Driver";
        String databaseUrl = "jdbc:mysql://localhost:3306/sillyhat_project_db?useUnicode=true&characterEncoding=utf8&autoReconnect=true&rewriteBatchedStatements=TRUE";
        String databaseUserName = "sillyhat_project";
        String databasePassword = "sillyhat_project_pw";
        String outFilePath = "E:\\Log\\Created\\";
        String author = "XUSHIKUAN";
        String tableName = "T_SYS_DICTIONARY";
        String packageName = "com.sillyhat.project.sys";
        String moduelName = "dictionary";
        String entityName = "Dictionary";
//        String tableName = "T_BASE_USER_MENU";
//        String packageName = "com.sillyhat.project.org";
//        String moduelName = "usermenu";
//        String entityName = "UserMenu";
        boolean usePackagePath = true;//使用包路径,如果为false,则全部文件生成到同一目录
        boolean createdFieldMerge = false;//生成的文件，是否放在同一个目录下，false则dao  server impl mapper mapping 分别放入不同目录
        SillyHatGeneratorCreatedDTO dto = new SillyHatGeneratorCreatedDTO(databaseDriverClassName, databaseUrl, databaseUserName, databasePassword, outFilePath, author, tableName, packageName, moduelName, entityName);
        SillyHatGeneratorCreatedMain.getInstance().createDefault(dto, SillyHatGeneratorConstants.DATABASE_TYPE_MYSQL,usePackagePath,createdFieldMerge);
    }
}
