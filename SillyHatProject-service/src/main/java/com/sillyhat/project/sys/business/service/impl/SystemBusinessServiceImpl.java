package com.sillyhat.project.sys.business.service.impl;


import com.sillyhat.project.common.constants.Constants;
import com.sillyhat.project.sys.business.service.SystemBusinessService;
import com.sillyhat.project.sys.dictionary.dto.DictionaryDTO;
import com.sillyhat.project.sys.dictionary.service.DictionaryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * SystemBusinessService
 *
 * @author 徐士宽
 * @date 2017/7/6 10:29
 */
@Service
public class SystemBusinessServiceImpl implements SystemBusinessService {

    @Autowired
    private DictionaryService dictionaryService;

    @Override
    public boolean initSystemData(){
        DictionaryDTO dictionary = dictionaryService.selectDictionaryByDictCode(Constants.SYS_DICTIONARY_DICTCODE_INIT_SYSTEM);
        if(dictionary != null && dictionary.getDictValue().equals(Constants.INIT_SYSTEM_YES)){
            return true;
        }else{
            return false;
        }
    }
}
