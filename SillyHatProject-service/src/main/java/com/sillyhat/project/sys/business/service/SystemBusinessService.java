package com.sillyhat.project.sys.business.service;

/**
 * SystemBusinessService
 *
 * @author 徐士宽
 * @date 2017/7/6 10:28
 */
public interface SystemBusinessService {

    public boolean initSystemData();

}
