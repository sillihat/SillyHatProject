package com.sillyhat.project.sys.dictionary.service.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sillyhat.project.core.dto.PageDTO;
import com.sillyhat.project.sys.dictionary.service.DictionaryService;
import com.sillyhat.project.sys.dictionary.dto.DictionaryDTO;
import com.sillyhat.project.sys.dictionary.mapper.DictionaryMapper;
import javax.annotation.Resource;

@Service
public class DictionaryServiceImpl implements DictionaryService{

	@Resource
	private DictionaryMapper dictionaryMapper;
	
	@Override
	public DictionaryDTO addDictionary(DictionaryDTO dto) {
		Long id = dictionaryMapper.insertDictionary(dto);
		dto.setId(id);
		return dto;
	}

	@Override
	public DictionaryDTO updateDictionary(DictionaryDTO dto) {
		dictionaryMapper.updateDictionary(dto);
		return dto;
	}

	@Override
	public boolean deleteDictionaryByPrimaryKey(long id) {
		int i = dictionaryMapper.deleteDictionaryByPrimaryKey(id);
		return i > 0 ? true : false;
	}
	
	@Override
	public DictionaryDTO selectDictionaryByPrimaryKey(long id) {
		return dictionaryMapper.selectDictionaryByPrimaryKey(id);
	}

	@Override
	public DictionaryDTO selectDictionaryByDictCode(String dictCode) {
		return dictionaryMapper.selectDictionaryByDictCode(dictCode);
	}

	@Override
	public PageDTO queryDictionaryByPage(PageDTO page) {
        List<DictionaryDTO> list = dictionaryMapper.queryDictionaryByPage(page);
	    int totalCount = dictionaryMapper.getDictionaryCountByParams(page.getParams());
        page.setResultList(list);
        page.setTotalCount(totalCount);
		return page;
	}

	@Override
	public int getDictionaryCountByParams(Map<String,Object> params) {
		return dictionaryMapper.getDictionaryCountByParams(params);
	}

	@Override
	public List<DictionaryDTO> queryDictionaryAll() {
		return dictionaryMapper.queryDictionaryAll();
	}
}

