package com.sillyhat.project.sys.dictionary.service;

import java.util.List;
import java.util.Map;

import com.sillyhat.project.core.dto.PageDTO;
import com.sillyhat.project.sys.dictionary.dto.DictionaryDTO;

public interface DictionaryService {

	/**
	 * <p>Title: add</p>
	 * <p>Description: </p>保存
	 * @param obj
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
	public DictionaryDTO addDictionary(DictionaryDTO dto);
	
	/**
	 * <p>Title: update</p>
	 * <p>Description: </p>修改
	 * @param obj
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
	public DictionaryDTO updateDictionary(DictionaryDTO dto);
	
	/**
	 * <p>Title: deleteDictionaryByPrimaryKey</p>
	 * <p>Description: </p>根据主键删除
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
	public boolean deleteDictionaryByPrimaryKey(long id);
	
	/**
	 * <p>Title: selectDictionaryByPrimaryKey</p>
	 * <p>Description: </p>根据主键查询
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
	public DictionaryDTO selectDictionaryByPrimaryKey(long id);


	public DictionaryDTO selectDictionaryByDictCode(String dictCode);

	/**
	 * <p>Title: queryDictionaryByPage</p>
	 * <p>Description: </p>分页查询
	 * @param page
	 * @return	PageDTO：分页查询实体类
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
	public PageDTO queryDictionaryByPage(PageDTO page);


	/**
	 * <p>Title: getDictionaryCountByParams</p>
	 * <p>Description: </p>查询总数
	 * @param param 查询条件
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
    public int getDictionaryCountByParams(Map<String, Object> params);

	/**
	 * <p>Title: queryDictionaryAll</p>
	 * <p>Description: </p>查询所有
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
	public List<DictionaryDTO> queryDictionaryAll();

}
