package com.sillyhat.project.org.role.service.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sillyhat.project.common.dto.DataTables;
import com.sillyhat.project.core.dto.PageDTO;
import com.sillyhat.project.org.menu.dto.MenuDTO;
import com.sillyhat.project.org.role.service.RoleService;
import com.sillyhat.project.org.role.dto.RoleDTO;
import com.sillyhat.project.org.role.mapper.RoleMapper;
import javax.annotation.Resource;

@Service
public class RoleServiceImpl implements RoleService{

	@Resource
	private RoleMapper roleMapper;
	
	@Override
	public RoleDTO addRole(RoleDTO dto) {
		Long id = roleMapper.insertRole(dto);
		dto.setId(id);
		return dto;
	}

	@Override
	public RoleDTO updateRole(RoleDTO dto) {
		roleMapper.updateRole(dto);
		return dto;
	}

	@Override
	public boolean deleteRoleByPrimaryKey(long id) {
		int i = roleMapper.deleteRoleByPrimaryKey(id);
		return i > 0 ? true : false;
	}
	
	@Override
	public RoleDTO selectRoleByPrimaryKey(long id) {
		return roleMapper.selectRoleByPrimaryKey(id);
	}

	@Override
	public DataTables queryMenuByPage(DataTables dataTables) {
		List<RoleDTO> list = roleMapper.queryDataTablesMenuByPage(dataTables);
		dataTables.setData(list);
		return dataTables;
	}

	@Override
	public List<RoleDTO> queryRoleAll() {
		return roleMapper.queryRoleAll();
	}
}

