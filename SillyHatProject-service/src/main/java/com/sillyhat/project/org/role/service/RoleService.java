package com.sillyhat.project.org.role.service;

import java.util.List;
import java.util.Map;

import com.sillyhat.project.common.dto.DataTables;
import com.sillyhat.project.core.dto.PageDTO;
import com.sillyhat.project.org.role.dto.RoleDTO;

public interface RoleService {

	/**
	 * <p>Title: add</p>
	 * <p>Description: </p>保存
	 * @param obj
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 */
	public RoleDTO addRole(RoleDTO dto);
	
	/**
	 * <p>Title: update</p>
	 * <p>Description: </p>修改
	 * @param obj
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 */
	public RoleDTO updateRole(RoleDTO dto);
	
	/**
	 * <p>Title: deleteRoleByPrimaryKey</p>
	 * <p>Description: </p>根据主键删除
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 */
	public boolean deleteRoleByPrimaryKey(long id);
	
	/**
	 * <p>Title: selectRoleByPrimaryKey</p>
	 * <p>Description: </p>根据主键查询
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 */
	public RoleDTO selectRoleByPrimaryKey(long id);


	public DataTables queryMenuByPage(DataTables dataTables);
	/**
	 * <p>Title: queryRoleAll</p>
	 * <p>Description: </p>查询所有
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 */
	public List<RoleDTO> queryRoleAll();

}
