package com.sillyhat.project.org.menu.service;

import com.sillyhat.project.common.dto.DataTables;
import com.sillyhat.project.org.menu.dto.MenuDTO;

import java.util.List;

public interface MenuService {

	/**
	 * <p>Title: add</p>
	 * <p>Description: </p>保存
	 * @param obj
	 * @author XUSHIKUAN
	 * @date 2017-04-12
	 */
	public MenuDTO addMenu(MenuDTO dto);
	
	/**
	 * <p>Title: update</p>
	 * <p>Description: </p>修改
	 * @param obj
	 * @author XUSHIKUAN
	 * @date 2017-04-12
	 */
	public MenuDTO updateMenu(MenuDTO dto);
	
	/**
	 * <p>Title: deleteMenuByPrimaryKey</p>
	 * <p>Description: </p>根据主键删除
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-04-12
	 */
	public boolean deleteMenuByPrimaryKey(long id);
	
	/**
	 * <p>Title: selectMenuByPrimaryKey</p>
	 * <p>Description: </p>根据主键查询
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-04-12
	 */
	public MenuDTO selectMenuByPrimaryKey(long id);

	public DataTables queryMenuByPage(DataTables dataTables);

	/**
	 * <p>Title: queryMenuAll</p>
	 * <p>Description: </p>查询所有
	 * @author XUSHIKUAN
	 * @date 2017-04-12
	 */
	public List<MenuDTO> queryMenuAll();

	public List<MenuDTO> queryHeaderMenuByPermission(long permissionId);

	public List<MenuDTO> querySafeManagerHeaderMenu();




}
