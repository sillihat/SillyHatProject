package com.sillyhat.project.org.usermenu.service.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sillyhat.project.core.dto.PageDTO;
import com.sillyhat.project.org.usermenu.service.UserMenuService;
import com.sillyhat.project.org.usermenu.dto.UserMenuDTO;
import com.sillyhat.project.org.usermenu.mapper.UserMenuMapper;
import javax.annotation.Resource;

@Service
public class UserMenuServiceImpl implements UserMenuService{

	@Resource
	private UserMenuMapper userMenuMapper;
	
	@Override
	public UserMenuDTO addUserMenu(UserMenuDTO dto) {
		Long id = userMenuMapper.insertUserMenu(dto);
		dto.setId(id);
		return dto;
	}

	@Override
	public UserMenuDTO updateUserMenu(UserMenuDTO dto) {
		userMenuMapper.updateUserMenu(dto);
		return dto;
	}

	@Override
	public boolean deleteUserMenuByPrimaryKey(long id) {
		int i = userMenuMapper.deleteUserMenuByPrimaryKey(id);
		return i > 0 ? true : false;
	}
	
	@Override
	public UserMenuDTO selectUserMenuByPrimaryKey(long id) {
		return userMenuMapper.selectUserMenuByPrimaryKey(id);
	}
	
	@Override
	public PageDTO queryUserMenuByPage(PageDTO page) {
        List<UserMenuDTO> list = userMenuMapper.queryUserMenuByPage(page);
	    int totalCount = userMenuMapper.getUserMenuCountByParams(page.getParams());
        page.setResultList(list);
        page.setTotalCount(totalCount);
		return page;
	}

	@Override
	public int getUserMenuCountByParams(Map<String,Object> params) {
		return userMenuMapper.getUserMenuCountByParams(params);
	}

	@Override
	public List<UserMenuDTO> queryUserMenuAll() {
		return userMenuMapper.queryUserMenuAll();
	}
}

