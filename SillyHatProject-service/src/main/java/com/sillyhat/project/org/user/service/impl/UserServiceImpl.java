package com.sillyhat.project.org.user.service.impl;

import com.sillyhat.project.common.constants.Constants;
import com.sillyhat.project.common.dto.DataTables;
import com.sillyhat.project.common.utils.MD5Util;
import com.sillyhat.project.org.menu.dto.MenuDTO;
import com.sillyhat.project.org.menu.service.MenuService;
import com.sillyhat.project.org.user.dto.UserDTO;
import com.sillyhat.project.org.user.dto.UserDetailDTO;
import com.sillyhat.project.org.user.mapper.UserMapper;
import com.sillyhat.project.org.user.service.UserService;
import com.sillyhat.project.org.usermenu.dto.UserMenuDTO;
import com.sillyhat.project.org.usermenu.service.UserMenuService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

/**
 * UserServiceImpl
 *
 * @author 徐士宽
 * @date 2017/3/29 14:05
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Autowired
    private MenuService menuService;

    @Autowired
    private UserMenuService userMenuService;

    public List<UserDTO> queryAllUser() {
        return userMapper.queryAllUser();
    }

    private List<Long> commonMenuId = new ArrayList<>();
    private List<Long> allMenuId = new ArrayList<>();
    private List<Long> allUserId = new ArrayList<>();

    private void initMenu(List<MenuDTO> menuList,Long administratorId,Long parentId){
        for (int i = 0; i < menuList.size(); i++) {
            MenuDTO dto = menuList.get(i);
            dto.setMenuParentId(parentId != null ? parentId : 0l);
            dto.setCreatedUser(administratorId);//创建人
            dto.setCreatedDate(new Date());//创建时间
            dto.setUpdatedUser(administratorId);//修改人
            dto.setUpdatedDate(new Date());//修改时间
            menuService.addMenu(dto);
            allMenuId.add(dto.getId());
            if(dto.getIsCommon() == 1){
                //公共菜单
                commonMenuId.add(dto.getId());
            }
            if(dto.getChildMenus() != null && dto.getChildMenus().size() > 0){
                initMenu(dto.getChildMenus(),administratorId,dto.getId());
            }
        }
    }

    @Override
    public void initSystem(List<MenuDTO> menuList,List<UserDTO> userList) {
        Long administratorId = null;
        for (UserDTO user : userList){
            user.setPassword(MD5Util.toMD5Upper(user.getPassword()));
            user.setCreatedUser(administratorId == null ? 1l : administratorId);//创建人
            user.setCreatedDate(new Date());//创建时间
            user.setUpdatedUser(administratorId == null ? 1l : administratorId);//修改人
            user.setUpdatedDate(new Date());//修改时间
            addUser(user);//新增超管用户
            if(administratorId == null){
                administratorId = user.getId();
            }
            allUserId.add(user.getId());
        }
        initMenu(menuList,administratorId,null);
        for (int i = 0; i < commonMenuId.size(); i++) {
            UserMenuDTO dto = new UserMenuDTO();
            dto.setUserId(Constants.PERMISSION_PUBLIC_MENU);//公共用户
            dto.setMenuId(commonMenuId.get(i));//菜单ID
            userMenuService.addUserMenu(dto);
        }
        for (int i = 0; i < allUserId.size(); i++) {
            for (int j = 0; j < allMenuId.size(); j++) {
                UserMenuDTO dto = new UserMenuDTO();
                dto.setUserId(allUserId.get(i));//用户ID
                dto.setMenuId(allMenuId.get(j));//菜单ID
                userMenuService.addUserMenu(dto);
            }
        }
    }

    @Override
    public UserDTO getUserByLogin(String login) {
        return userMapper.getUserByLogin(login);
    }

    @Override
    public UserDetailDTO getUserDetailByUserId(Long userId) {
        return userMapper.getUserDetailByUserId(userId);
    }

    @Override
    public void addUser(UserDTO obj) {
        userMapper.insertUser(obj);
        obj.getUserDetail().setUserId(obj.getId());//用户ID
        userMapper.insertUserDetail(obj.getUserDetail());
    }

    @Override
    public void updateUser(UserDTO obj) {
        userMapper.updateUser(obj);
        obj.getUserDetail().setUserId(obj.getId());//用户ID
        userMapper.updateUserDetailByUserId(obj.getUserDetail());
    }

    @Override
    public void deleteUserById(UserDTO obj) {
        userMapper.deleteUserById(obj);
    }

    @Override
    public UserDTO getUserById(String id) {
        return userMapper.getUserById(Long.parseLong(id));
    }

    @Override
    public DataTables queryUserByPage(DataTables dataTables) {
        List<UserDTO> list = userMapper.queryDataTablesUserByPage(dataTables);
        dataTables.setData(list);
        return dataTables;
    }


//    private void initPermission(List<MenuDTO> list){
//        String [] publicMenuCodeArray = publicMenuCodeList.split(",");
//        for (int i = 0; i < list.size(); i++) {
//            MenuDTO menuDTO = list.get(i);
//            for (int j = 0; j < publicMenuCodeArray.length; j++) {
//                if(menuDTO.getMenuCode().equals(publicMenuCodeArray[j])){
//                    UserMenuDTO dto = new UserMenuDTO();
//                    dto.setUserId(Constants.PERMISSION_PUBLIC_MENU);//公共用户
//                    dto.setMenuId(menuDTO.getId());//菜单ID
//                    userMenuService.addUserMenu(dto);
//                    break;
//                }
//            }
//        }
//    }

//    private void initAdministrator(List<MenuDTO> list){
//        UserDTO dto = new UserDTO();
//        dto.setLogin(initLogin);
//        dto.setPassword(MD5Util.toMD5Upper(initPassword));
//        dto.setIsAdministrators(0);
//        dto.setIsDelete(0);
//        dto.setCreatedUser(1l);
//        dto.setUpdatedUser(1l);
//        UserDetailDTO userDetail = new UserDetailDTO();
//        userDetail.setUserName(initUsername);//用户名
//        String ageSrc = propertiesIsNull(initAge);
//        String genderSrc = propertiesIsNull(initGender);
//        userDetail.setUserAge(Integer.parseInt(ageSrc != null ? ageSrc : "0"));//年龄
//        userDetail.setUserGender(Integer.parseInt(genderSrc != null ? genderSrc : "0"));//性别；0：未知；1：男；2：女
//        userDetail.setIdentityCard(propertiesIsNull(initIdentitycard));//身份证号
//        userDetail.setUserBirthday(new Date());//出生日期
//        userDetail.setUserPhone(propertiesIsNull(initPhone));//手机号
//        userDetail.setUserEmail(propertiesIsNull(initEmail));//邮箱
//        userDetail.setUserIcon(propertiesIsNull(initIcon));//用户头像
//        userDetail.setCompanyId(null);//公司ID
//        userDetail.setDepartmentId(null);//部门ID
//        userDetail.setIsJob(1);//是否在职；0：离职；1：在职
//        dto.setUserDetail(userDetail);
//        userService.addUser(dto);
//        for (int i = 0; i < list.size(); i++) {
//            MenuDTO menuDTO = list.get(i);
//            UserMenuDTO userMenuDTO = new UserMenuDTO();
//            userMenuDTO.setUserId(dto.getId());//公共用户
//            userMenuDTO.setMenuId(menuDTO.getId());//菜单ID
//            userMenuService.addUserMenu(userMenuDTO);
//        }
//    }

//    private void initMenu(){
//        String [] menuCodeArray = menuCodeList.split(",");
//        String [] menuNameArray = menuNameList.split(",");
//        String [] menuLinkArray = menuLinkList.split(",");
//        String [] menuIconArray = menuIconList.split(",");
//        for (int i = 0; i < menuNameArray.length; i++) {
//            MenuDTO dto = new MenuDTO();
//            dto.setMenuCode(menuCodeArray[i]);//唯一性标识
//            dto.setMenuName(menuNameArray[i]);//名称(国际化code)
//            dto.setMenuLink(menuLinkArray[i]);//链接
//            dto.setMenuIcon(propertiesIsNull(menuIconArray[i]));
//            dto.setMenuType(Constants.MENU_TYPE_HEADER);//菜单类型
//            dto.setMenuParentId(0);//父节点ID
//            dto.setMenuSort((i+1) * 10);//排序
//            dto.setCreatedUser(1l);//创建人
//            dto.setCreatedDate(new Date());//创建时间
//            dto.setUpdatedUser(1l);//修改人
//            dto.setUpdatedDate(new Date());//修改时间
//            menuService.addMenu(dto);
//        }
//    }
}
