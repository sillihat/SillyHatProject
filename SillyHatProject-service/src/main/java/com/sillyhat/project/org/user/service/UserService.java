package com.sillyhat.project.org.user.service;

import com.sillyhat.project.common.dto.DataTables;
import com.sillyhat.project.core.dto.PageDTO;
import com.sillyhat.project.org.menu.dto.MenuDTO;
import com.sillyhat.project.org.user.dto.UserDTO;
import com.sillyhat.project.org.user.dto.UserDetailDTO;

import java.util.List;

/**
 * UserService
 *
 * @author 徐士宽
 * @date 2017/3/29 14:05
 */
public interface UserService {

    public List<UserDTO> queryAllUser();

    /**
     * 系统菜单属性初始化
     */
    public void initSystem(List<MenuDTO> menuList,List<UserDTO> userList);

    public UserDTO getUserByLogin(String login);

    public UserDetailDTO getUserDetailByUserId(Long userId);

    public void addUser(UserDTO obj);

    public void updateUser(UserDTO obj);

    public void deleteUserById(UserDTO obj);

    public UserDTO getUserById(String id);

    public DataTables queryUserByPage(DataTables dataTables);

}
