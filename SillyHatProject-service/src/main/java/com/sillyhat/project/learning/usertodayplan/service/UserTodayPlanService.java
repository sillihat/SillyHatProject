package com.sillyhat.project.learning.usertodayplan.service;

import com.sillyhat.project.core.dto.PageDTO;
import com.sillyhat.project.learning.usertodayplan.dto.UserTodayPlanDTO;

import java.util.List;

/**
 * UserTodayPlanService
 *
 * @author 徐士宽
 * @date 2017/4/10 13:35
 */
public interface UserTodayPlanService {

    public List<UserTodayPlanDTO> queryUserTodayPlanAll();

    public PageDTO queryUserTodayPlanPageByPage(long userId,PageDTO page);

    public int queryUserTodayPlanCountByParams(long userId,int learningDateNum);

    public void addUserTodayPlan(UserTodayPlanDTO dto);

    public void updateUserTodayPlan(UserTodayPlanDTO dto);

}
