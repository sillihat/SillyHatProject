package com.sillyhat.project.learning.wordrepository.service.impl;

import com.sillyhat.project.learning.wordrepository.dto.WordRepositoryDTO;
import com.sillyhat.project.learning.wordrepository.mapper.WordRepositoryMapper;
import com.sillyhat.project.learning.wordrepository.service.WordRepositoryService;

import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

/**
 * WordRepositoryServiceImpl
 *
 * @author 徐士宽
 * @date 2017/4/7 9:58
 */
@Service
public class WordRepositoryServiceImpl implements WordRepositoryService {

    @Resource
    private WordRepositoryMapper wordRepositoryMapper;

    @Override
    public List<WordRepositoryDTO> queryWordRepositoryAll() {
        return wordRepositoryMapper.queryWordRepositoryAll();
    }

    @Override
    public WordRepositoryDTO getWordRepositoryById(String id) {
        return wordRepositoryMapper.getWordRepositoryById(id);
    }

    @Override
    public WordRepositoryDTO getWordRepositoryByWord(String word) {
        return wordRepositoryMapper.getWordRepositoryByWord(word);
    }

    @Override
    public void addWordRepository(WordRepositoryDTO dto) {
        long id = wordRepositoryMapper.addWordRepository(dto);
        dto.setId(id);
    }

    @Override
    public void updateWordRepository(WordRepositoryDTO dto) {
        wordRepositoryMapper.updateWordRepository(dto);
    }

    @Override
    public void deleteWordRepositoryById(String id) {
        wordRepositoryMapper.deleteWordRepositoryById(id);
    }
}
