package com.sillyhat.project.learning.userlearningword.service.impl;

import com.sillyhat.project.learning.userlearningword.dto.UserLearningWordDTO;
import com.sillyhat.project.learning.userlearningword.mapper.UserLearningWordMapper;
import com.sillyhat.project.learning.userlearningword.service.UserLearningWordService;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

/**
 * UserLearningWordServiceImpl
 *
 * @author 徐士宽
 * @date 2017/4/7 16:43
 */
@Service
public class UserLearningWordServiceImpl implements UserLearningWordService {

    @Resource
    private UserLearningWordMapper userLearningWordMapper;

    @Override
    public List<UserLearningWordDTO> queryUserLearningWordAll() {
        return userLearningWordMapper.queryUserLearningWordAll();
    }

    @Override
    public List<UserLearningWordDTO> queryUserLearningWordByUserId(long userId) {
        return userLearningWordMapper.queryUserLearningWordByUserId(userId);
    }

    @Override
    public void addUserLearningWord(UserLearningWordDTO dto) {
        long id = userLearningWordMapper.addUserLearningWord(dto);
        dto.setId(id);
    }

    @Override
    public void updateUserLearningWord(UserLearningWordDTO dto) {
        userLearningWordMapper.updateUserLearningWord(dto);
    }
}
