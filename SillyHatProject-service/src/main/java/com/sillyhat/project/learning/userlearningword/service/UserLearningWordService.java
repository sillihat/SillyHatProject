package com.sillyhat.project.learning.userlearningword.service;

import com.sillyhat.project.learning.userlearningword.dto.UserLearningWordDTO;

import java.util.List;

/**
 * UserLearningWordService
 *
 * @author 徐士宽
 * @date 2017/4/7 16:42
 */
public interface UserLearningWordService {

    public List<UserLearningWordDTO> queryUserLearningWordAll();

    public List<UserLearningWordDTO> queryUserLearningWordByUserId(long userId);

    public void addUserLearningWord(UserLearningWordDTO dto);

    public void updateUserLearningWord(UserLearningWordDTO dto);

}
