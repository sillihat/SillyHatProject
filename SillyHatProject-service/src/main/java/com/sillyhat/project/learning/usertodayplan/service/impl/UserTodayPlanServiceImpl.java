package com.sillyhat.project.learning.usertodayplan.service.impl;

import com.sillyhat.project.common.utils.SillyHatDateUtils;
import com.sillyhat.project.core.dto.PageDTO;
import com.sillyhat.project.learning.usertodayplan.mapper.UserTodayPlanMapper;
import com.sillyhat.project.learning.usertodayplan.service.UserTodayPlanService;
import com.sillyhat.project.learning.usertodayplan.dto.UserTodayPlanDTO;
import org.springframework.stereotype.Service;
import java.util.List;
import javax.annotation.Resource;

/**
 * UserTodayServiceImpl
 *
 * @author 徐士宽
 * @date 2017/4/10 13:36
 */
@Service
public class UserTodayPlanServiceImpl implements UserTodayPlanService {

    @Resource
    private UserTodayPlanMapper userTodayPlanMapper;


    @Override
    public List<UserTodayPlanDTO> queryUserTodayPlanAll() {
        return userTodayPlanMapper.queryUserTodayPlanAll();
    }

    @Override
    public PageDTO queryUserTodayPlanPageByPage(long userId,PageDTO page) {
        int learningDateNum = SillyHatDateUtils.getTodayNum();
        page.putParams("userId",userId);
        page.putParams("learningDateNum",learningDateNum);
        int totalCount = userTodayPlanMapper.queryUserTodayPlanCountByParams(userId,learningDateNum);
        if(totalCount > 0){
            //有今日计划
            page.setTotalCount(totalCount);

            List<UserTodayPlanDTO> list = userTodayPlanMapper.queryUserTodayPlanListPageByPage(page);
            page.setResultList(list);
        }else{
            //无今日计划，需要生成今日计划


        }
        return page;
    }

    @Override
    public int queryUserTodayPlanCountByParams(long userId, int learningDateNum) {
        return userTodayPlanMapper.queryUserTodayPlanCountByParams(userId,learningDateNum);
    }

    @Override
    public void addUserTodayPlan(UserTodayPlanDTO dto) {
        long id = userTodayPlanMapper.addUserTodayPlan(dto);
        dto.setId(id);
    }

    @Override
    public void updateUserTodayPlan(UserTodayPlanDTO dto) {
        userTodayPlanMapper.updateUserTodayPlan(dto);
    }
}
