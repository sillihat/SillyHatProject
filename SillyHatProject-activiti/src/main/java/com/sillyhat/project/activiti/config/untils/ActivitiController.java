package com.sillyhat.project.activiti.config.untils;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * ActivitiController
 *
 * @author 徐士宽
 * @date 2017/5/11 10:43
 */
public abstract class ActivitiController {


    public void setAttribute(String key,Object value){
        RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
        requestAttributes.setAttribute(key,value,0);
    }

}
