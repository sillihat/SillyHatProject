package com.sillyhat.project.activiti.config.exception;

/**
 * BPMException
 *
 * @author 徐士宽
 * @date 2017/5/11 9:58
 */
public class BPMException extends Exception{

    public BPMException() {
        super();
    }

    public BPMException(String message) {
        super(message);
    }
}
