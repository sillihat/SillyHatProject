package com.sillyhat.project.activiti.config.builder;

import com.sillyhat.project.activiti.config.untils.ActivitiConstants;
import com.sillyhat.project.activiti.process.service.Headler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 反射到相对应的业务类
 *
 * @author 徐士宽
 * @date 2017/5/11 9:52
 */
public class HeadlerBuilder {

    private static final Logger logger = LoggerFactory.getLogger(HeadlerBuilder.class);

    public static Headler getProcessConfigService(String processCode, String processStartNode) {
        try {
            String className = ActivitiConstants.PROCESS_MODEL + processCode + "." + processStartNode;
            Class<?> thisClass = Class.forName(className);
            return (Headler) thisClass.newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logger.error("ClassNotFoundException：" + e);
        } catch (InstantiationException e) {
            e.printStackTrace();
            logger.error("InstantiationException：" + e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            logger.error("IllegalAccessException：" + e);
        }
        return null;
    }

    public static Class<?> getProcessDomain(String processCode) {
        try {
//            String packageName = Constants.PROCESS_BEGIN + processCode + Constants.PROCESS_DOMAIN_END + getDoamin(processCode);
            String packageName = processCode;
            Class<?> thisClass = Class.forName(packageName);
            return thisClass;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logger.error("ClassNotFoundException：" + e);
        }
        return null;
    }
    public static Class<?> getProcessService(String processCode) {
        try {
//            String packageName = Constants.PROCESS_BEGIN + processCode + Constants.PROCESS_SERVICE_END + getDoamin(processCode) + "Service";
            String packageName = processCode;
            Class<?> thisClass = Class.forName(packageName);
            return thisClass;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logger.error("ClassNotFoundException：" + e);
        }
        return null;
    }

    private static String getDoamin(String processCode){
        String domain = processCode.substring(0, 1).toUpperCase() + processCode.substring(1, processCode.length());
        return domain;
    }
}
