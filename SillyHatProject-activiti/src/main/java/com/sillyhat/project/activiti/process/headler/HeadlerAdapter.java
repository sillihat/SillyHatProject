package com.sillyhat.project.activiti.process.headler;

import com.sillyhat.project.activiti.config.exception.BPMException;
import com.sillyhat.project.activiti.process.service.Headler;

import org.springframework.context.ApplicationContext;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * HeadlerAdapter
 *
 * @author 徐士宽
 * @date 2017/5/11 9:52
 */
public abstract class HeadlerAdapter implements Headler {

    /**
     * 进入页面，存储参数前
     * @param context
     * @param request
     * @param params
     * @throws BPMException
     */
    @Override
    public void executeApplyBefore(ApplicationContext context,
                                   HttpServletRequest request, Map<String, Object> params)
            throws BPMException {

    }

    /**
     * 进入页面，存储参数后
     * @param context
     * @param request
     * @param params
     * @throws BPMException
     */
    @Override
    public void executeApplyAfter(ApplicationContext context,
                                  HttpServletRequest request, Map<String, Object> params)
            throws BPMException {

    }

    /**
     * 提交流程前
     * @param context
     * @param request
     * @param params
     * @throws BPMException
     */
    @Override
    public void executeSubmitBefore(ApplicationContext context,
                                    HttpServletRequest request, Map<String, Object> params)
            throws BPMException {

    }

    /**
     * 提交流程后
     * @param context
     * @param request
     * @param params
     * @throws BPMException
     */
    @Override
    public void executeSubmitAfter(ApplicationContext context,
                                   HttpServletRequest request, Map<String, Object> params)
            throws BPMException {

    }
    
}
