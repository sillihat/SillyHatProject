package com.sillyhat.project.activiti.process.service;
import com.sillyhat.project.activiti.config.exception.BPMException;
import org.springframework.context.ApplicationContext;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * Headler
 *
 * @author 徐士宽
 * @date 2017/5/11 9:52
 */
public interface Headler {

    //页面传值前
    void executeApplyBefore(ApplicationContext context, HttpServletRequest request, Map<String, Object> params) throws BPMException;
    //页面传值后
    void executeApplyAfter(ApplicationContext context,HttpServletRequest request, Map<String, Object> params) throws BPMException;

    //提交前
    void executeSubmitBefore(ApplicationContext context,HttpServletRequest request, Map<String, Object> params) throws BPMException;
    //提交后
    void executeSubmitAfter(ApplicationContext context,HttpServletRequest request, Map<String, Object> params) throws BPMException;
}
