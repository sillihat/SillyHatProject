package com.sillyhat.project.learning.wordrepository.mapper;

import com.sillyhat.project.learning.wordrepository.dto.WordRepositoryDTO;

import java.util.List;

/**
 * WordRepositoryMapper
 *
 * @author 徐士宽
 * @date 2017/4/7 10:03
 */
public interface WordRepositoryMapper {

    /**
     * 查询词库列表
     * @return
     */
    public List<WordRepositoryDTO> queryWordRepositoryAll();

    /**
     *  根据主键查询词库信息
     * @param id
     * @return
     */
    public WordRepositoryDTO getWordRepositoryById(String id);

    /**
     *  根据单词查询词库信息
     * @param word
     * @return
     */
    public WordRepositoryDTO getWordRepositoryByWord(String word);

    /**
     * 新增词库
     * @param dto
     */
    public long addWordRepository(WordRepositoryDTO dto);

    /**
     * 修改词库
     * @param dto
     */
    public void updateWordRepository(WordRepositoryDTO dto);

    /**
     * 根据主键删除
     * @param id
     */
    public void deleteWordRepositoryById(String id);

}
