package com.sillyhat.project.learning.usertodayplan.mapper;

import com.sillyhat.project.core.dto.PageDTO;
import com.sillyhat.project.learning.usertodayplan.dto.UserTodayPlanDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * UserTodayPlanMapper
 *
 * @author 徐士宽
 * @date 2017/4/10 13:35
 */
public interface UserTodayPlanMapper {

    public List<UserTodayPlanDTO> queryUserTodayPlanAll();

    public List<UserTodayPlanDTO> queryUserTodayPlanListAll(@Param("userId")long userId,@Param("learningDateNum")int learningDateNum);

    public List<UserTodayPlanDTO> queryUserTodayPlanListPageByPage(PageDTO page);

    public int queryUserTodayPlanCountByParams(@Param("userId")long userId,@Param("learningDateNum")int learningDateNum);

    public long addUserTodayPlan(UserTodayPlanDTO dto);

    public void updateUserTodayPlan(UserTodayPlanDTO dto);

}
