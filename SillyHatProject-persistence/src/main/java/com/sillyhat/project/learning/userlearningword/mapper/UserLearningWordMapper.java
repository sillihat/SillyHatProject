package com.sillyhat.project.learning.userlearningword.mapper;

import com.sillyhat.project.learning.userlearningword.dto.UserLearningWordDTO;
import java.util.List;

/**
 * UserLearningWordMapper
 *
 * @author 徐士宽
 * @date 2017/4/7 16:45
 */
public interface UserLearningWordMapper {

    public List<UserLearningWordDTO> queryUserLearningWordAll();

    public List<UserLearningWordDTO> queryUserLearningWordByUserId(long userId);

    public long addUserLearningWord(UserLearningWordDTO dto);

    public void updateUserLearningWord(UserLearningWordDTO dto);

}
