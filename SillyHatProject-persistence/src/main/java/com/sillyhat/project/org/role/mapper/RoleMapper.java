package com.sillyhat.project.org.role.mapper;

import com.sillyhat.project.common.dto.DataTables;
import com.sillyhat.project.org.role.dto.RoleDTO;

import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface RoleMapper {


    /**
	 * <p>Title: insertRole</p>
	 * <p>Description: </p>保存
	 * @param dto
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 */
	public long insertRole(RoleDTO dto);

	/**
	 * <p>Title: update</p>
	 * <p>Description: </p>修改
	 * @param dto
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 */
	public void updateRole(RoleDTO dto);

	/**
     * <p>Title: updateRoleByPrimaryKeySelective</p>
     * <p>Description: </p>根据主键ID，选择性的修改数据
     * @param dto
     * @return
     * @author XUSHIKUAN
     * @date 2017-05-27
     */
    public Integer updateRoleByPrimaryKeySelective(RoleDTO dto);

	/**
	 * <p>Title: deleteRoleByPrimaryKey</p>
	 * <p>Description: </p>根据主键删除
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 */
	public Integer deleteRoleByPrimaryKey(@Param("id") long id);

	/**
	 * <p>Title: selectRoleByPrimaryKey</p>
	 * <p>Description: </p>根据主键查询
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 */
	public RoleDTO selectRoleByPrimaryKey(@Param("id") long id);

	public List<RoleDTO> queryDataTablesMenuByPage(DataTables dataTables);

	/**
	 * <p>Title: queryRoleAll</p>
	 * <p>Description: </p>查询所有
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 */
	public List<RoleDTO> queryRoleAll();
}

