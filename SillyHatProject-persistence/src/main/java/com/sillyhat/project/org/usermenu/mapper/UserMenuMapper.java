package com.sillyhat.project.org.usermenu.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import com.sillyhat.project.core.dto.PageDTO;
import com.sillyhat.project.org.usermenu.dto.UserMenuDTO;


public interface UserMenuMapper {


    /**
	 * <p>Title: insertUserMenu</p>
	 * <p>Description: </p>保存
	 * @param dto
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 */
	public long insertUserMenu(UserMenuDTO dto);

	/**
	 * <p>Title: update</p>
	 * <p>Description: </p>修改
	 * @param dto
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 */
	public void updateUserMenu(UserMenuDTO dto);

	/**
     * <p>Title: updateUserMenuByPrimaryKeySelective</p>
     * <p>Description: </p>根据主键ID，选择性的修改数据
     * @param dto
     * @return
     * @author XUSHIKUAN
     * @date 2017-04-14
     */
    public Integer updateUserMenuByPrimaryKeySelective(UserMenuDTO dto);

	/**
	 * <p>Title: deleteUserMenuByPrimaryKey</p>
	 * <p>Description: </p>根据主键删除
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 */
	public Integer deleteUserMenuByPrimaryKey(@Param("id") long id);

	/**
	 * <p>Title: selectUserMenuByPrimaryKey</p>
	 * <p>Description: </p>根据主键查询
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 */
	public UserMenuDTO selectUserMenuByPrimaryKey(@Param("id") long id);

	/**
	 * <p>Title: queryUserMenuByPage</p>
	 * <p>Description: </p>分页查询
	 * @param page
	 * @return	PageDTO：分页查询实体类
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 */
	public List<UserMenuDTO> queryUserMenuByPage(PageDTO page);


	/**
	 * <p>Title: searchPagingCount</p>
	 * <p>Description: </p>查询总数
	 * @param param 查询条件
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 */
    public int getUserMenuCountByParams(Map<String, Object> params);

	/**
	 * <p>Title: queryUserMenuAll</p>
	 * <p>Description: </p>查询所有
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 */
	public List<UserMenuDTO> queryUserMenuAll();
}

