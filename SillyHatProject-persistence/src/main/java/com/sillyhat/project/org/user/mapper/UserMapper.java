package com.sillyhat.project.org.user.mapper;

import com.sillyhat.project.common.dto.DataTables;
import com.sillyhat.project.core.dto.PageDTO;
import com.sillyhat.project.org.user.dto.UserDTO;
import com.sillyhat.project.org.user.dto.UserDetailDTO;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * UserMapper
 *
 * @author 徐士宽
 * @date 2017/3/29 13:55
 */
public interface UserMapper {

    public List<UserDTO> queryAllUser();

    public UserDTO getUserByLogin(@Param("login") String login);

    public Long insertUser(UserDTO obj);

    public int updateUser(UserDTO obj);

    public Long insertUserDetail(UserDetailDTO obj);

    public int updateUserDetailByUserId(UserDetailDTO obj);

    public UserDTO getUserById(@Param("id") Long id);

    public int deleteUserById(UserDTO obj);

    /**
     * <p>Title: queryMenuByPage</p>
     * <p>Description: </p>分页查询
     * @author XUSHIKUAN
     * @date 2017-04-12
     */
//    public List<UserDTO> queryUserByPage(DataTables dataTables);
    public List<UserDTO> queryDataTablesUserByPage(DataTables dataTables);
//    public DataTables queryDataTablesUserByPage(DataTables dataTables);


    /**
     * <p>Title: searchPagingCount</p>
     * <p>Description: </p>查询总数
     * @param param 查询条件
     * @author XUSHIKUAN
     * @date 2017-04-12
     */
    public int getUserCountByParams(Map<String, Object> params);

    public UserDetailDTO getUserDetailByUserId(@Param("userId")Long userId);

}
