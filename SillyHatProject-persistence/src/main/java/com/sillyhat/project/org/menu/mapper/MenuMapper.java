package com.sillyhat.project.org.menu.mapper;

import com.sillyhat.project.common.dto.DataTables;
import com.sillyhat.project.org.menu.dto.MenuDTO;

import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface MenuMapper {


    /**
	 * <p>Title: insertMenu</p>
	 * <p>Description: </p>保存
	 * @param dto
	 * @author XUSHIKUAN
	 * @date 2017-04-12
	 */
	public Long insertMenu(MenuDTO dto);

	/**
	 * <p>Title: update</p>
	 * <p>Description: </p>修改
	 * @param dto
	 * @author XUSHIKUAN
	 * @date 2017-04-12
	 */
	public void updateMenu(MenuDTO dto);

	/**
     * <p>Title: updateMenuByPrimaryKeySelective</p>
     * <p>Description: </p>根据主键ID，选择性的修改数据
     * @param dto
     * @return
     * @author XUSHIKUAN
     * @date 2017-04-12
     */
    public Integer updateMenuByPrimaryKeySelective(MenuDTO dto);

	/**
	 * <p>Title: deleteMenuByPrimaryKey</p>
	 * <p>Description: </p>根据主键删除
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-04-12
	 */
	public Integer deleteMenuByPrimaryKey(@Param("id")long id);

	/**
	 * <p>Title: selectMenuByPrimaryKey</p>
	 * <p>Description: </p>根据主键查询
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-04-12
	 */
	public MenuDTO selectMenuByPrimaryKey(@Param("id")long id);

	/**
	 * <p>Title: queryMenuAll</p>
	 * <p>Description: </p>查询所有
	 * @author XUSHIKUAN
	 * @date 2017-04-12
	 */
	public List<MenuDTO> queryMenuAll();

	/**
	 * 获得权限下的菜单列表
	 * @param permissionId
	 * @return
	 */
	public List<MenuDTO> queryHeaderMenuByPermission(@Param("permissionId")long permissionId);

	public List<MenuDTO> queryDataTablesMenuByPage(DataTables dataTables);

	public List<MenuDTO> querySafeManagerHeaderMenu(Long menuId);
}

