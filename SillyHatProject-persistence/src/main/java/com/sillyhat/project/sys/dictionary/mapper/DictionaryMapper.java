package com.sillyhat.project.sys.dictionary.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import com.sillyhat.project.core.dto.PageDTO;
import com.sillyhat.project.sys.dictionary.dto.DictionaryDTO;


public interface DictionaryMapper {


    /**
	 * <p>Title: insertDictionary</p>
	 * <p>Description: </p>保存
	 * @param dto
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
	public long insertDictionary(DictionaryDTO dto);

	/**
	 * <p>Title: update</p>
	 * <p>Description: </p>修改
	 * @param dto
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
	public void updateDictionary(DictionaryDTO dto);

	/**
     * <p>Title: updateDictionaryByPrimaryKeySelective</p>
     * <p>Description: </p>根据主键ID，选择性的修改数据
     * @param dto
     * @return
     * @author XUSHIKUAN
     * @date 2017-07-05
     */
    public Integer updateDictionaryByPrimaryKeySelective(DictionaryDTO dto);

	/**
	 * <p>Title: deleteDictionaryByPrimaryKey</p>
	 * <p>Description: </p>根据主键删除
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
	public Integer deleteDictionaryByPrimaryKey(@Param("id") long id);

	/**
	 * <p>Title: selectDictionaryByPrimaryKey</p>
	 * <p>Description: </p>根据主键查询
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
	public DictionaryDTO selectDictionaryByPrimaryKey(@Param("id") long id);

	/**
	 * 根据code查询
	 * @param dictCode
	 * @return
	 */
	public DictionaryDTO selectDictionaryByDictCode(@Param("dictCode") String dictCode);

	/**
	 * 根据code查询
	 * @param dictCode
	 * @return
	 */
	public List<DictionaryDTO> selectDictionaryListByDictCode(@Param("dictCode") String dictCode);

	/**
	 * <p>Title: queryDictionaryByPage</p>
	 * <p>Description: </p>分页查询
	 * @param page
	 * @return	PageDTO：分页查询实体类
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
	public List<DictionaryDTO> queryDictionaryByPage(PageDTO page);


	/**
	 * <p>Title: searchPagingCount</p>
	 * <p>Description: </p>查询总数
	 * @param param 查询条件
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
    public int getDictionaryCountByParams(Map<String, Object> params);

	/**
	 * <p>Title: queryDictionaryAll</p>
	 * <p>Description: </p>查询所有
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 */
	public List<DictionaryDTO> queryDictionaryAll();
}

