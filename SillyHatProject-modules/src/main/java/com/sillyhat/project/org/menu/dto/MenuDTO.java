package com.sillyhat.project.org.menu.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class MenuDTO implements Serializable {

	private static final long serialVersionUID = 7086129897946459783L;

	private Long id;//主键

	private String menuCode;//唯一性标识

	private String menuName;//名称(国际化code)

	private String menuLink;//链接

	private int menuType;//菜单类型

	private String menuIcon;//菜单图标

	private Long menuParentId;//父节点ID

	private int menuSort;//排序

	private int isDisabled;//是否禁用;0:未禁用;1:已禁用

	private int isDelete;//是否删除;0:未删除;1:已删除

	private Long createdUser;//创建人

	private Date createdDate;//创建时间

	private Long updatedUser;//修改人

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updatedDate;//修改时间

	private String updatedUserName;
	private String menuParentCode;//父节点名称

	private int isCommon;//初始化菜单时使用；是否公共页面

	private List<MenuDTO> childMenus;

	public List<MenuDTO> getChildMenus() {
		return childMenus;
	}

	public void setChildMenus(List<MenuDTO> childMenus) {
		this.childMenus = childMenus;
	}

	public int getIsCommon() {
		return isCommon;
	}

	public void setIsCommon(int isCommon) {
		this.isCommon = isCommon;
	}

	/**
	 * <p>Title: getId</p>
	 * <p>Description: </p>主键
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:Long               
	 */                             
	public Long getId(){
		return  id;
	}


	/**                             
	 * <p>Title: getMenuCode</p>
	 * <p>Description: </p>唯一性标识
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:String               
	 */                             
	public String getMenuCode(){
		return  menuCode;
	}


	/**                             
	 * <p>Title: getMenuName</p>
	 * <p>Description: </p>名称(国际化code)
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:String               
	 */                             
	public String getMenuName(){
		return  menuName;
	}


	/**                             
	 * <p>Title: getMenuLink</p>
	 * <p>Description: </p>链接
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:String               
	 */                             
	public String getMenuLink(){
		return  menuLink;
	}


	/**                             
	 * <p>Title: getMenuType</p>
	 * <p>Description: </p>菜单类型
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:int               
	 */                             
	public int getMenuType(){
		return  menuType;
	}


	/**                             
	 * <p>Title: getMenuParentId</p>
	 * <p>Description: </p>父节点ID
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:int               
	 */                             
	public Long getMenuParentId(){
		return  menuParentId;
	}


	/**                             
	 * <p>Title: getMenuSort</p>
	 * <p>Description: </p>排序
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:int               
	 */                             
	public int getMenuSort(){
		return  menuSort;
	}


	/**                             
	 * <p>Title: getIsDisabled</p>
	 * <p>Description: </p>是否禁用;0:未禁用;1:已禁用
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:int               
	 */                             
	public int getIsDisabled(){
		return  isDisabled;
	}


	/**                             
	 * <p>Title: getIsDelete</p>
	 * <p>Description: </p>是否删除;0:未删除;1:已删除
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:int               
	 */                             
	public int getIsDelete(){
		return  isDelete;
	}


	/**                             
	 * <p>Title: getCreatedUser</p>
	 * <p>Description: </p>创建人
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:Long               
	 */                             
	public Long getCreatedUser(){
		return  createdUser;
	}


	/**                             
	 * <p>Title: getCreatedDate</p>
	 * <p>Description: </p>创建时间
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:Date               
	 */                             
	public Date getCreatedDate(){
		return  createdDate;
	}


	/**                             
	 * <p>Title: getUpdatedUser</p>
	 * <p>Description: </p>修改人
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:Long               
	 */                             
	public Long getUpdatedUser(){
		return  updatedUser;
	}


	/**                             
	 * <p>Title: getUpdatedDate</p>
	 * <p>Description: </p>修改时间
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:Date               
	 */                             
	public Date getUpdatedDate(){
		return  updatedDate;
	}



	/**                             
	 * <p>Title: setId</p>
	 * <p>Description: </p>主键
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setId(Long id){
		this.id = id;
	}


	/**                             
	 * <p>Title: setMenuCode</p>
	 * <p>Description: </p>唯一性标识
	 * @param menuCode
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setMenuCode(String menuCode){
		this.menuCode = menuCode;
	}


	/**                             
	 * <p>Title: setMenuName</p>
	 * <p>Description: </p>名称(国际化code)
	 * @param menuName
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setMenuName(String menuName){
		this.menuName = menuName;
	}


	/**                             
	 * <p>Title: setMenuLink</p>
	 * <p>Description: </p>链接
	 * @param menuLink
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setMenuLink(String menuLink){
		this.menuLink = menuLink;
	}


	/**                             
	 * <p>Title: setMenuType</p>
	 * <p>Description: </p>菜单类型
	 * @param menuType
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setMenuType(int menuType){
		this.menuType = menuType;
	}


	/**                             
	 * <p>Title: setMenuParentId</p>
	 * <p>Description: </p>父节点ID
	 * @param menuParentId
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setMenuParentId(Long menuParentId){
		this.menuParentId = menuParentId;
	}


	/**                             
	 * <p>Title: setMenuSort</p>
	 * <p>Description: </p>排序
	 * @param menuSort
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setMenuSort(int menuSort){
		this.menuSort = menuSort;
	}


	/**                             
	 * <p>Title: setIsDisabled</p>
	 * <p>Description: </p>是否禁用;0:未禁用;1:已禁用
	 * @param isDisabled
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setIsDisabled(int isDisabled){
		this.isDisabled = isDisabled;
	}


	/**                             
	 * <p>Title: setIsDelete</p>
	 * <p>Description: </p>是否删除;0:未删除;1:已删除
	 * @param isDelete
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setIsDelete(int isDelete){
		this.isDelete = isDelete;
	}


	/**                             
	 * <p>Title: setCreatedUser</p>
	 * <p>Description: </p>创建人
	 * @param createdUser
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setCreatedUser(Long createdUser){
		this.createdUser = createdUser;
	}


	/**                             
	 * <p>Title: setCreatedDate</p>
	 * <p>Description: </p>创建时间
	 * @param createdDate
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setCreatedDate(Date createdDate){
		this.createdDate = createdDate;
	}


	/**                             
	 * <p>Title: setUpdatedUser</p>
	 * <p>Description: </p>修改人
	 * @param updatedUser
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setUpdatedUser(Long updatedUser){
		this.updatedUser = updatedUser;
	}


	/**                             
	 * <p>Title: setUpdatedDate</p>
	 * <p>Description: </p>修改时间
	 * @param updatedDate
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setUpdatedDate(Date updatedDate){
		this.updatedDate = updatedDate;
	}

	public String getMenuIcon() {
		return menuIcon;
	}

	public void setMenuIcon(String menuIcon) {
		this.menuIcon = menuIcon;
	}

	public String getMenuParentCode() {
		return menuParentCode;
	}

	public void setMenuParentCode(String menuParentCode) {
		this.menuParentCode = menuParentCode;
	}

	public String getUpdatedUserName() {
		return updatedUserName;
	}

	public void setUpdatedUserName(String updatedUserName) {
		this.updatedUserName = updatedUserName;
	}

	@Override
	public String toString() {
		return "MenuDTO{" +
				"id=" + id +
				", menuCode='" + menuCode + '\'' +
				", menuName='" + menuName + '\'' +
				", menuLink='" + menuLink + '\'' +
				", menuType=" + menuType +
				", menuIcon='" + menuIcon + '\'' +
				", menuParentId=" + menuParentId +
				", menuParentCode='" + menuParentCode + '\'' +
				", menuSort=" + menuSort +
				", isDisabled=" + isDisabled +
				", isDelete=" + isDelete +
				", createdUser=" + createdUser +
				", createdDate=" + createdDate +
				", updatedUser=" + updatedUser +
				", updatedDate=" + updatedDate +
				'}';
	}
}

