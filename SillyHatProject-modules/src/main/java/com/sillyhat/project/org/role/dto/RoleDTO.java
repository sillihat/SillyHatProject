package com.sillyhat.project.org.role.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


public class RoleDTO implements Serializable {

	private static final long serialVersionUID = -9052780972348644652L;

	private Long id;//主键

	private String roleCode;//唯一性标识

	private String roleName;//名称(国际化code)

	private Long roleParentId;//父节点ID

	private int roleSort;//排序

	private int isDisabled;//是否禁用;0:未禁用;1:已禁用

	private Long createdUser;//创建人

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;//创建时间

	private Long updatedUser;//修改人

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updatedDate;//修改时间

	private String updatedUserName;

	/**                             
	 * <p>Title: getId</p>
	 * <p>Description: </p>主键
	 * @author XUSHIKUAN
	 * @date 2017-05-27              
	 * @return:Long               
	 */                             
	public Long getId(){
		return  id;
	}


	/**                             
	 * <p>Title: getRoleCode</p>
	 * <p>Description: </p>唯一性标识
	 * @author XUSHIKUAN
	 * @date 2017-05-27              
	 * @return:String               
	 */                             
	public String getRoleCode(){
		return  roleCode;
	}


	/**                             
	 * <p>Title: getRoleName</p>
	 * <p>Description: </p>名称(国际化code)
	 * @author XUSHIKUAN
	 * @date 2017-05-27              
	 * @return:String               
	 */                             
	public String getRoleName(){
		return  roleName;
	}


	/**                             
	 * <p>Title: getRoleParentId</p>
	 * <p>Description: </p>父节点ID
	 * @author XUSHIKUAN
	 * @date 2017-05-27              
	 * @return:int               
	 */                             
	public Long getRoleParentId(){
		return  roleParentId;
	}


	/**                             
	 * <p>Title: getRoleSort</p>
	 * <p>Description: </p>排序
	 * @author XUSHIKUAN
	 * @date 2017-05-27              
	 * @return:int               
	 */                             
	public int getRoleSort(){
		return  roleSort;
	}


	/**                             
	 * <p>Title: getIsDisabled</p>
	 * <p>Description: </p>是否禁用;0:未禁用;1:已禁用
	 * @author XUSHIKUAN
	 * @date 2017-05-27              
	 * @return:int               
	 */                             
	public int getIsDisabled(){
		return  isDisabled;
	}


	/**                             
	 * <p>Title: getCreatedUser</p>
	 * <p>Description: </p>创建人
	 * @author XUSHIKUAN
	 * @date 2017-05-27              
	 * @return:Long               
	 */                             
	public Long getCreatedUser(){
		return  createdUser;
	}


	/**                             
	 * <p>Title: getCreatedDate</p>
	 * <p>Description: </p>创建时间
	 * @author XUSHIKUAN
	 * @date 2017-05-27              
	 * @return:Date               
	 */                             
	public Date getCreatedDate(){
		return  createdDate;
	}


	/**                             
	 * <p>Title: getUpdatedUser</p>
	 * <p>Description: </p>修改人
	 * @author XUSHIKUAN
	 * @date 2017-05-27              
	 * @return:Long               
	 */                             
	public Long getUpdatedUser(){
		return  updatedUser;
	}


	/**                             
	 * <p>Title: getUpdatedDate</p>
	 * <p>Description: </p>修改时间
	 * @author XUSHIKUAN
	 * @date 2017-05-27              
	 * @return:Date               
	 */                             
	public Date getUpdatedDate(){
		return  updatedDate;
	}



	/**                             
	 * <p>Title: setId</p>
	 * <p>Description: </p>主键
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 * @return:void                 
	 */                             
	public void setId(Long id){
		this.id = id;
	}


	/**                             
	 * <p>Title: setRoleCode</p>
	 * <p>Description: </p>唯一性标识
	 * @param roleCode
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 * @return:void                 
	 */                             
	public void setRoleCode(String roleCode){
		this.roleCode = roleCode;
	}


	/**                             
	 * <p>Title: setRoleName</p>
	 * <p>Description: </p>名称(国际化code)
	 * @param roleName
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 * @return:void                 
	 */                             
	public void setRoleName(String roleName){
		this.roleName = roleName;
	}


	/**                             
	 * <p>Title: setRoleParentId</p>
	 * <p>Description: </p>父节点ID
	 * @param roleParentId
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 * @return:void                 
	 */                             
	public void setRoleParentId(Long roleParentId){
		this.roleParentId = roleParentId;
	}


	/**                             
	 * <p>Title: setRoleSort</p>
	 * <p>Description: </p>排序
	 * @param roleSort
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 * @return:void                 
	 */                             
	public void setRoleSort(int roleSort){
		this.roleSort = roleSort;
	}


	/**                             
	 * <p>Title: setIsDisabled</p>
	 * <p>Description: </p>是否禁用;0:未禁用;1:已禁用
	 * @param isDisabled
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 * @return:void                 
	 */                             
	public void setIsDisabled(int isDisabled){
		this.isDisabled = isDisabled;
	}


	/**                             
	 * <p>Title: setCreatedUser</p>
	 * <p>Description: </p>创建人
	 * @param createdUser
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 * @return:void                 
	 */                             
	public void setCreatedUser(Long createdUser){
		this.createdUser = createdUser;
	}


	/**                             
	 * <p>Title: setCreatedDate</p>
	 * <p>Description: </p>创建时间
	 * @param createdDate
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 * @return:void                 
	 */                             
	public void setCreatedDate(Date createdDate){
		this.createdDate = createdDate;
	}


	/**                             
	 * <p>Title: setUpdatedUser</p>
	 * <p>Description: </p>修改人
	 * @param updatedUser
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 * @return:void                 
	 */                             
	public void setUpdatedUser(Long updatedUser){
		this.updatedUser = updatedUser;
	}


	/**                             
	 * <p>Title: setUpdatedDate</p>
	 * <p>Description: </p>修改时间
	 * @param updatedDate
	 * @author XUSHIKUAN
	 * @date 2017-05-27
	 * @return:void                 
	 */                             
	public void setUpdatedDate(Date updatedDate){
		this.updatedDate = updatedDate;
	}


	public String getUpdatedUserName() {
		return updatedUserName;
	}

	public void setUpdatedUserName(String updatedUserName) {
		this.updatedUserName = updatedUserName;
	}

	@Override
	public String toString() {
		return "Role{" +
			"id='" + id + '\'' +  "," +
			"roleCode='" + roleCode + '\'' +  "," +
			"roleName='" + roleName + '\'' +  "," +
			"roleParentId='" + roleParentId + '\'' +  "," +
			"roleSort='" + roleSort + '\'' +  "," +
			"isDisabled='" + isDisabled + '\'' +  "," +
			"createdUser='" + createdUser + '\'' +  "," +
			"createdDate='" + createdDate + '\'' +  "," +
			"updatedUserName='" + updatedUserName + '\'' +  "," +
			"updatedUser='" + updatedUser + '\'' +  "," +
			"updatedDate='" + updatedDate + '\'' +
			'}';
	}


}

