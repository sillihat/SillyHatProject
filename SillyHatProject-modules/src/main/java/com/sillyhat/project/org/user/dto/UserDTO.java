package com.sillyhat.project.org.user.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * UserDTO
 *
 * @author 徐士宽
 * @date 2017/3/29 14:13
 */
public class UserDTO implements Serializable{

    private static final long serialVersionUID = 7469030001914228449L;

    /**
     *  主键
     */
    private Long id;

    /**
     *  账号
     */
    private String login;

    /**
     *  密码
     */
    private String password;

    /**
     *  是否删除；0：未删除；1：删除
     */
    private int isDelete;

    /**
     *  是否超管；0：不是；1：是
     */
    private int isAdministrators;

    private UserDetailDTO userDetail;

    /**
     *  创建人
     */
    private Long createdUser;

    /**
     *  创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;

    /**
     *  修改人
     */
    private Long updatedUser;

    private String updatedUserName;

    /**
     *  修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedDate;

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public int getIsAdministrators() {
        return isAdministrators;
    }

    public void setIsAdministrators(int isAdministrators) {
        this.isAdministrators = isAdministrators;
    }

    public UserDetailDTO getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetailDTO userDetail) {
        this.userDetail = userDetail;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedUserName() {
        return updatedUserName;
    }

    public void setUpdatedUserName(String updatedUserName) {
        this.updatedUserName = updatedUserName;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", isDelete=" + isDelete +
                ", isAdministrators=" + isAdministrators +
                ", userDetail=" + userDetail +
                ", createdUser='" + createdUser + '\'' +
                ", createdDate=" + createdDate +
                ", updatedUser='" + updatedUser + '\'' +
                ", updatedUserName='" + updatedUserName + '\'' +
                ", updatedDate=" + updatedDate +
                '}';
    }
}
