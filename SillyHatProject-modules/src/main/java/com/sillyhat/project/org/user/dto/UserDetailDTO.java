package com.sillyhat.project.org.user.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


public class UserDetailDTO implements Serializable {

	private static final long serialVersionUID = 493783990809850095L;

	private Long id;//主键ID

	private Long userId;//用户ID

	private String userName;//用户名

	private int userAge;//年龄

	private int userGender;//性别；0：未知；1：男；2：女

	private String identityCard;//身份证号

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date userBirthday;//出生日期

	private String userPhone;//手机号

	private String userEmail;//邮箱

	private String userIcon;//用户头像

	private Long companyId;//公司ID

	private Long departmentId;//部门ID

	private int isJob;//是否在职；0：离职；1：在职

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date entryDate;//入职时间

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date leaveDate;//离职时间


	/**                             
	 * <p>Title: getId</p>
	 * <p>Description: </p>主键ID
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:Long               
	 */                             
	public Long getId(){
		return  id;
	}


	/**                             
	 * <p>Title: getUserId</p>
	 * <p>Description: </p>用户ID
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:int               
	 */                             
	public Long getUserId(){
		return  userId;
	}


	/**                             
	 * <p>Title: getUserName</p>
	 * <p>Description: </p>用户名
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:String               
	 */                             
	public String getUserName(){
		return  userName;
	}


	/**                             
	 * <p>Title: getUserAge</p>
	 * <p>Description: </p>年龄
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:int               
	 */                             
	public int getUserAge(){
		return  userAge;
	}


	/**                             
	 * <p>Title: getUserGender</p>
	 * <p>Description: </p>性别；0：未知；1：男；2：女
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:int               
	 */                             
	public int getUserGender(){
		return  userGender;
	}


	/**                             
	 * <p>Title: getIdentityCard</p>
	 * <p>Description: </p>身份证号
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:String               
	 */                             
	public String getIdentityCard(){
		return  identityCard;
	}


	/**                             
	 * <p>Title: getUserBirthday</p>
	 * <p>Description: </p>出生日期
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:String               
	 */                             
	public Date getUserBirthday(){
		return  userBirthday;
	}


	/**                             
	 * <p>Title: getUserPhone</p>
	 * <p>Description: </p>手机号
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:String               
	 */                             
	public String getUserPhone(){
		return  userPhone;
	}


	/**                             
	 * <p>Title: getUserEmail</p>
	 * <p>Description: </p>邮箱
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:String               
	 */                             
	public String getUserEmail(){
		return  userEmail;
	}


	/**                             
	 * <p>Title: getUserIcon</p>
	 * <p>Description: </p>用户头像
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:String               
	 */                             
	public String getUserIcon(){
		return  userIcon;
	}


	/**                             
	 * <p>Title: getCompanyId</p>
	 * <p>Description: </p>公司ID
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:int               
	 */                             
	public Long getCompanyId(){
		return  companyId;
	}


	/**                             
	 * <p>Title: getDepartmentId</p>
	 * <p>Description: </p>部门ID
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:int               
	 */                             
	public Long getDepartmentId(){
		return  departmentId;
	}


	/**                             
	 * <p>Title: getIsJob</p>
	 * <p>Description: </p>是否在职；0：离职；1：在职
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:int               
	 */                             
	public int getIsJob(){
		return  isJob;
	}


	/**                             
	 * <p>Title: getEntryDate</p>
	 * <p>Description: </p>入职时间
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:Date               
	 */                             
	public Date getEntryDate(){
		return  entryDate;
	}


	/**                             
	 * <p>Title: getLeaveDate</p>
	 * <p>Description: </p>离职时间
	 * @author XUSHIKUAN
	 * @date 2017-05-18              
	 * @return:Date               
	 */                             
	public Date getLeaveDate(){
		return  leaveDate;
	}



	/**                             
	 * <p>Title: setId</p>
	 * <p>Description: </p>主键ID
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setId(Long id){
		this.id = id;
	}


	/**                             
	 * <p>Title: setUserId</p>
	 * <p>Description: </p>用户ID
	 * @param userId
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setUserId(Long userId){
		this.userId = userId;
	}


	/**                             
	 * <p>Title: setUserName</p>
	 * <p>Description: </p>用户名
	 * @param userName
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setUserName(String userName){
		this.userName = userName;
	}


	/**                             
	 * <p>Title: setUserAge</p>
	 * <p>Description: </p>年龄
	 * @param userAge
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setUserAge(int userAge){
		this.userAge = userAge;
	}


	/**                             
	 * <p>Title: setUserGender</p>
	 * <p>Description: </p>性别；0：未知；1：男；2：女
	 * @param userGender
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setUserGender(int userGender){
		this.userGender = userGender;
	}


	/**                             
	 * <p>Title: setIdentityCard</p>
	 * <p>Description: </p>身份证号
	 * @param identityCard
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setIdentityCard(String identityCard){
		this.identityCard = identityCard;
	}


	/**                             
	 * <p>Title: setUserBirthday</p>
	 * <p>Description: </p>出生日期
	 * @param userBirthday
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setUserBirthday(Date userBirthday){
		this.userBirthday = userBirthday;
	}


	/**                             
	 * <p>Title: setUserPhone</p>
	 * <p>Description: </p>手机号
	 * @param userPhone
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setUserPhone(String userPhone){
		this.userPhone = userPhone;
	}


	/**                             
	 * <p>Title: setUserEmail</p>
	 * <p>Description: </p>邮箱
	 * @param userEmail
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setUserEmail(String userEmail){
		this.userEmail = userEmail;
	}


	/**                             
	 * <p>Title: setUserIcon</p>
	 * <p>Description: </p>用户头像
	 * @param userIcon
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setUserIcon(String userIcon){
		this.userIcon = userIcon;
	}


	/**                             
	 * <p>Title: setCompanyId</p>
	 * <p>Description: </p>公司ID
	 * @param companyId
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setCompanyId(Long companyId){
		this.companyId = companyId;
	}


	/**                             
	 * <p>Title: setDepartmentId</p>
	 * <p>Description: </p>部门ID
	 * @param departmentId
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setDepartmentId(Long departmentId){
		this.departmentId = departmentId;
	}


	/**                             
	 * <p>Title: setIsJob</p>
	 * <p>Description: </p>是否在职；0：离职；1：在职
	 * @param isJob
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setIsJob(int isJob){
		this.isJob = isJob;
	}


	/**                             
	 * <p>Title: setEntryDate</p>
	 * <p>Description: </p>入职时间
	 * @param entryDate
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setEntryDate(Date entryDate){
		this.entryDate = entryDate;
	}


	/**                             
	 * <p>Title: setLeaveDate</p>
	 * <p>Description: </p>离职时间
	 * @param leaveDate
	 * @author XUSHIKUAN
	 * @date 2017-05-18
	 * @return:void                 
	 */                             
	public void setLeaveDate(Date leaveDate){
		this.leaveDate = leaveDate;
	}





	@Override
	public String toString() {
		return "UserDetail{" +
			"id='" + id + '\'' +  "," +
			"userId='" + userId + '\'' +  "," +
			"userName='" + userName + '\'' +  "," +
			"userAge='" + userAge + '\'' +  "," +
			"userGender='" + userGender + '\'' +  "," +
			"identityCard='" + identityCard + '\'' +  "," +
			"userBirthday='" + userBirthday + '\'' +  "," +
			"userPhone='" + userPhone + '\'' +  "," +
			"userEmail='" + userEmail + '\'' +  "," +
			"userIcon='" + userIcon + '\'' +  "," +
			"companyId='" + companyId + '\'' +  "," +
			"departmentId='" + departmentId + '\'' +  "," +
			"isJob='" + isJob + '\'' +  "," +
			"entryDate='" + entryDate + '\'' +  "," +
			"leaveDate='" + leaveDate + '\'' +
			'}';
	}


}

