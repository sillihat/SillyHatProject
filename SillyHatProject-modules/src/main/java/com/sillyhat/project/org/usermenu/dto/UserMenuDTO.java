package com.sillyhat.project.org.usermenu.dto;

import java.io.Serializable;


public class UserMenuDTO implements Serializable {


	private Long id;//主键

	private Long userId;//用户ID

	private Long menuId;//菜单ID


	/**                             
	 * <p>Title: getId</p>
	 * <p>Description: </p>主键
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:Long               
	 */                             
	public Long getId(){
		return  id;
	}


	/**                             
	 * <p>Title: getUserId</p>
	 * <p>Description: </p>用户ID
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:int               
	 */                             
	public Long getUserId(){
		return  userId;
	}


	/**                             
	 * <p>Title: getMenuId</p>
	 * <p>Description: </p>菜单ID
	 * @author XUSHIKUAN
	 * @date 2017-04-14              
	 * @return:int               
	 */                             
	public Long getMenuId(){
		return  menuId;
	}



	/**                             
	 * <p>Title: setId</p>
	 * <p>Description: </p>主键
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setId(Long id){
		this.id = id;
	}


	/**                             
	 * <p>Title: setUserId</p>
	 * <p>Description: </p>用户ID
	 * @param userId
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setUserId(Long userId){
		this.userId = userId;
	}


	/**                             
	 * <p>Title: setMenuId</p>
	 * <p>Description: </p>菜单ID
	 * @param menuId
	 * @author XUSHIKUAN
	 * @date 2017-04-14
	 * @return:void                 
	 */                             
	public void setMenuId(Long menuId){
		this.menuId = menuId;
	}





	@Override
	public String toString() {
		return "UserMenu{" +
			"id='" + id + '\'' +  "," +
			"userId='" + userId + '\'' +  "," +
			"menuId='" + menuId + '\'' +
			'}';
	}


}

