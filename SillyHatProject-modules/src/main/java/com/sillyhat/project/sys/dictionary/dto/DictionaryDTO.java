package com.sillyhat.project.sys.dictionary.dto;

import java.io.Serializable;
import java.util.Date;


public class DictionaryDTO implements Serializable {

	private static final long serialVersionUID = 8718336376807047815L;

	private Long id;//主键ID

	private String dictCode;//code

	private String dictName;//名称

	private String dictValue;//值

	private int isDisabled;//是否禁用;0:未禁用;1:已禁用

	private int dictSort;//排序

	private String remark;//备注

	private Long createdUser;//创建人

	private Date createdDate;//创建时间

	private Long updatedUser;//修改人

	private Date updatedDate;//修改时间


	/**                             
	 * <p>Title: getId</p>
	 * <p>Description: </p>主键ID
	 * @author XUSHIKUAN
	 * @date 2017-07-05              
	 * @return:Long               
	 */                             
	public Long getId(){
		return  id;
	}


	/**                             
	 * <p>Title: getDictCode</p>
	 * <p>Description: </p>code
	 * @author XUSHIKUAN
	 * @date 2017-07-05              
	 * @return:String               
	 */                             
	public String getDictCode(){
		return  dictCode;
	}


	/**                             
	 * <p>Title: getDictName</p>
	 * <p>Description: </p>名称
	 * @author XUSHIKUAN
	 * @date 2017-07-05              
	 * @return:String               
	 */                             
	public String getDictName(){
		return  dictName;
	}


	/**                             
	 * <p>Title: getDictValue</p>
	 * <p>Description: </p>值
	 * @author XUSHIKUAN
	 * @date 2017-07-05              
	 * @return:String               
	 */                             
	public String getDictValue(){
		return  dictValue;
	}


	/**                             
	 * <p>Title: getIsDisabled</p>
	 * <p>Description: </p>是否禁用;0:未禁用;1:已禁用
	 * @author XUSHIKUAN
	 * @date 2017-07-05              
	 * @return:int               
	 */                             
	public int getIsDisabled(){
		return  isDisabled;
	}


	/**                             
	 * <p>Title: getDictSort</p>
	 * <p>Description: </p>排序
	 * @author XUSHIKUAN
	 * @date 2017-07-05              
	 * @return:int               
	 */                             
	public int getDictSort(){
		return  dictSort;
	}


	/**                             
	 * <p>Title: getRemark</p>
	 * <p>Description: </p>备注
	 * @author XUSHIKUAN
	 * @date 2017-07-05              
	 * @return:String               
	 */                             
	public String getRemark(){
		return  remark;
	}


	/**                             
	 * <p>Title: getCreatedUser</p>
	 * <p>Description: </p>创建人
	 * @author XUSHIKUAN
	 * @date 2017-07-05              
	 * @return:Long               
	 */                             
	public Long getCreatedUser(){
		return  createdUser;
	}


	/**                             
	 * <p>Title: getCreatedDate</p>
	 * <p>Description: </p>创建时间
	 * @author XUSHIKUAN
	 * @date 2017-07-05              
	 * @return:Date               
	 */                             
	public Date getCreatedDate(){
		return  createdDate;
	}


	/**                             
	 * <p>Title: getUpdatedUser</p>
	 * <p>Description: </p>修改人
	 * @author XUSHIKUAN
	 * @date 2017-07-05              
	 * @return:Long               
	 */                             
	public Long getUpdatedUser(){
		return  updatedUser;
	}


	/**                             
	 * <p>Title: getUpdatedDate</p>
	 * <p>Description: </p>修改时间
	 * @author XUSHIKUAN
	 * @date 2017-07-05              
	 * @return:Date               
	 */                             
	public Date getUpdatedDate(){
		return  updatedDate;
	}



	/**                             
	 * <p>Title: setId</p>
	 * <p>Description: </p>主键ID
	 * @param id
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 * @return:void                 
	 */                             
	public void setId(Long id){
		this.id = id;
	}


	/**                             
	 * <p>Title: setDictCode</p>
	 * <p>Description: </p>code
	 * @param dictCode
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 * @return:void                 
	 */                             
	public void setDictCode(String dictCode){
		this.dictCode = dictCode;
	}


	/**                             
	 * <p>Title: setDictName</p>
	 * <p>Description: </p>名称
	 * @param dictName
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 * @return:void                 
	 */                             
	public void setDictName(String dictName){
		this.dictName = dictName;
	}


	/**                             
	 * <p>Title: setDictValue</p>
	 * <p>Description: </p>值
	 * @param dictValue
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 * @return:void                 
	 */                             
	public void setDictValue(String dictValue){
		this.dictValue = dictValue;
	}


	/**                             
	 * <p>Title: setIsDisabled</p>
	 * <p>Description: </p>是否禁用;0:未禁用;1:已禁用
	 * @param isDisabled
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 * @return:void                 
	 */                             
	public void setIsDisabled(int isDisabled){
		this.isDisabled = isDisabled;
	}


	/**                             
	 * <p>Title: setDictSort</p>
	 * <p>Description: </p>排序
	 * @param dictSort
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 * @return:void                 
	 */                             
	public void setDictSort(int dictSort){
		this.dictSort = dictSort;
	}


	/**                             
	 * <p>Title: setRemark</p>
	 * <p>Description: </p>备注
	 * @param remark
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 * @return:void                 
	 */                             
	public void setRemark(String remark){
		this.remark = remark;
	}


	/**                             
	 * <p>Title: setCreatedUser</p>
	 * <p>Description: </p>创建人
	 * @param createdUser
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 * @return:void                 
	 */                             
	public void setCreatedUser(Long createdUser){
		this.createdUser = createdUser;
	}


	/**                             
	 * <p>Title: setCreatedDate</p>
	 * <p>Description: </p>创建时间
	 * @param createdDate
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 * @return:void                 
	 */                             
	public void setCreatedDate(Date createdDate){
		this.createdDate = createdDate;
	}


	/**                             
	 * <p>Title: setUpdatedUser</p>
	 * <p>Description: </p>修改人
	 * @param updatedUser
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 * @return:void                 
	 */                             
	public void setUpdatedUser(Long updatedUser){
		this.updatedUser = updatedUser;
	}


	/**                             
	 * <p>Title: setUpdatedDate</p>
	 * <p>Description: </p>修改时间
	 * @param updatedDate
	 * @author XUSHIKUAN
	 * @date 2017-07-05
	 * @return:void                 
	 */                             
	public void setUpdatedDate(Date updatedDate){
		this.updatedDate = updatedDate;
	}





	@Override
	public String toString() {
		return "Dictionary{" +
			"id='" + id + '\'' +  "," +
			"dictCode='" + dictCode + '\'' +  "," +
			"dictName='" + dictName + '\'' +  "," +
			"dictValue='" + dictValue + '\'' +  "," +
			"isDisabled='" + isDisabled + '\'' +  "," +
			"dictSort='" + dictSort + '\'' +  "," +
			"remark='" + remark + '\'' +  "," +
			"createdUser='" + createdUser + '\'' +  "," +
			"createdDate='" + createdDate + '\'' +  "," +
			"updatedUser='" + updatedUser + '\'' +  "," +
			"updatedDate='" + updatedDate + '\'' +
			'}';
	}


}

