package com.sillyhat.project.learning.usertodayplan.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * UserTodayPlanDTO
 *
 * @author 徐士宽
 * @date 2017/4/10 11:46
 */
public class UserTodayPlanDTO implements Serializable {

    private static final long serialVersionUID = -6044917229871750529L;

    /**
     *  主键
     */
    private long id;

    /**
     * 用户ID
     */
    private long userId;

    /**
     *  学习次数
     */
    private int learningNum;

    /**
     * 是否完成；0：未完成；1：已完成
     */
    private int isFinish;

    /**
     * 学习日期，数字类型
     */
    private int learningDateNum;

    /**
     *  创建人
     */
    private long createdUser;


    /**
     *  创建时间
     */
    private Date createdDate;

    /**
     *  修改人
     */
    private long updatedUser;

    /**
     *  修改时间
     */
    private Date updatedDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getLearningNum() {
        return learningNum;
    }

    public void setLearningNum(int learningNum) {
        this.learningNum = learningNum;
    }

    public int getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(int isFinish) {
        this.isFinish = isFinish;
    }

    public int getLearningDateNum() {
        return learningDateNum;
    }

    public void setLearningDateNum(int learningDateNum) {
        this.learningDateNum = learningDateNum;
    }

    public long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(long updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "UserTodayPlanDTO{" +
                "id=" + id +
                ", userId=" + userId +
                ", learningNum=" + learningNum +
                ", isFinish=" + isFinish +
                ", learningDateNum=" + learningDateNum +
                ", createdUser=" + createdUser +
                ", createdDate=" + createdDate +
                ", updatedUser=" + updatedUser +
                ", updatedDate=" + updatedDate +
                '}';
    }
}
