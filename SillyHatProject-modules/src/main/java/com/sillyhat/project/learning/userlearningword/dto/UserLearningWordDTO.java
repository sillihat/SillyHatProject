package com.sillyhat.project.learning.userlearningword.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * UserLearningWordDTO
 *
 * @author 徐士宽
 * @date 2017/4/7 16:28
 */
public class UserLearningWordDTO implements Serializable {

    private static final long serialVersionUID = -5406156787214361063L;

    /**
     *  主键
     */
    private long id;

    /**
     * 用户ID
     */
    private long userId;

    /**
     *  单词ID
     */
    private long wordId;

    /**
     * 学习状态;0:未学;1:已学;2:已掌握
     */
    private int learningStatus;

    /**
     * 复习次数;每次复习完成后，直接点击记得则递增，否则恢复0，当达到5时，学习状态变更为已掌握状态
     */
    private int learningFrequency;

    /**
     *  创建人
     */
    private long createdUser;


    /**
     *  创建时间
     */
    private Date createdDate;

    /**
     *  修改人
     */
    private long updatedUser;

    /**
     *  修改时间
     */
    private Date updatedDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getWordId() {
        return wordId;
    }

    public void setWordId(long wordId) {
        this.wordId = wordId;
    }

    public int getLearningStatus() {
        return learningStatus;
    }

    public void setLearningStatus(int learningStatus) {
        this.learningStatus = learningStatus;
    }

    public int getLearningFrequency() {
        return learningFrequency;
    }

    public void setLearningFrequency(int learningFrequency) {
        this.learningFrequency = learningFrequency;
    }

    public long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(long updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }


    @Override
    public String toString() {
        return "UserLearningWordDTO{" +
                "id=" + id +
                ", userId=" + userId +
                ", wordId=" + wordId +
                ", learningStatus=" + learningStatus +
                ", learningFrequency=" + learningFrequency +
                ", createdUser=" + createdUser +
                ", createdDate=" + createdDate +
                ", updatedUser=" + updatedUser +
                ", updatedDate=" + updatedDate +
                '}';
    }
}
