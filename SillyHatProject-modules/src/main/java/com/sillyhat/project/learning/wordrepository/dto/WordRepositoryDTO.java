package com.sillyhat.project.learning.wordrepository.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 单词库
 */
public class WordRepositoryDTO implements Serializable {

    private static final long serialVersionUID = -2992912088565114022L;

    /**
     *  主键
     */
    private long id;

    /**
     *  单词
     */
    private String word;

    /**
     * 单词类型
     */
    private int wordType;

    /**
     *  美式音标
     */
    private String usPhonetic;

    /**
     *  英式音标
     */
    private String ukPhonetic;

    /**
     *  翻译
     */
    private String wordTranslate;

    /**
     *  网络释义
     */
    private String webTranslate;

    /**
     *  例句
     */
    private String sampleSentences;

    /**
     *  创建人
     */
    private long createdUser;


    /**
     *  创建时间
     */
    private Date createdDate;

    /**
     *  修改人
     */
    private long updatedUser;

    /**
     *  修改时间
     */
    private Date updatedDate;

    private String createdUserName;
    private String updatedUserName;
    public String getCreatedUserName() {
        return createdUserName;
    }

    public void setCreatedUserName(String createdUserName) {
        this.createdUserName = createdUserName;
    }

    public String getUpdatedUserName() {
        return updatedUserName;
    }

    public void setUpdatedUserName(String updatedUserName) {
        this.updatedUserName = updatedUserName;
    }

    public int getWordType() {
        return wordType;
    }

    public void setWordType(int wordType) {
        this.wordType = wordType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getUsPhonetic() {
        return usPhonetic;
    }

    public void setUsPhonetic(String usPhonetic) {
        this.usPhonetic = usPhonetic;
    }

    public String getUkPhonetic() {
        return ukPhonetic;
    }

    public void setUkPhonetic(String ukPhonetic) {
        this.ukPhonetic = ukPhonetic;
    }

    public String getWordTranslate() {
        return wordTranslate;
    }

    public void setWordTranslate(String wordTranslate) {
        this.wordTranslate = wordTranslate;
    }

    public String getWebTranslate() {
        return webTranslate;
    }

    public void setWebTranslate(String webTranslate) {
        this.webTranslate = webTranslate;
    }

    public String getSampleSentences() {
        return sampleSentences;
    }

    public void setSampleSentences(String sampleSentences) {
        this.sampleSentences = sampleSentences;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(long createdUser) {
        this.createdUser = createdUser;
    }

    public long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(long updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "WordRepositoryDTO{" +
                "id=" + id +
                ", learning='" + word + '\'' +
                ", usPhonetic='" + usPhonetic + '\'' +
                ", ukPhonetic='" + ukPhonetic + '\'' +
                ", wordTranslate='" + wordTranslate + '\'' +
                ", webTranslate='" + webTranslate + '\'' +
                ", sampleSentences='" + sampleSentences + '\'' +
                ", createdUser=" + createdUser +
                ", createdDate='" + createdDate + '\'' +
                ", updatedUser=" + updatedUser +
                ", updatedDate='" + updatedDate + '\'' +
                ", createdUserName='" + createdUserName + '\'' +
                ", updatedUserName='" + updatedUserName + '\'' +
                '}';
    }
}