package com.sillyhat.project.core.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AccessDeniedFilter
 *
 * @author 徐士宽
 * @date 2017/3/30 13:44
 */
public class AccessDeniedFilter extends OncePerRequestFilter {

    private static final String ACCESS_DENIED = "Access denied!"; //access denied 访问被拒绝

    private static final Logger log = LogManager.getLogger(AccessDeniedFilter.class);

    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
        try {
            response.sendError(403, ACCESS_DENIED);
        } catch (IOException e) {
            log.error("发送访问被拒绝指令错误:{}", e.getMessage());
        }
    }
}
