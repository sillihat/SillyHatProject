package com.sillyhat.project.core.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.support.ResourcePropertySource;
import java.io.IOException;

/**
 * 为了实现自动读取配置环境
 *
 * @author 徐士宽
 * @date 2017/3/30 15:56
 */
public class CustomerApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final Logger log = LogManager.getLogger(CustomerApplicationContextInitializer.class);

    public void initialize(ConfigurableApplicationContext applicationContext) {
        log.info("启动项目自动读取[核心]配置文件......");
        try {
            ResourcePropertySource propertySource = new ResourcePropertySource("classpath:/env_config.properties");
            applicationContext.getEnvironment().getPropertySources().addFirst(propertySource);
        } catch (IOException e) {
            log.error("env_config.properties is not exists");
        }
    }
}