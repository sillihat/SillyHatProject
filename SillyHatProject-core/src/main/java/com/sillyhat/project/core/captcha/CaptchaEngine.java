package com.sillyhat.project.core.captcha;

import com.octo.captcha.component.image.backgroundgenerator.BackgroundGenerator;
import com.octo.captcha.component.image.backgroundgenerator.UniColorBackgroundGenerator;
import com.octo.captcha.component.image.color.RandomListColorGenerator;
import com.octo.captcha.component.image.deformation.ImageDeformation;
import com.octo.captcha.component.image.deformation.ImageDeformationByFilters;
import com.octo.captcha.component.image.fontgenerator.FontGenerator;
import com.octo.captcha.component.image.fontgenerator.RandomFontGenerator;
import com.octo.captcha.component.image.textpaster.DecoratedRandomTextPaster;
import com.octo.captcha.component.image.textpaster.TextPaster;
import com.octo.captcha.component.image.textpaster.textdecorator.TextDecorator;
import com.octo.captcha.component.image.wordtoimage.DeformedComposedWordToImage;
import com.octo.captcha.component.image.wordtoimage.WordToImage;
import com.octo.captcha.component.word.wordgenerator.RandomWordGenerator;
import com.octo.captcha.component.word.wordgenerator.WordGenerator;
import com.octo.captcha.engine.image.ListImageCaptchaEngine;
import com.octo.captcha.image.gimpy.GimpyFactory;

import java.awt.*;
import java.awt.image.ImageFilter;

/**
 * @author 徐士宽
 * @version 1.0
 * @project SillyHat Framework
 * @create 2017-03-23 14:22
 **/
public class CaptchaEngine extends ListImageCaptchaEngine {
    private static final String ACCEPTEDCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final  int FONTSIZE = 50;
    private static final Font[] fontsList = { new Font("nyala", Font.BOLD, FONTSIZE), new Font("Arial", Font.BOLD, FONTSIZE),
            new Font("nyala", Font.BOLD, FONTSIZE), new Font("Bell", Font.BOLD, FONTSIZE),
            new Font("Bell MT", Font.BOLD, FONTSIZE), new Font("Credit", Font.BOLD, FONTSIZE),
            new Font("valley", Font.BOLD, FONTSIZE), new Font("Impact", Font.BOLD, FONTSIZE)
    };
    private static final Color[] colorsList = { new Color(23, 170, 27),
            new Color(220, 34, 11),
            new Color(60, 60, 100),
            new Color(23, 67, 172),
            new Color(15, 60, 222)
    };

    protected void buildInitialFactories(){

        WordGenerator wordGenerator = new RandomWordGenerator(ACCEPTEDCHARS);
        //第一个参数用于设置验证码最少字符数，第二个参数为最多的字符数，第三个参数为字体颜色,第四个参数为干扰设置，例如new TextDecorator[] { new BaffleTextDecorator(new Integer(1), Color.WHITE) })这里是一个字符一个干扰点，并且干扰点为白色。
        TextPaster randomPaster = new DecoratedRandomTextPaster(4, 4, new RandomListColorGenerator(colorsList), new TextDecorator[0]);

        FontGenerator font = new RandomFontGenerator(12, 16, fontsList);

        ImageDeformation postDef = new ImageDeformationByFilters(new ImageFilter[] {});
        ImageDeformation backDef = new ImageDeformationByFilters(new ImageFilter[] {});
        ImageDeformation textDef = new ImageDeformationByFilters(new ImageFilter[] {});

        BackgroundGenerator background = new UniColorBackgroundGenerator(80, 28, Color.white);

        WordToImage word2image = new DeformedComposedWordToImage(font, background, randomPaster, backDef, textDef, postDef);
        addFactory(new GimpyFactory(wordGenerator, word2image));
    }
}
