package com.sillyhat.project.core.captcha;

import com.octo.captcha.service.CaptchaService;
import com.octo.captcha.service.multitype.GenericManageableCaptchaService;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wanyuxiang
 * @version 1.0
 * @project MGFaceFramework
 * @create 2017-03-23 14:39
 **/
@Configuration
public class CaptchaConfiguration {

    @Bean(name = "captchaService")
    public CaptchaService newInstance(){
        //minGuarantedStorageDelayInSeconds 验证码有效时间，单位秒
        return new GenericManageableCaptchaService(CSRCaptchaEngine(),300, 20000,20000);
    }

    @Bean(name = "captchaEngine")
    public CaptchaEngine CSRCaptchaEngine(){
        return new CaptchaEngine();
    }
}
