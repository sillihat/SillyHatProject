package com.sillyhat.project.redis.serializer;

import com.sillyhat.project.redis.utils.SillyHatSerializeUtils;

import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

/**
 * SillyHatValueRedisSerializer
 *
 * @author 徐士宽
 * @date 2017/5/4 14:28
 */
public class SillyHatValueRedisSerializer implements RedisSerializer<Object> {

    @Override
    public byte[] serialize(Object object) throws SerializationException {
        return SillyHatSerializeUtils.serialize(object);
    }

    @Override
    public Object deserialize(byte[] bytes) throws SerializationException {
        return SillyHatSerializeUtils.deSerialize(bytes);
    }
}
