package com.sillyhat.project.redis.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * RedisService
 *
 * @author 徐士宽
 * @date 2017/4/13 14:31
 */
@Service
public class RedisService {

    private Logger logger = LoggerFactory.getLogger(RedisService.class);

    @Autowired
    protected RedisTemplate<Serializable, Serializable> redisTemplate;

//    @Resource(name = "redisTemplate")
//    private ValueOperations valueOps;

//    @Resource(name = "redisTemplate")
//    private ListOperations listOps;
//
//    @Resource(name = "redisTemplate")
//    private SetOperations setOps;
//
//    @Resource(name = "redisTemplate")
//    private ZSetOperations zSetOps;

//    @Resource(name = "redisTemplate")
//    private ValueOperations<String, Object> valueOps;
//
//    @Resource(name = "redisTemplate")
//    private ListOperations<String, Object> listOps;
//
//    @Resource(name = "redisTemplate")
//    private SetOperations<String, Object> setOps;
//
//    @Resource(name = "redisTemplate")
//    private ZSetOperations<String, Object> zSetOps;

//    @Resource(name = "redisTemplate")
//    private ValueOperations<Serializable, Serializable> valueOps;
//
//    @Resource(name = "redisTemplate")
//    private ListOperations<Serializable, Serializable> listOps;
//
//    @Resource(name = "redisTemplate")
//    private SetOperations<Serializable, Serializable> setOps;
//
//    @Resource(name = "redisTemplate")
//    private ZSetOperations<Serializable, Serializable> zSetOps;

//    ValueOperations<Serializable, Serializable> test = redisTemplate.opsForValue();
//        test.set("","");

    /**
     * 存值
     * @param key
     * @param value
     */
    public void set(String key,Object value){
        this.set(key,value,0,null);
    }

    /**
     *
     * @param key
     * @param value
     * @param timeout   时间
     * @param timeUnit  时间单位
     */
    public void set(String key, Object value, long timeout, TimeUnit timeUnit){
        ValueOperations valueOps = redisTemplate.opsForValue();
        if(timeout > 0){
            valueOps.set(key,value,timeout,timeUnit);
        }else{
            valueOps.set(key,value);
        }
    }

    /**
     * 取值
     * @param key
     * @return
     */
    public Object get(String key){
        ValueOperations valueOps = redisTemplate.opsForValue();
        return valueOps.get(key);
    }

    /**
     * 取值
     * @param key
     * @return
     */
    public Object get(int key){
        ValueOperations valueOps = redisTemplate.opsForValue();
        return valueOps.get(key);
    }

    /**
     * 判断key是否存在
     * @param key
     * @return true:存在值
     */
    public boolean exists(final String key){
        return redisTemplate.execute(new RedisCallback<Boolean>() {
            @Override
            public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
                return connection.exists(key.getBytes());
            }
        });
    }

//    private static String redisCode = "utf-8";
//
//    /**
//     * @param key
//     */
//    public long del(final String... keys) {
//        return redisTemplate.execute(new RedisCallback() {
//            public Long doInRedis(RedisConnection connection) throws DataAccessException {
//                long result = 0;
//                for (int i = 0; i < keys.length; i++) {
//                    result = connection.del(keys[i].getBytes());
//                }
//                return result;
//            }
//        });
//    }
//
//    /**
//     * @param key
//     * @param value
//     * @param liveTime
//     */
//    public void set(final byte[] key, final byte[] value, final long liveTime) {
//        redisTemplate.execute(new RedisCallback() {
//            public Long doInRedis(RedisConnection connection) throws DataAccessException {
//                connection.set(key, value);
//                if (liveTime > 0) {
//                    connection.expire(key, liveTime);
//                }
//                return 1L;
//            }
//        });
//    }
//
//    /**
//     * @param key
//     * @param value
//     * @param liveTime
//     */
//    public void set(String key, String value, long liveTime) {
//        this.set(key.getBytes(), value.getBytes(), liveTime);
//    }
//
//    /**
//     * @param key
//     * @param value
//     */
//    public void set(String key, String value) {
//        this.set(key, value, 0L);
//    }
//
//    /**
//     * @param key
//     * @param value
//     */
//    public void set(byte[] key, byte[] value) {
//        this.set(key, value, 0L);
//    }
//
//    /**
//     * @param key
//     * @return
//     */
//    public String get(final String key) {
//        return redisTemplate.execute(new RedisCallback() {
//            public String doInRedis(RedisConnection connection) throws DataAccessException {
//                try {
//                    return new String(connection.get(key.getBytes()), redisCode);
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                }
//                return "";
//            }
//        });
//    }
//
//    /**
//     * @param pattern
//     * @return
//     */
//    public Setkeys(String pattern) {
//        return redisTemplate.keys(pattern);
//
//    }
//
//    /**
//     * @param key
//     * @return
//     */
//    public boolean exists(final String key) {
//        return redisTemplate.execute(new RedisCallback() {
//            public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
//                return connection.exists(key.getBytes());
//            }
//        });
//    }
//
//    /**
//     * @return
//     */
//    public String flushDB() {
//        return redisTemplate.execute(new RedisCallback() {
//            public String doInRedis(RedisConnection connection) throws DataAccessException {
//                connection.flushDb();
//                return "ok";
//            }
//        });
//    }
//
//    /**
//     * @return
//     */
//    public long dbSize() {
//        return redisTemplate.execute(new RedisCallback() {
//            public Long doInRedis(RedisConnection connection) throws DataAccessException {
//                return connection.dbSize();
//            }
//        });
//    }
//
//    /**
//     * @return
//     */
//    public String ping() {
//        return redisTemplate.execute(new RedisCallback() {
//            public String doInRedis(RedisConnection connection) throws DataAccessException {
//
//                return connection.ping();
//            }
//        });
//    }
}