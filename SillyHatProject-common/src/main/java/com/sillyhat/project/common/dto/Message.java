package com.sillyhat.project.common.dto;

/**
 * Created by yuxiang on 2017/3/23.
 */

import com.sillyhat.project.common.utils.SpringUtils;

/**
 * @param type    类型
 * @param content 内容
 * @author wanyuxiang
 */
public final class Message {
    private Type type;
    private String content;

    /**
     * @param success 成功
     * @param error   错误
     * @param  warn   警告
     */
    public enum Type {
        success, warn, error
    }

    public Message(Type type, String content) {
        this.type = type;
        this.content = content;
    }

    public Message(Type type, String content, Object[] args) {
        this.type = type;
        this.content = SpringUtils.getMessage(content, args);
    }

    public static Message success(String content, Object[] args) {
        return new Message(Type.success, content, args);
    }

    public static Message warn(String content, Object[] args) {
        return new Message(Type.warn, content, args);
    }

    public static Message error(String content, Object[] args) {
        return new Message(Type.error, content, args);
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String toString() {
        return SpringUtils.getMessage(this.content, new Object[0]);
    }
}
