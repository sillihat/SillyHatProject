package com.sillyhat.project.common.dto;

/**
 * DataTablesOrder
 *
 * @author 徐士宽
 * @date 2017/5/17 11:28
 */
public class DataTablesOrder {

    private int column;

    private String dir;

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    @Override
    public String toString() {
        return "DataTablesOrder{" +
                "column=" + column +
                ", dir='" + dir + '\'' +
                '}';
    }
}
