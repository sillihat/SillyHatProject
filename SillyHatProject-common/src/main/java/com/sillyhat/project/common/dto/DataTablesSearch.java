package com.sillyhat.project.common.dto;

/**
 * DataTablesSearch
 *
 * @author 徐士宽
 * @date 2017/5/17 11:27
 */
public class DataTablesSearch {

    private String value;

    private boolean regex;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isRegex() {
        return regex;
    }

    public void setRegex(boolean regex) {
        this.regex = regex;
    }

    @Override
    public String toString() {
        return "DataTablesSearch{" +
                "value='" + value + '\'' +
                ", regex=" + regex +
                '}';
    }
}
