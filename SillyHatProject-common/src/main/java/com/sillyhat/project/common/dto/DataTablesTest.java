package com.sillyhat.project.common.dto;

/**
 * DataTablesTest
 *
 * @author 徐士宽
 * @date 2017/5/17 13:44
 */
public class DataTablesTest {

    //请求传递参数
    private int draw;

    private int start;

    private int length;

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "DataTablesTest{" +
                "draw=" + draw +
                ", start=" + start +
                ", length=" + length +
                '}';
    }
}
