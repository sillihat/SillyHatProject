package com.sillyhat.project.common.constants;

import javax.servlet.http.HttpServletResponse;

/**
 * Constants
 *
 * @author 徐士宽
 * @date 2017/3/30 17:38
 */
public class Constants {

    /******************************浏览器状态码******************************/
    /**
     *
     */
    public static final int SILLYHAT_AJAX_REQUEST_ERROR = -1;


    public static final int SILLYHAT_AJAX_SUCCESS = HttpServletResponse.SC_OK;
    public static final String SILLYHAT_AJAX_SUCCESS_MSG = "Success";

    //    public static final int SUCCESS = HttpServletResponse.SC_OK; //200
//    public static final String SUCCESS_MSG = "success";
//
//    public static final int    UNAUTHORIZED = HttpServletResponse.SC_UNAUTHORIZED; //401
//    public static final String UNAUTHORIZED_MSG = "未授权";
//
//    public static final int    INTERNAL_SERVER_ERROR = HttpServletResponse.SC_INTERNAL_SERVER_ERROR; //500
//    public static final String INTERNAL_SERVER_ERROR_MSG = "内部服务器错误";
//
//    public static final int    LOGIN_TIMEOUT = 402;
//    public static final String LOGIN_TIMEOUT_MSG = "登录超时";

    /******************************浏览器状态码******************************/


    /**
     * 浏览器会话ID JSESSIONID
     */
    public static final String DEFAULT_COOKIE_JSESSIONID = "JSESSIONID";

    public static final int WORD_TYPE_WORD = 1;

    public static final int WORD_TYPE_PHRASE = 2;

    /**
     * 头部菜单
     */
    public static final int MENU_TYPE_HEADER = 1;


    /**
     * 当前用户
     */
    public static final String CURRENT_USER = "CURRENT.USER.";
    /**
     * 当前用户权限
     */
    public static final String CURRENT_USER_PERMISSION = "CURRENT.USER.PERMISSION.ID.";
//    public static final String CURRENT_USER = "";

    /**
     * 公共菜单
     */
    public static final long PERMISSION_PUBLIC_MENU = 0l;


    /**
     *  判断是否初始化code
     */
    public static final String SYS_DICTIONARY_DICTCODE_INIT_SYSTEM = "INIT_SYSTEM";
    public static final String INIT_SYSTEM_YES = "1";
    public static final String INIT_SYSTEM_NO = "0";


    public static final String SAFE_MANAGER_MENU_LIST = "SAFE.MANAGER.MENU.LIST";
    public static final Long SAFE_MANAGER_MENU_ID = 6l;


}
