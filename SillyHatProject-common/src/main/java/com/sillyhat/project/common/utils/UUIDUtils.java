package com.sillyhat.project.common.utils;

import java.util.UUID;

/**
 * UUIDUtils
 *
 * @author 徐士宽
 * @date 2017/5/19 16:25
 */
public class UUIDUtils {

    /**
     * 得到32位UUID
     * @return
     */
    public static String get32UUID() {
        return UUID.randomUUID().toString().trim().replaceAll("-", "").toUpperCase();
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(UUIDUtils.get32UUID());
        }
    }
}
