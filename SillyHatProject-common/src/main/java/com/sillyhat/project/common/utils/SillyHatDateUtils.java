package com.sillyhat.project.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * SillyHatDateUtils
 *
 * @author 徐士宽
 * @date 2017/4/10 16:07
 */
public class SillyHatDateUtils {

    private static final SimpleDateFormat sdf24DateTime = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");//24小时制

    private static final SimpleDateFormat sdf12DateTime = new SimpleDateFormat( "yyyy-MM-dd hh:mm:ss" );//12小时制

    private static final SimpleDateFormat sdfDate = new SimpleDateFormat( "yyyy-MM-dd" );//日期

    private static final SimpleDateFormat sdfDateNum = new SimpleDateFormat( "yyyyMMdd" );//日期

    /**
     * 得到今天的日期的数字显示方式YYYYMMDD
     * @return
     */
    public static int getTodayNum(){
        return Integer.parseInt(sdfDateNum.format(new Date()));
    }

}
