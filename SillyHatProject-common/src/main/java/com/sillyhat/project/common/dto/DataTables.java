package com.sillyhat.project.common.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * DataTables
 *
 * @author 徐士宽
 * @date 2017/5/17 11:22
 */
public class DataTables {

    //请求传递参数
    private int draw = 0;

    private List<DataTablesColumns> columns = new ArrayList<DataTablesColumns>();

    private List<DataTablesOrder> order = new ArrayList<DataTablesOrder>();

    private int start;

    private int length;

    private DataTablesSearch search = new DataTablesSearch();

    /**
     * 框架封装查询参数
     */
    private Map<String,Object> searchParams;

    //相应传递参数
    private Object data;

    private int recordsTotal = 0;

    private int recordsFiltered = 0;


    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public List<DataTablesColumns> getColumns() {
        return columns;
    }

    public void setColumns(List<DataTablesColumns> columns) {
        this.columns = columns;
    }

    public List<DataTablesOrder> getOrder() {
        return order;
    }

    public void setOrder(List<DataTablesOrder> order) {
        this.order = order;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public DataTablesSearch getSearch() {
        return search;
    }

    public void setSearch(DataTablesSearch search) {
        this.search = search;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        setRecordsFiltered(recordsTotal);
        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }


    public Map<String, Object> getSearchParams() {
        return searchParams;
    }

    public void setSearchParams(Map<String, Object> searchParams) {
        this.searchParams = searchParams;
    }

    @Override
    public String toString() {
        return "DataTables{" +
                "draw=" + draw +
                ", columns=" + columns +
                ", order=" + order +
                ", start=" + start +
                ", length=" + length +
                ", search=" + search +
                ", searchParams=" + searchParams +
                ", data=" + data +
                ", recordsTotal=" + recordsTotal +
                ", recordsFiltered=" + recordsFiltered +
                '}';
    }
}
