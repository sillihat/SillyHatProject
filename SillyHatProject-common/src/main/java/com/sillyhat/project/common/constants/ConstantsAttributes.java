package com.sillyhat.project.common.constants;

/**
 * 共同的属性类
 *
 * @author wanyuxiang
 */
public final class ConstantsAttributes {
    public static final String[] DATE_PATTERNS = {"yyyy", "yyyy-MM", "yyyyMM", "yyyy/MM", "yyyy-MM-dd", "yyyyMMdd", "yyyy/MM/dd",
            "yyyy-MM-dd HH:mm:ss", "yyyyMMddHHmmss", "yyyy/MM/dd HH:mm:ss"};
    public static final String AIPANDA_XML_PATH = "/aipanda.xml";
    public static final String AIPANDA_PROPERTIES_PATH = "/aipanda.properties";

    private ConstantsAttributes() {
    }
}